<?php
session_start(); 
if($_SESSION["connected"]!==TRUE){
 header("Location: ./testing/login.php");
 exit();
 }
 else $corrusername=$_SESSION["Corrusername"];
include("./poc/pdotest.php");
//$file = fopen("testing/votes-log.csv","r");


$idDoc=$_GET['idDoc'];
$arrayJSON=array();
$number=$_GET["number"];
$loadmyhighlight=0;
if(isset($_GET["loadmyhighlight"]))
  $loadmyhighlight=$_GET["loadmyhighlight"];
else
  $loadmyhighlight = null;
  
$stmt = null;
if($loadmyhighlight == 0){
  $stmt = $pdo->prepare("SELECT id,selection,user,idDoc number FROM redactor_highlight where idDoc=? and user=?");
  $stmt->execute([$idDoc,$corrusername]);
}else{
  $stmt = $pdo->prepare("SELECT id,selection,user,idDoc,count(selection) number FROM redactor_highlight where idDoc=? group BY selection having number >= ?");
  $stmt->execute([$idDoc,$number]);
}
while($array = $stmt->fetch())
  {
    //chargement des corrections pour chaque selection
    $corr = $pdo->prepare("SELECT correction FROM redactor_correction where fk_highlight = ?");
    $corr->execute([$array["id"]]);
    $correction = $corr->fetch();
    $object = new stdClass();
    $object->idDoc = $array["idDoc"];
    $object->id = $array["id"];
    $object->highlight = str_replace(array("\n", "\r"), '', $array["selection"]);
    $object->username = $array["user"];
    $object->correction = $correction['correction'];
    array_push($arrayJSON,$object);
  }
  echo json_encode( $arrayJSON, JSON_UNESCAPED_UNICODE);
?>

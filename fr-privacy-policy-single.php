<?php   

 session_start();  
 ?> 

<!DOCTYPE html>
<!-- saved from url=(0052)https://www.Iferu.com/fr/privacy-policy/single -->
<html class="js no-touch" lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Politique de confidentialité</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="fb:app_id" content="143891362343386">

<script type="text/javascript" src="./include/996526df3c"></script><script type="text/javascript" src="./include/pd.js"></script><script src="./include/nr-1198.min.js"></script><script src="./include/4918719.js" type="text/javascript" id="hs-analytics"></script><script src="./include/leadflows.js" type="text/javascript" id="LeadFlows-4918719" crossorigin="anonymous" data-leadin-portal-id="4918719" data-leadin-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod" data-hsjs-hublet="na1"></script><script src="./include/feedbackweb-new.js" type="text/javascript" id="hs-feedback-web-4918719" crossorigin="anonymous" data-hubspot-feedback-portal-id="4918719" data-hubspot-feedback-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod" data-hsjs-hublet="na1"></script><script src="./include/4918719(1).js" type="text/javascript" id="cookieBanner-4918719" data-cookieconsent="ignore" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod" data-hsjs-hublet="na1"></script><script async="" src="./include/clearbit.min.js"></script><script async="" src="./include/uwt.js"></script><script async="" src="./include/fbevents.js"></script><script async="" src="./include/tfa.js" id="tb_tfa_script"></script><script async="" src="./include/obtp.js" type="text/javascript"></script><script type="text/javascript" async="" src="./include/bat.js"></script><script type="text/javascript" async="" src="./include/insight.min.js"></script><script type="text/javascript" async="" src="./include/analytics.js"></script><script type="text/javascript" async="" src="./include/dc.js"></script><script async="" src="./include/gtm.js"></script><script type="text/javascript" async="async" src="./include/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js"></script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"996526df3c",applicationID:"3375187"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,w=["click","keydown","mousedown","pointerdown","touchstart"];w.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?u(e,f,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){e&&a&&e(n,r,i);for(var c=t(i),f=v(n),u=f.length,s=0;s<u;s++)f[s].apply(c,r);var p=d[h[n]];return p&&p.push([b,n,r,c]),c}}function o(e,t){y[e]=v(e).concat(t)}function m(e,t){var n=y[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return y[e]||[]}function g(e){return p[e]=p[e]||i(n)}function w(e,t){s(e,function(e,n){t=t||"feature",h[n]=t,t in d||(d[t]=[])})}var y={},h={},b={on:o,addEventListener:o,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:w,abort:c,aborted:!1};return b}function o(e){return u(e,f,a)}function a(){return new r}function c(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var f="nr@context",u=e("gos"),s=e(6),d={},p={},l=t.exports=i();t.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(y,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var w=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1198.min.js"},h=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:w,features:{},xhrWrappable:h,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var x=0},{}],"wrap-function":[function(e,t,n){function r(e,t){function n(t,n,r,f,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,f],s],e)}c(n+"start",[o,a,f],s,u);try{return p=t.apply(a,o)}catch(m){throw c(n+"err",[o,a,m],s,u),m}finally{c(n+"end",[o,a,p],s,u)}}return a(t)?t:(n||(n=""),nrWrapper[p]=t,o(t,nrWrapper,e),nrWrapper)}function r(e,t,r,i,o){r||(r="");var c,f,u,s="-"===r.charAt(0);for(u=0;u<t.length;u++)f=t[u],c=e[f],a(c)||(e[f]=n(c,s?f+r:r,i,f,o))}function c(n,r,o,a){if(!m||t){var c=m;m=!0;try{e.emit(n,r,o,t,a)}catch(f){i([f,n,r,o],e)}m=c}}return e||(e=s),n.inPlace=r,n.flag=p,n}function i(e,t){t||(t=s);try{t.emit("internal-error",e)}catch(n){}}function o(e,t,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(e);return r.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(o){i([o],n)}for(var a in e)l.call(e,a)&&(t[a]=e[a]);return t}function a(e){return!(e&&e instanceof Function&&e.apply&&!e[p])}function c(e,t){var n=t(e);return n[p]=e,o(e,n,s),n}function f(e,t,n){var r=e[t];e[t]=c(r,n)}function u(){for(var e=arguments.length,t=new Array(e),n=0;n<e;++n)t[n]=arguments[n];return t}var s=e("ee"),d=e(7),p="nr@original",l=Object.prototype.hasOwnProperty,m=!1;t.exports=r,t.exports.wrapFunction=c,t.exports.wrapInPlace=f,t.exports.argsToArray=u},{}]},{},["loader"]);</script>
 <link rel="stylesheet" type="text/css" href="./include/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css">

<link rel="stylesheet" type="text/css" href="./include/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css">

 <link rel="stylesheet" type="text/css" href="./include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
 <link rel="stylesheet" type="text/css" href="./fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css">

<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/AkkRg.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/AkkBd.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/AkkLi.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/line-icons.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/line-icons.woff2" type="font/woff2">


 <link rel="stylesheet" type="text/css" href="./include/styles-cb-8hnwdoahd9ttvwdf8gzbp7geuw33slu.css">


<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
 <script>
 window.VWO = window.VWO || [];
 window.VWO.push(['tag', 'Login', 'true', 'session']); 
 _vis_opt_check_segment = {'95': true };
 window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
 window.anonymous = 'true'; /* Used for VWO segmentation */
 </script>
 <script>
 var _vwo_code=(function(){
 var account_id=5083,
 settings_tolerance=2000,
 library_tolerance=2500,
 use_existing_jquery=false,
 // DO NOT EDIT BELOW THIS LINE
 f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
 </script><script src="./include/j.php" type="text/javascript"></script>


<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->
<?php 
if(isset($_SESSION["username"])){
 echo "<input type='hidden' value='true' id='session'></input>";
}?>
<script src="./include/f.txt"></script><script src="./include/f(1).txt"></script><script src="./include/va-9d6ac57dbcbba3321dd904e6ee78b647.js" crossorigin="anonymous" type="text/javascript"></script><script src="./include/track-9d6ac57dbcbba3321dd904e6ee78b647.js" crossorigin="anonymous" type="text/javascript"></script><script src="./include/opa-3bf1d20a05f5e943629318cc3d43e637.js" crossorigin="anonymous" type="text/javascript"></script><style type="text/css">
 #hs-feedback-ui {
   animation-duration: 0.4s;
   animation-timing-function: ease-out;
   display: none;
   height: 0;
   overflow: hidden;
   position: fixed;
   z-index: 2147483647;
   max-width: 100%;
 }

 .hubspot.space-sword #hs-feedback-ui {
   z-index: 1070;
 }

 #hs-feedback-ui.hs-feedback-shown {
   display: block;
 }

 #hs-feedback-fetcher {
   position: fixed;
   left: 1000%;
 }

 
 @keyframes feedback-slide-in-hs-feedback-left {
   from {transform: translate(0, 100%);}
   to {transform: translate(0, 0);}
 }

 @keyframes feedback-slide-out-hs-feedback-left {
   from {transform: translate(0, 0);}
   to {transform: translate(0, 100%);}
 }

 #hs-feedback-ui.hs-feedback-left {
   animation-name: feedback-slide-in-hs-feedback-left;
 }

 #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
   animation-name: feedback-slide-out-hs-feedback-left;
   animation-fill-mode: forwards;
 }

 
 @keyframes feedback-slide-in-hs-feedback-right {
   from {transform: translate(0, 100%);}
   to {transform: translate(0, 0);}
 }

 @keyframes feedback-slide-out-hs-feedback-right {
   from {transform: translate(0, 0);}
   to {transform: translate(0, 100%);}
 }

 #hs-feedback-ui.hs-feedback-right {
   animation-name: feedback-slide-in-hs-feedback-right;
 }

 #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
   animation-name: feedback-slide-out-hs-feedback-right;
   animation-fill-mode: forwards;
 }

 
 @keyframes feedback-slide-in-hs-feedback-top {
   from {transform: translate(0, -100%);}
   to {transform: translate(0, 0);}
 }

 @keyframes feedback-slide-out-hs-feedback-top {
   from {transform: translate(0, 0);}
   to {transform: translate(0, -100%);}
 }

 #hs-feedback-ui.hs-feedback-top {
   animation-name: feedback-slide-in-hs-feedback-top;
 }

 #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
   animation-name: feedback-slide-out-hs-feedback-top;
   animation-fill-mode: forwards;
 }


 #hs-feedback-ui > iframe {
   width: 100%;
   height: 100%;
 }

 #hs-feedback-ui:not(.hs-feedback-top) {
   bottom: 0;
 }

 #hs-feedback-ui.hs-feedback-left {
   left: 0;
 }

 #hs-feedback-ui.hs-feedback-right {
   right: 0;
 }

 .zorse #hs-feedback-ui:not(.hs-feedback-top) {
   bottom: 6px;
 }

 .zorse #hs-feedback-ui.hs-feedback-right {
   right: 0;
 }

 #hs-feedback-ui.hs-feedback-top {
   left: 0;
   top: 0;
   width: 100%;
 }

 #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
   width: 480px;
 }

 #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
   width: 350px;
 }

 #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
   width: 550px;
 }

 @media only screen and (min-width: 544px) {
   #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
     width: 450px;
   }
 }

 #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
   width: 550px !important;
 }

 #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
   width: 450px !important;
 }

 @media only screen and (max-width: 768px) {
   #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
   #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
     width: 100% !important;
   }
 }

 @media only screen and (max-width: 600px) {
   #hs-feedback-ui.preview:not(.hs-feedback-top),
   #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
     width: 100% !important;
   }
 }

 #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
 #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
   display: none !important;
 }

 /* hide all popups in the same position as us */
 @media only screen and (min-width: 544px) {
   #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
   #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
   #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
   #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
     display: none !important;
   }
 }

 /* hide leadflows when we're tablet-stretched across from them */
 @media only screen and (min-width: 544px) and (max-width: 970px) {
   #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
   #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
     display: none !important;
   }
 }

 /* hide messages when we're tablet-stretched across from them */
 @media only screen and (max-width: 966px) {
   #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
     display: none !important;
   }
 }

 @media only screen and (max-width: 544px) {

   /* repeat above rules for small screens when we're set to display on mobile */
   #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
   #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
   #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
   #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
   #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
   #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
   #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
     display: none !important;
   }

   /* don't display us on small screens if we're set to not display on mobile */
   #hs-feedback-ui.hs-feedback-no-mobile {
     display: none;
   }
 }
</style><script type="text/javascript" src="./include/analytics"></script><script type="text/javascript" src="./include/analytics(1)"></script></head>
<body class="" data-new-gr-c-s-check-loaded="14.997.0" data-gr-ext-installed=""><div id="hs-feedback-fetcher"><iframe frameborder="0" src="./include/feedback-web-fetcher.html"></iframe></div>

   <script>
     dataLayer = [{
       "PageView": "\x2Ffr\x2Fprivacy\x2Dpolicy\x2Fsingle"
       
     }];
   </script>
 
 <div class="sr-only"><a href="https://www.Iferu.com/fr/privacy-policy/single#main-content" tabindex="-1">Ignorer la navigation</a></div>

 <div class="" id="main-content" role="main">

 <?php 
include ("./header.php")
?>
<!--- START MAIN_BODY -->






<div class="pageheader" style="background-image: url(/www/images/privacy-policy.jpg)">
 
 <div class="pageheader__container container">
   <div class="row">
   <div class="col-lg-offset-2 col-lg-8">
     
 <h1>Politique de confidentialité</h1>

   </div>
   </div>
 </div>
</div><div class="primary container">
 <div style="margin-top: 2rem;">
   <p>Chez Iferu AG (<b>«&nbsp;nous&nbsp;»</b> ou <b>«&nbsp;notre&nbsp;»</b>), nous comprenons que vos données personnelles vous appartiennent. Toutes les informations recueillies à votre sujet (<b>«&nbsp;Vous&nbsp;»</b> ou <b>«&nbsp;votre&nbsp;»</b>), sont traitées en conformité avec les dispositions prévues par la Loi suisse sur la protection des données (<b>«&nbsp;LPD&nbsp;»</b>) et le Règlement général sur la protection des données (<b>«&nbsp;RGPD&nbsp;»</b>), en vigueur (collectivement, la «&nbsp;Réglementation&nbsp;»).</p>

<p> Cette politique de protection des données (la <b>«&nbsp;Politique&nbsp;»</b>) explique comment nous utilisons les informations personnelles (également  appelées «&nbsp;données personnelles&nbsp;» dans la Règlementation) que nous recueillons à votre sujet lorsque Vous souscrivez et utilisez nos services tels qu'ils sont détaillés dans le contrat d'abonnement (les <b>«&nbsp;Services&nbsp;»</b>) et lorsque Vous utilisez ce site Internet (le <b>«&nbsp;Site Internet&nbsp;»</b>).</p>

<h2>Comment recueillons-nous les informations Vous concernant&nbsp;?</h2>

<p>Nous recueillons des informations Vous concernant dans le contexte suivant&nbsp;:</p>

<p> Lorsque Vous vous abonnez à nos Services pour la première fois, Vous fournissez divers types d'information (voir ci-dessous). </p>

<p> En conséquence, nous créons et activons un accès à la Bibliothèque Iferu (la <b>«&nbsp;Bibliothèque&nbsp;»</b>) pour Vous. Cette Bibliothèque est hébergée et maintenue sur nos serveurs en Suisse. Elle se compose de résumés auxquels Vous aurez accès pendant la durée du contrat d'abonnement. </p>

<p>Lors de votre inscription, Vous bénéficierez de l'abonnement au service personnalisé hebdomadaire de résumés. Vous recevrez ainsi chaque semaine par e-mail un résumé correspondant à vos centres d'intérêt. La fréquence de ce service peut être modifiée à tout moment dans votre compte personnel. </p>


<h2> Quel type d'informations recueillons-nous Vous concernant&nbsp;?</h2>

<p>Les informations que nous recueillons concernent des données à caractère personnel que Vous avez consenti à nous fournir lorsque Vous vous êtes abonné à nos Services. Cela inclut les données suivantes&nbsp;: vos nom et prénom, adresse, numéro de téléphone, adresse e-mail, centre d’intérêt, langue, coordonnées de facturation et de carte de crédit.</p>

<p>Lorsque Vous utilisez nos services et notre Site Internet, nous recueillons des données sur Vous et votre utilisation de nos Services et de notre Site Internet. Elles incluent les données suivantes&nbsp;: adresses IP (y compris le pays d'où vous accédez à Internet), visites effectuées dans la Bibliothèque et sur le Site Internet, historique de lecture et de téléchargement à partir de la Bibliothèque.</p>

<p>Nous recueillons également des informations lorsque Vous répondez spontanément à des enquêtes de satisfaction, publiez des commentaires ou participez à des concours. Les informations relatives à l'utilisation du Site Internet sont recueillies grâce aux cookies (conjointement avec les informations ci-dessus&nbsp;: les <b> «&nbsp;Informations&nbsp;»</b>).</p>


<h2>Pourquoi et sur quelle base légale traitons-nous vos Informations&nbsp;?</h2>

<p>Nous traitons vos Informations pour honorer nos obligations au titre du contrat d'abonnement que nous avons conclu avec Vous ainsi que pour les utilisations suivantes&nbsp;: authentification des utilisateurs, contrôle d'accès, application des restrictions régionales en matière de droits d'auteur, personnalisation des notifications, transmission des notifications Push, ou aux fins de tout autre intérêt légitime, ou dans le but de respecter une obligation légale à laquelle nous sommes soumis dans le cadre de la prestation de nos Services.</p>


<h2> Comment utiliserons-nous les informations Vous concernant&nbsp;?</h2>

<p> Les Informations vous concernant sont utilisées pour fournir les Services auxquels Vous vous êtes abonné, gérer Votre compte et, si Vous y consentez, pour Vous informer par voie électronique de services supplémentaires susceptibles de Vous intéresser. Nous utilisons ces Informations pour personnaliser Vos visites répétées sur le Site Internet, pour améliorer votre expérience utilisateur ainsi qu'à des fins de développement interne.</p>

<p> À des fins statistiques ou publicitaires et pour le traitement des paiements, il se peut que nous partagions des Informations Vous concernant avec les prestataires tiers suivants&nbsp;:</p>

ul&gt;
<li>Datatrans, qui se charge du traitement des paiements en ligne par carte bancaire</li>
<li>Bing Adcenter</li>
<li>DoubleClick Floodlight</li>
<li>Facebook Ads</li>
<li>Google Analytics</li>
<li>Leadforensics.com</li>
<li>LinkedIn Ads</li>
<li>Outbrain</li>
<li>VisualWebsiteOptimizer</li>
<li>Google Adwords</li>
<li>Visitortracklog.com</li>
<li>Taboola</li>
<li>Salesforce</li>
<li>Twitter</li>
<li>Hubspot</li>
<li>Clearbit</li>
<li>New Relic</li>
<li>Amazon QuickSight</li>



<p>Aux fins de vérifier l'identité des étudiants, nous pouvons partager certaines de vos Informations avec les prestataires tiers suivants&nbsp;:</p>
<ul>
 <li>SheerId</li>
 <li>UNiDAYS</li>
</ul>

<p>Aux fins d'affichage des avatars des utilisateurs, nous pouvons partager certaines de vos informations avec Gravatar.</p>

<h2> Où sont conservées vos Informations personnelles&nbsp;?</h2>

<p>Nous sommes une entreprise suisse, Toutes vos Informations personnelles sont traitées et stockées sur des serveurs situés en Suisse. Il se peut que les prestataires tiers stockent et traitent Vos Informations dans d'autres pays. Nous avons mis en place des accords de transfert de données, tels que les Clauses Contractuelles Types de l'UE ou les accords suisses de Flux transfrontières de données avec chacun de ces prestataires. </p>

<h2>Transfert des Informations dans un pays hors EEE</h2>

<p>Il se peut que nous transférions des Informations dans des pays hors EEE tels que le Mexique, les Philippines et les États-Unis. Le transfert des Informations vers ces pays n'a lieu qu'à condition que les accords de transferts de données (tels que les Clauses Contractuelles Types de l'UE ou les accords suisses de Flux transfrontières de données) soient mis en place.</p>

<h2> Sécurité des données </h2>

<p> Nous avons mis en œuvre des mesures techniques et organisationnelles afin de protéger les Informations contre tout accès non autorisé de tiers et toute manipulation intentionnelle ou accidentelle, perte, destruction ou modification. Nous nous efforçons sans cesse d'améliorer cette protection à mesure que la technologie évolue.</p>


h2&gt;Marketing

<p> Il se peut que nous Vous fassions parvenir des informations relatives aux services que nous proposons et qui seraient susceptibles de Vous intéresser. Vous avez le droit de nous demander de cesser de Vous contacter à des fins de marketing et vous pouvez renoncer à cette option à tout moment. </p>


<h2>Vos droits</h2>

<p> Vous êtes en droit de demander l'accès aux et/ou une copie des Informations que nous détenons à votre sujet. Le cas échéant, merci de nous contacter par e-mail à l'adresse suivante&nbsp;: privacy@Iferu.com.</p>

<p> Nous souhaitons nous assurer que les Informations sont précises et actualisées, aussi vous avez le droit de nous demander de corriger ou de supprimer des Informations que vous jugez non correctes. </p>

<p>Vous êtes également en droit de nous demander de cesser de traiter vos Informations.</p>

<p>Vous avez également la possibilité d'exiger que les Informations soient transférées sous format structuré, communément utilisé et lisible par ordinateur, sous réserve que les conditions prévues par la Réglementation soient respectées. Vous pouvez également exercer vos droits en matière de profilage (s'il y a lieu). </p>


h2&gt; Notifications en cas de violation des données 

<p>Toute violation de données doit être signalée à privacy@Iferu.com. En cas de violation de données personnelles susceptible de présenter un risque pour Vos droits et votre liberté, nous veillerons à avertir immédiatment les autorités compétentes en matière de protection des données.</p>


<h2>Révocation du consentement</h2>

<p>Dans le cas où un consentement a été accordé, Vous avez le droit de révoquer ce consentement à tout moment en nous envoyant un avis écrit ou un e-mail à privacy@Iferu.com.</p>


<h2> Cookies </h2>

<p>Ce site Internet utilise des cookies et des technologies similaires afin de vous distinguer des autres utilisateurs. L'utilisation de cookies nous permet de vous proposer une meilleure expérience et d'améliorer notre site Internet grâce à une meilleure compréhension de la manière dont vous l'utilisez. </p>

<p>Pour plus d'informations, merci de consulter notre <a href="https://www.Iferu.com/cookie-policy"> Politique en matière de cookies</a>.</p>


<h2>Autres sites Internet</h2>

<p> Le Site Internet contient des liens vers d'autres sites web. La présente Politique s'applique uniquement aux Services et à ce Site Internet. Lorsque Vous vous connectez à d'autres sites, nous Vous conseillons de prendre connaissance de leurs propres politiques de confidentialité. </p>


<h2> Modifications de notre Politique </h2>

<p> Nous revoyons périodiquement notre Politique et toute modification sera notifiée sur ce Site Internet. La présente Politique a été mise à jour pour la dernière fois en novembre 2018. </p>


<h2> Nous contacter </h2>

<p> Nous répondrons volontiers à Vos questions sur notre Politique et sur les Informations que nous détenons sur Vous. Contactez-nous à&nbsp;: privacy@Iferu.com ou Iferu AG | Alpenquai 12 | 6005 Lucerne | Suisse. </p>

 </div>

</div><!-- End .primary -->
</div><!--- END MAIN BODY -->
<!--- END WRAPPER -->
 












<?php 
include ("./footer.php")
?>


<div id="fb-root"></div>


<script src="./include/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js"></script>
<script src="./include/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js"></script>
<script src="./include/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js"></script>
<script src="./include/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js"></script>
<script src="./include/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js"></script>

<script src="./include/plugins-cb-ejog1ksyxr8xfu5refkihg3clp16c6q.js"></script>
<script src="./include/main-cb-rvzyajns63bi2b72pp02t6b0oicxajz.js"></script>

<script>
 var hy = document.createElement('script'); hy.type = 'text/javascript'; hy.setAttribute("async", "async");
 hy.src = "/www/js/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js";
 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hy, s);
</script>

<script>
 
 $(function(){
   $.gaNotificationInit("/fr/notification/check?jsp=%2fWEB-INF%2fviews%2fabout%2fprivacy-policy-b2c.jsp", "/fr/notification/dismiss/{notificationId}");
 });
</script>

 <!-- Google Tag Manager -->
 <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-S2VR" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
 <script>
   (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
   new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
   j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
   '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
   })(window,document,'script','dataLayer','GTM-S2VR');
 </script>
 <!-- End Google Tag Manager -->
 
 
 <script>
   var google_conversion_id = 1043811595;
   var google_conversion_label = "dIVwCMG31wEQi5rd8QM";
   var google_custom_params = window.google_tag_params;
   var google_remarketing_only = true;
 </script>
 <script type="text/javascript" src="./include/f(2).txt"></script>
 <noscript>
   <div style="display:inline;">
     <img height="1" width="1" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1043811595/?value=0&amp;label=dIVwCMG31wEQi5rd8QM&amp;guid=ON&amp;script=0"/>
   </div>
 </noscript>
 <script>
 $(document).ready(function(){
  $('.navigation__btn').click(function(){
	$('.navigation__dropdown').toggle("show");
  });
 });
 </script>
 <script>
   var google_conversion_id = 1044773340;
   var google_conversion_label = "0ls-CJzzyAEQ3POX8gM";
   var google_custom_params = window.google_tag_params;
   var google_remarketing_only = true;
 </script>
 <script type="text/javascript" src="./include/f(2).txt"></script>
 <noscript>
   <div style="display:inline;">
     <img height="1" width="1" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1044773340/?value=0&amp;label=0ls-CJzzyAEQ3POX8gM&amp;guid=ON&amp;script=0"/>
   </div>
 </noscript>
 
 
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam-cell.nr-data.net","licenseKey":"996526df3c","agent":"","beacon":"bam-cell.nr-data.net","applicationTime":6,"applicationID":"3375187","transactionName":"ZlwEbUVTCENRAhVQWV8WNUlFWwhXcw4PTUReVQpcRR0WQlkXAFpPHEkJVV5RHx9DCA9eWlQZTn5yZk8=","queueTime":0}</script>
<div id="stats" data-ga-analytics="" data-ga-analytics-vt-attributes="{}" data-ga-analytics-meta="">
 <noscript>
   
   <img src="/gastat.gif?pv=1&url=https%3a%2f%2fwww.Iferu.com%2ffr%2fprivacy-policy%2fsingle&iframe=&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
 </noscript>
</div><script type="text/javascript" id="" src="./include/100323.js"></script><script type="text/javascript" id="">(function(){function t(){++g;50==g&&(k=!0);l()}function u(){m=!0;l()}function l(){k&&m&&!n&&(n=!0,dataLayer.push({event:"nobounce",scrollCount:g}))}var d=(new Date).getTime(),b=0,p=0,c=!0,h=!1,m=!1,k=!1,n=!1,g=0;setTimeout(u,3E4);var q=function(){p=(new Date).getTime();b+=p-d;c=!0},e=function(b){c&&(c=!1,d=(new Date).getTime(),h=!1);window.clearTimeout(r);r=window.setTimeout(q,5E3)},a=function(b,a){window.addEventListener?window.addEventListener(b,a):window.attachEvent&&window.attachEvent("on"+b,
a)},f=function(a){c||(b+=(new Date).getTime()-d);!h&&0<b&&36E5>b&&window.dataLayer.push({event:"nonIdle",nonIdleTimeElapsed:b});c&&(h=!0);a&&"beforeunload"===a.type&&window.removeEventListener("beforeunload",f);b=0;d=(new Date).getTime();window.setTimeout(f,15E3)};a("mousedown",e);a("keydown",e);a("mousemove",e);a("beforeunload",f);a("scroll",e);a("scroll",t);var r=window.setTimeout(q,5E3);window.setTimeout(f,15E3)})();</script><script type="text/javascript" id="">!function(d,e){var b="0059a9de7316e9b74d58489d3ccdef8d87";if(d.obApi){var c=function(a){return"[object Array]"===Object.prototype.toString.call(a)?a:[a]};d.obApi.marketerId=c(d.obApi.marketerId).concat(c(b))}else{var a=d.obApi=function(){a.dispatch?a.dispatch.apply(a,arguments):a.queue.push(arguments)};a.version="1.1";a.loaded=!0;a.marketerId=b;a.queue=[];b=e.createElement("script");b.async=!0;b.src="//amplify.outbrain.com/cp/obtp.js";b.type="text/javascript";c=e.getElementsByTagName("script")[0];
c.parentNode.insertBefore(b,c)}}(window,document);obApi("track","PAGE_VIEW");</script><script type="text/javascript" id="">window._tfa=window._tfa||[];window._tfa.push({notify:"event",name:"page_view"});!function(a,b,d,c){document.getElementById(c)||(a.async=1,a.src=d,a.id=c,b.parentNode.insertBefore(a,b))}(document.createElement("script"),document.getElementsByTagName("script")[0],"//cdn.taboola.com/libtrc/unip/1154722/tfa.js","tb_tfa_script");</script>
<noscript>
 <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","832786183443261");fbq("set","agent","tmgoogletagmanager","832786183443261");fbq("track","Page View");</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="hs-script-loader" src="./include/4918719(2).js"></script><script type="text/javascript" id="">!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version="1.1",a.queue=[],b=e.createElement(f),b.async=!0,b.src="//static.ads-twitter.com/uwt.js",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,"script");twq("init","o0jtb");twq("track","PageView");</script><script type="text/javascript" id="">!function(b){var a=b.clearbit=b.clearbit||[];if(!a.initialize)if(a.invoked)b.console&&console.error&&console.error("Clearbit snippet included twice.");else{a.invoked=!0;a.methods="trackSubmit trackClick trackLink trackForm pageview identify reset group track ready alias page once off on".split(" ");a.factory=function(c){return function(){var d=Array.prototype.slice.call(arguments);d.unshift(c);a.push(d);return a}};for(b=0;b<a.methods.length;b++){var e=a.methods[b];a[e]=a.factory(e)}a.load=function(c){var d=
document.createElement("script");d.async=!0;d.src=("https:"===document.location.protocol?"https://":"http://")+"x.clearbitjs.com/v1/"+c+"/clearbit.min.js";c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(d,c)};a.SNIPPET_VERSION="3.1.0";a.load("pk_381e766f277206e0822dcd305318bae2");a.page()}}(window);</script><script type="text/javascript" id="">piAId="903971";piCId="6461";piHostname="pi.pardot.com";(function(){function b(){var a=document.createElement("script");a.type="text/javascript";a.src=("https:"==document.location.protocol?"https://pi":"http://cdn")+".pardot.com/pd.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(a,c)}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load",b,!1)})();</script><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon742002095523"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon765714208139" width="0" height="0" alt="" src="./include/0"></div>
<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
</body></html>
<?php  

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Content-Type: application/json');
//database connection  
$conn = mysqli_connect('localhost', 'max', 'toor','books');  
if (! $conn) {  
die("Connection failed" . mysqli_connect_error());  
}  
else {  
mysqli_select_db($conn, 'pagination');  
}  
if (!isset ($_GET['page'])  ) {  
    $page = 1;  
   
} else {  
    $page = $_GET['page'];
  
    
}  
if ( isset($_GET['idlang'])){
 


    $idlang=$_GET['idlang'];

}
else{
    $idlang=".";
}
if ( isset($_GET['idChaine'])){
    $idChaine=$_GET['idChaine'];
}
else{
    $idChaine="%";
}

if ( isset($_GET['hasaudio'])){
    $hasaudio=$_GET['hasaudio'];
}
else{
    $hasaudio="%";
}
if ( isset($_GET['bestseller'])){
    $bestseller=$_GET['bestseller'];
}
else{
    $bestseller="%";
}
if ( isset($_GET['type'])){
    $type=$_GET['type'];

    if(  $type =='book' OR $type == 'livre' OR $type =='livro' OR   $type== 'buch' OR   $type== 'libro' OR $type=='Книга'){
        $type='Book|Livre|Livro|Buch|Libro|Книга';


    }
    if(  $type =='Artigo'||   $type == 'Artículo' || $type =='Artikel' ||   $type== 'article'){
        $type='Artigo|Artículo|Artikel|Article';

    }
}
else{
    $type=".";
}

//define total number of results per page  
$results_per_page = 10;  

//find the total number of results stored in the database  
$query = "SELECT D.*,C.nom_fr,C.desc_en,C.desc_fr,Auteur.nom FROM  Doc_Aut,Auteur,Document D,Chaine C  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and C.id= ".$idChaine." and D.idLangue like 1 and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id  ";
$result = mysqli_query($conn, $query);  
$number_of_result = mysqli_num_rows($result);  

//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page); 

//determine which page number visitor is currently on  


//determine the sql LIMIT starting number for the results on the displaying page  
$page_first_result = ($page-1) * $results_per_page;  

//retrieve the selected results from database   
// $query = "SELECT *FROM Document  LIMIT " . $page_first_result . ',' . $results_per_page;  
$query = "SELECT D.*,C.nom_fr,C.nom_en,Auteur.nom,C.desc_en,C.desc_fr, Rating.number FROM  Doc_Aut,Auteur,Document D,Chaine C,Rating  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and Rating.id=D.idRating and C.id= ".$idChaine." and idLangue REGEXP '^(".$idlang.")'  and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type REGEXP '^(".$type.")' and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id group by D.id order by D.id  LIMIT " . $page_first_result . ',' . $results_per_page;
if(isset($_GET['best'])){
    $query = "SELECT D.*,C.nom_fr,C.nom_en,Auteur.nom,C.desc_en,C.desc_fr, Rating.number FROM  Doc_Aut,Auteur,Document D,Chaine C,Rating  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and Rating.id=D.idRating and C.id= ".$idChaine." and idLangue REGEXP '^(".$idlang.")'  and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type REGEXP '^(".$type.")' and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id and bestseller='yes' group by D.id order by D.id  LIMIT " . $page_first_result . ',' . $results_per_page;



}
if(isset($_GET['popular'])){
    $query = "SELECT D.*,C.nom_fr,C.nom_en,Auteur.nom,C.desc_en,C.desc_fr, Rating.number FROM  Doc_Aut,Auteur,Document D,Chaine C,Rating  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and Rating.id=D.idRating and C.id= ".$idChaine." and idLangue REGEXP '^(".$idlang.")'  and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type REGEXP '^(".$type.")' and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id group by D.id order by likes +0 desc,D.id desc  LIMIT " . $page_first_result . ',' . $results_per_page;


}

if(isset($_GET['recent'])){
    $query = "SELECT D.*,C.nom_fr,C.nom_en,Auteur.nom,C.desc_en,C.desc_fr, Rating.number FROM  Doc_Aut,Auteur,Document D,Chaine C,Rating  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and Rating.id=D.idRating and C.id= ".$idChaine." and idLangue REGEXP '^(".$idlang.")'  and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type REGEXP '^(".$type.")' and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id group by D.id  order by date desc,D.id desc LIMIT " . $page_first_result . ',' . $results_per_page;


} 

if(isset($_GET['rating'])){
    $query = "SELECT D.*,C.nom_fr,C.nom_en,Auteur.nom,C.desc_en,C.desc_fr, Rating.number FROM  Doc_Aut,Auteur,Document D,Chaine C,Rating  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and Rating.id=D.idRating and C.id= ".$idChaine." and idLangue REGEXP '^(".$idlang.")'  and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type REGEXP '^(".$type.")' and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id group by D.id order by idRating desc ,D.id desc  LIMIT " . $page_first_result . ',' . $results_per_page;


} 


$result = mysqli_query($conn, $query);  
$table = array();

//display the retrieved result on the webpage  
while ($row = $result->fetch_assoc()) {  
      array_push($table,$row);
}  

 echo json_encode($table);	

 

?>  
<?php  

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Content-Type: application/json');
//database connection  
$conn = mysqli_connect('localhost', 'max', 'toor','books');  
if (! $conn) {  
die("Connection failed" . mysqli_connect_error());  
}  
else {  
mysqli_select_db($conn, 'pagination');  
}  

//define total number of results per page  
$results_per_page = 10;  
if ( isset($_GET['idChaine'])){
    $idChaine=$_GET['idChaine'];
}
else{
    $idChaine="%";
}
//find the total number of results stored in the database  
$query = "SELECT C.*  FROM Chaine C  where id like '".$idChaine."'";  
$result = mysqli_query($conn, $query);  
$number_of_result = mysqli_num_rows($result);  

//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  

//determine which page number visitor is currently on  
if (!isset ($_GET['page'])  ) {  
    $page = 1;  
   
} else {  
    $page = $_GET['page'];
  
    
}  




//determine the sql LIMIT starting number for the results on the displaying page  
$page_first_result = ($page-1) * $results_per_page;  

//retrieve the selected results from database   
// $query = "SELECT *FROM Document  LIMIT " . $page_first_result . ',' . $results_per_page;  
$query = "select C.*, (select count(idChaine)  from Doc_Chaine D where D.idChaine = C.id ) as bookCount   from Chaine C where C.id LIKE '".$idChaine."'  LIMIT " . $page_first_result . ',' . $results_per_page ;
$result = mysqli_query($conn, $query);  
$table = array();

//display the retrieved result on the webpage  
while ($row = $result->fetch_assoc()) {  
      array_push($table,$row);
}  

 echo json_encode($table);	

 

?>  
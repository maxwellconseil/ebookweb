<?php   
 session_start(); 

 ?> 

<!DOCTYPE html>
<!-- saved from url=(0068)http://backend.iferu.com/fr/resume/intelligence-economique/32972? -->
<html class="js no-touch" lang="fr">

<head>


<?php
  include("./poc/pdo.php");

      // Select Title of the book 
      $str= $_SERVER['REQUEST_URI'];

      $gg=explode('/',$str);
      $id=$_GET['id'];       
      $titre = $pdo->prepare("SELECT titre as t FROM Document Where id= ".$id);
      $titre->execute();
      $title = $titre->fetch();

      // Select description of the book
      $desc = $pdo->prepare("SELECT description as descr FROM Document Where id= ".$id);
      $desc->execute();
      $description = $desc->fetch();

      // Select authors of the book
      $at="select GROUP_CONCAT(DISTINCT A.nom separator ', ') AS author FROM Auteur A, Document D, Doc_Aut DA WHERE DA.idDoc =".$id." and A.id= DA.idAut";
       $auteur = $pdo->prepare($at);
      $auteur->execute();
      $author = $auteur->fetch(); 
    
    
$des = $description['descr'];
?>

<?php
$str= $_SERVER['REQUEST_URI'];

$gg=explode('/',$str);



if( empty($gg[2])){
  $gg[2]=$_GET['id'];


}

 if( substr( $gg[1], 0, 4 ) == 'book' or empty(substr( $gg[1], 7, 1) )){
  $name = str_replace(' ', '-', $title['t']);
  
header("Location: http://backend.iferu.com/resume-".$name."/".$gg[2], true, 301);
exit();
}

?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Iferu Book : <?php  echo $title['t']; ?></title>
<meta name="description" content="<?php  echo $description['descr']; ?>">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="og:title" content="<?php  echo $title['t']; ?>">
<meta property="og:type" content="article">
<meta property="og:url" content="<?php  echo 'http://backend.iferu.com/book.php?id='.$id; ?>">
<meta property="og:image" content="<?php  echo "http://backend.iferu.com/image_merge.php?id=".$id; ?>" >
<meta property="og:image:type" content="image/jpeg" />
<meta name="author" content="<?php echo $author['author'];?>">
<meta property="og:site_name" content="iferu">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@iferu">
<meta name="twitter:title" content="<?php  echo $title['t']; ?>">
<meta name="twitter:description" content="<?php   echo $description['descr'];  ?>">
<meta name="twitter:image" content="<?php  echo "http://backend.iferu.com/image_merge.php?id=".$id; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:app_id" content="143891362343386">
<style>.col-sm-8.col-sm-push-2{display:none;}.ix--bar{display:none;}.slick-container{display:flex;}</style>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->

<!-- jQuery library -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

<!-- Latest compiled JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
<!-- Optional theme -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
 -->
<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!-- <link rel="alternate" href="http://backend.iferu.com/oembed?url=https%3A%2F%2Fbackend.iferu.com%2Fsummary%2F32972&amp;format=json" type="application/json+oembed" title="Intelligence économique">
<link rel="alternate" href="http://backend.iferu.com/oembed?url=https%3A%2F%2Fbackend.iferu.com%2Fsummary%2F32972&amp;format=xml" type="text/xml+oembed" title="Intelligence économique">
 --><link rel="shortcut icon" href="http://backend.iferu.com/favicon.ico" type="image/x-icon">
<link rel="search" href="http://backend.iferu.com/opensearch.xml" type="application/opensearchdescription+xml" title="iferu">
 <script src="jquery-3.5.1.min.js"></script>

<link rel="stylesheet" type="text/css" href="http://backend.iferu.com/include_book/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css">
<link rel="stylesheet" type="text/css" href="http://backend.iferu.com/include_book/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css"> 
<link rel="stylesheet" type="text/css" href="http://backend.iferu.com/fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="http://backend.iferu.com/include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
<?php
if(isset($_SESSION['username']))
{
  echo "<input type='hidden' value='true' id='session'></input>";
  $idDoc = $gg[2] ;
  if( empty($gg[2])){ 
    $idDoc=$_GET['id'];
  }  
  
  echo "<input type='hidden' value='$idDoc' id='idDoc'></input>";
  $user_name = $_SESSION["username"] ;
  echo "<input type='hidden' value='$user_name' id='user_name'></input>";
  
  /* Hide share button when the user has already sent a book */
  $sent = "SELECT COUNT(*) as n FROM user_sent_books WHERE user_name='".$_SESSION['username']."'";
  $u = $pdo->prepare($sent);
  $u->execute();
  $res = $u->fetch();

  if($res['n'] > 0){
    echo '<style>.ico-share-alt{display:none;}</style>';
   
    
  }
    
  $flag = "SELECT available FROM Document WHERE id=".$idDoc;
  $u = $pdo->prepare($flag);
  $u->execute();
  $res = $u->fetch();

  if($res['available'] > 0){
    echo "<script>window.location.href='./no-book.php';</script>";
  }
}
?>
<script>
	window.VWO = window.VWO || [];
	window.VWO.push(['tag', 'Login', 'true', 'session']); 
	_vis_opt_check_segment = {'95': true };
	window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
	window.anonymous = 'true'; /* Used for VWO segmentation */
	</script>
	<script>
	var _vwo_code=(function(){
	var account_id=5083,
	settings_tolerance=2000,
	library_tolerance=2500,
	use_existing_jquery=false,
	// DO NOT EDIT BELOW THIS LINE
	f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script><script src="../include_book/j.php" type="text/javascript"></script>


<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->

<script src="../include_book/f.txt"></script>
<script src="../include_book/f(1).txt"></script>
<script src="../include_book/va-0ca7acdf418d8c12f3819dda65c35024.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include_book/track-0ca7acdf418d8c12f3819dda65c35024.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include_book/opa-56761856850233eb41e36332d7e3cf79.js" crossorigin="anonymous" type="text/javascript"></script><style type="text/css">
  
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }
  }
</style></head>
<body class="" data-new-gr-c-s-check-loaded="14.982.0"><div id="hs-feedback-fetcher">
<iframe frameborder="0" src="../include_book/feedback-web-fetcher.html"></iframe></div>


		<script>
			dataLayer = [{
				"PageView": "<?php  echo "/fr/resume/".$title['t']."/".$id; ?>"
				
			}];
		</script>
	
	<div class="sr-only">
  <a href="<?php  echo "http://backend.iferu.com/en/resume/".$title['t']."/".$id; ?>?#main-content">Skip navigation</a>
  </div>	
<?php 
 include ("header.php")

?>

<style>
.navigation__search {
  height: 60px; 
}

</style>

<!--- START MAIN_BODY -->


<?php 
$book = file_get_contents("./all_book/39492-h.html");
$sumary = file_get_contents("./all_book/39492.html");
$res = explode('<div class="clearfix" itemprop="review" itemscope itemtype="http://schema.org/Review">',$book);

 //echo $sumary;
 $url='http://backend.iferu.com/resume-/'.$id;
$url1 = urlencode($url);
 if(isset($_SESSION['username'])){
echo  $res[0];

//echo '<div class="multi-button"><button><i class="fa fa-amazon"></i><a href="http://www.facebook.com/sharer.php?u='.$url.'" target="_blank">Facebook</a></button>';
//echo '<button><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url='.$url.'"  target="_blank">LinkedIn</a></button>';
//echo '<button><a href="https://twitter.com/share?url='.$url.'"  target="_blank">Twitter</a></button></div>';
?>

<h2 style="text-align:center">Share to :</h2><br>
<div style="margin-left:44%">
<a style="margin-right:15px"  href="http://www.facebook.com/sharer.php?u=<?php echo $url1?>" target="_blank">
<button>
<i class="fa fa-facebook-official"></i></button></a>
<a style="margin-right:15px;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url1 ?>" 
target="_blank">
<button><i class="fa fa-linkedin-square"></i></button></a>
<a href="https://twitter.com/share?url=<?php echo $url1?>" target="_blank">
<button><i class="fa fa-twitter-square"></i></button></a>
 </div>
 <hr id="hr" style="display:none;">

 <div class="alert-danger" style="display:none; text-align:center;" id="data-msg"></div>
  <form style="display:none; text-align:center;" id="usernameForm" method="post">
    <label>Receiver</label>
    <input type="text" name="usernameS" id="usernameS">
    <input type="submit" id="sbmt" value="OK">
  </form>

  <?php
echo $sumary ;
echo explode('<div class="clear"></div>',$res[1])[1];

 }else{
  echo $book;
  
  ?>
  <h2 style="text-align:center" id="shareBox">Share to :</h2><br>
<div style="text-align:center" id="sharing">
<a style="margin-right:15px"  href="http://www.facebook.com/sharer.php?u=<?php echo $url1?>" target="_blank">
<button>
<i class="fa fa-facebook-official"></i></button></a>
<a style="margin-right:15px;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url1; ?>" 
target="_blank">
<button><i class="fa fa-linkedin-square"></i></button></a>
<a href="https://twitter.com/share?url=<?php echo $url1?>" target="_blank">
<button><i class="fa fa-twitter-square"></i></button></a>
 </div>
  <script>

$("h3 ").text('Linked channels').hide();
    $('.ga-channel-box').hide();
    $('#shareBox').insertAfter('.sump-buttons');
    $('#sharing').insertAfter('#shareBox');
    $('.ga-channel-box-changed').show();
    $('.chaine-changed').show();
    $('.row-changed').show();
    $('.col-sm-8.col-sm-push-2').hide();
    $('.row-changed').show();
</script>
<?php 
 }
 include './chaine_dynamique.php';
  include './commenter_resumer.php';
?>
 






<!--- END MAIN_BODY -->
<!--- END WRAPPER -->

<div id="fb-root"></div>


<script src="../include_book/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js"></script>
<script src="../include_book/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js"></script>
<script src="../include_book/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js"></script>
<script src="../include_book/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js"></script>
<script src="../include_book/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js"></script>

<script src="../include_book/plugins-cb-rqhn3ki7bf6j5lwmkcckeu85uqiuj47.js"></script>
<script src="../include_book/main-cb-tqm751dbrn8x06irjukcpdj53han94l.js"></script>

<script>
	var hy = document.createElement('script'); hy.type = 'text/javascript'; hy.setAttribute("async", "async");
	hy.src = "/www/js/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js";
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hy, s);
</script>

<script>
 	
 	$(function(){
 		$.gaNotificationInit("/fr/notification/check?jsp=%2fWEB-INF%2fviews%2fabstracts%2fsummary-page.jsp", "/fr/notification/dismiss/{notificationId}");
 	});
</script>
<style>#logout{display:none;}</style>

 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.0/jspdf.umd.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.2.6/purify.min.js"></script>  -->
<!-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jspdf-html2canvas@latest/dist/jspdf-html2canvas.min.js"></script> -->
<!-- 
<script src="html2pdf.bundle.js"></script> -->

<script>  

 $(document).ready(function(){  
 
  $(".fullSummaryShare").hide();$(".sump-audio").hide();$(".ix--bar").hide();

  $(".sump-formats li:eq(0)").click(function(event){
		//do something here
    event.preventDefault();

    pdfcreator();
	});
  $(".sump-formats li:eq(1)").click(function(event){
		//do something here
    event.preventDefault();

    epubcreator();
	});

function pdfcreator() {
 

     var resume= $('div[itemtype^="http://schema.org/"]').clone().find('h2[style="text-align:center"]').remove().end().find('.hidden').remove().end().find('.sump-cover-container').css('text-align','center').end().find('.affix-top,.furtherReading,.slick-list,.col-sm-push-2,.annot-section,.abstractCopyrightLine').remove().end().find('.scover--m').height('400px').end().find('hr').remove().end().html();



    var idDoc= $('#idDoc').val();
    console.log(idDoc);
    if(idDoc==undefined){
      alert("merci de vous connecter afin de télécharger le pdf OU EPUB");
    }
    else{
    var path="../pdfs/"+idDoc+".pdf"
		$.ajax({  
			 url:"../test.php",  
			 method:"POST",  
			 data:{resume:resume,idDoc:idDoc},  
			 success:function()  
			 {  
			   
        var link = document.createElement('a');
        link.href = path;
        link.download = idDoc+'.pdf';
        link.dispatchEvent(new MouseEvent('click'));
				  
			 }  
		});  }

   
}

  function pdfgenerator(){
    var resume= $('div[itemtype^="http://schema.org/"]').clone().find('h2[style="text-align:center"]').remove().end().find('.hidden').remove().end().find('.affix-top,.furtherReading,.slick-list,.col-sm-push-2,.annot-section,.abstractCopyrightLine').remove().end().find('hr').remove().end().html();


var idDoc= $('#idDoc').val();
if(idDoc==undefined){
  alert("merci de vous connecter afin de télécharger le pdf OU EPUB");
}
else{
var path="../pdfs/"+idDoc+".pdf"
$.ajax({  
   url:"../test.php",  
   method:"POST",  
   data:{resume:resume,idDoc:idDoc},  
   success:function()  
   {  
     
 
      
   }  
});  }
  }
function epubcreator(){
  pdfgenerator();

  
    var path="../pdfs/"+idDoc+".epub"
		$.ajax({  
			 url:"../pdfs/pdftoepub.php",  
			 method:"GET",  
			 data:{idDoc:idDoc},  
			 success:function()  
			 {  
        myVar = setTimeout(window.open(path, "_blank"), 2000);
        
				  
			 }  
		});
}


  
  var state= $('#session').val();
  var idDoc= $('#idDoc').val();
  var user_name= $('#user_name').val();
  
  
  if(state=='true'){
    $('#logout').show();
    $('#connexion').hide();
  }

// Pour les livres aimés
   $(".ico-heart-empty").click(function(event){
     event.preventDefault();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{user_name:user_name,idDoc:idDoc},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
       
	}); 



  // ajouter pour lire plus tard
  $(".ico-bookmark-empty").click(function(event){
    event.preventDefault();
    var id= $('#idDoc').val();
  var user= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{user:user,id:id},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});

  /* Dislike book */
  $(".fa-heart").click(function(event){
    event.preventDefault();
    var idL= $('#idDoc').val();
    var userN= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userN:userN,idL:idL},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
       
	});
  // supprimer de la liste à lire plus tard
  $(".fa-bookmark").click(function(event){
    event.preventDefault();
    var idl= $('#idDoc').val();
  var usern= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{usern:usern,idl:idl},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});


// ajouter à ma chaine
$(".ico-plus").click(function(event){
  event.preventDefault();
    var idLivre= $('#idDoc').val();
  var userName= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userName:userName,idLivre:idLivre},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});
 
// partager un livre avec un ami
$(".ico-share-alt").click(function(event){
  event.preventDefault();
  $("#usernameForm").show();
  $("#hr").show();});

  $('#sbmt').click(function(){
    var idlivre= $('#idDoc').val();
  var userNames= $('#user_name').val();
  var user_receiver = $("#usernameS").val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userNames:userNames,idlivre:idlivre,user_receiver:user_receiver},  
             success:function(data)  
             { 
               
               $('#data-msg').html(data) ;   
             }  
        });  
  });

$('.btn-link').click(function(){
  $('.navigation__dropdown').toggle("active");
});
      $('#login_button').click(function(){ 
        
           var username = $('#userName').val();  
           var password = $('#passWord').val();  
           if(username != '' && password != '')  
           {  
                $.ajax({  
                     url:"../pdo.php",  
                     method:"POST",  
                     data: {username:username, password:password},  
                     success:function(data)  
                     {  
                          
                          if(data == 'No')  
                          { 
                               alert("Username or password wrong");  
                               
                          }  
                          else  
                          { 
                               $('#loginModal').hide();  
                               
                               location.reload();  
                          }  
                          
                     }  
                });  
           }  
           else  
           {  
                alert("username and password are  Required");  
           }  
      });  
      $('.sumLoggingForm button').click(function(event){ $(this).removeAttr("data-target"); window.location.href="en-login.php";  });
      $('#logout').click(function(event){  
        
           var action = "logout"; 
           
           $.ajax({  
                url:"../pdo.php",  
                method:"POST",  
                data:{action:action},  
                success:function()  
                {  
                  
                     location.reload();  
                     
                }  
           });  
      });  
 });  
 </script>  
	
	<?php
/* Button pour aimer un livre */
// $like = "SELECT count(*) as nombre FROM user_like WHERE user_name='".$_SESSION['username']."' AND idDoc=".$idDoc;
// $u = $pdo->prepare($like);
// $u->execute();
// $res = $u->fetch();

// if($res['nombre'] <> 0){
//  echo '<script>$(".ico-heart-empty").attr("class","fa fa-heart");</script>';     
// } 

/* Button pour ajouter à lire plus tard */
$later = "SELECT count(*) as nombre FROM user_read_later WHERE username='".$_SESSION['username']."' AND idDoc=".$idDoc;
$la = $pdo->prepare($later);
$la->execute();
$resL = $la->fetch();

if($resL['nombre'] <> 0){
 echo '<script>$(".ico-bookmark-empty").attr("class","fa fa-bookmark");</script>';     
} 
  ?>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam.nr-data.net","licenseKey":"996526df3c","agent":"","beacon":"bam.nr-data.net","applicationTime":107,"applicationID":"3375187","transactionName":"ZlwEbUVTCENRAhVQWV8WNUlFWwhXcw4PTUReVQpcRR0VRV0MAEtPHkICWENTL1RNQUl+c2UQ","queueTime":0}</script>
<div id="stats" data-ga-analytics="21" data-ga-analytics-vt-attributes="{ &quot;id&quot; : &quot;32972&quot;}" data-ga-analytics-meta="">
	<noscript>
		
		<img src="/gastat.gif?pv=21&id=32972&url=https%3a%2f%2fbackend.iferu.com%2ffr%2fresume%2fintelligence-economique%2f32972&iframe=false&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
	</noscript>
</div><script type="text/javascript" id="" src="../include_book/100323.js"></script><script type="text/javascript" id="">(function(){function t(){++g;50==g&&(k=!0);l()}function u(){m=!0;l()}function l(){k&&m&&!n&&(n=!0,dataLayer.push({event:"nobounce",scrollCount:g}))}var d=(new Date).getTime(),b=0,p=0,c=!0,h=!1,m=!1,k=!1,n=!1,g=0;setTimeout(u,3E4);var q=function(){p=(new Date).getTime();b+=p-d;c=!0},e=function(b){c&&(c=!1,d=(new Date).getTime(),h=!1);window.clearTimeout(r);r=window.setTimeout(q,5E3)},a=function(b,a){window.addEventListener?window.addEventListener(b,a):window.attachEvent&&window.attachEvent("on"+b,
a)},f=function(a){c||(b+=(new Date).getTime()-d);!h&&0<b&&36E5>b&&window.dataLayer.push({event:"nonIdle",nonIdleTimeElapsed:b});c&&(h=!0);a&&"beforeunload"===a.type&&window.removeEventListener("beforeunload",f);b=0;d=(new Date).getTime();window.setTimeout(f,15E3)};a("mousedown",e);a("keydown",e);a("mousemove",e);a("beforeunload",f);a("scroll",e);a("scroll",t);var r=window.setTimeout(q,5E3);window.setTimeout(f,15E3)})();</script><script type="text/javascript" id="">!function(d,e){var b="0059a9de7316e9b74d58489d3ccdef8d87";if(d.obApi){var c=function(a){return"[object Array]"===Object.prototype.toString.call(a)?a:[a]};d.obApi.marketerId=c(d.obApi.marketerId).concat(c(b))}else{var a=d.obApi=function(){a.dispatch?a.dispatch.apply(a,arguments):a.queue.push(arguments)};a.version="1.1";a.loaded=!0;a.marketerId=b;a.queue=[];b=e.createElement("script");b.async=!0;b.src="//amplify.outbrain.com/cp/obtp.js";b.type="text/javascript";c=e.getElementsByTagName("script")[0];
c.parentNode.insertBefore(b,c)}}(window,document);obApi("track","PAGE_VIEW");</script><script type="text/javascript" id="">window._tfa=window._tfa||[];window._tfa.push({notify:"event",name:"page_view"});!function(a,b,d,c){document.getElementById(c)||(a.async=1,a.src=d,a.id=c,b.parentNode.insertBefore(a,b))}(document.createElement("script"),document.getElementsByTagName("script")[0],"//cdn.taboola.com/libtrc/unip/1154722/tfa.js","tb_tfa_script");</script>
<noscript>
  <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="hs-script-loader" src="../include_book/4918719(2).js"></script><script type="text/javascript" id="">!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version="1.1",a.queue=[],b=e.createElement(f),b.async=!0,b.src="//static.ads-twitter.com/uwt.js",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,"script");twq("init","o0jtb");twq("track","PageView");</script><script type="text/javascript" id="">!function(b){var a=b.clearbit=b.clearbit||[];if(!a.initialize)if(a.invoked)b.console&&console.error&&console.error("Clearbit snippet included twice.");else{a.invoked=!0;a.methods="trackSubmit trackClick trackLink trackForm pageview identify reset group track ready alias page once off on".split(" ");a.factory=function(c){return function(){var d=Array.prototype.slice.call(arguments);d.unshift(c);a.push(d);return a}};for(b=0;b<a.methods.length;b++){var e=a.methods[b];a[e]=a.factory(e)}a.load=function(c){var d=
document.createElement("script");d.async=!0;d.src=("https:"===document.location.protocol?"https://":"http://")+"x.clearbitjs.com/v1/"+c+"/clearbit.min.js";c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(d,c)};a.SNIPPET_VERSION="3.1.0";a.load("pk_381e766f277206e0822dcd305318bae2");a.page()}}(window);</script><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon807437191762"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon765245014009" width="0" height="0" alt="" src="../include_book/0"></div><script>window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>
<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
<script>$(".fullSummaryShare").hide();$(".sump-audio").hide();$(".ix--bar").hide();window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>
<script>
$("h3 ").text('Linked channels').hide();
    $('.ga-channel-box').hide();
    $('.ga-channel-box-changed').show();
    $('.chaine-changed').show();
    $('.row-changed').show();
    $('.col-sm-8.col-sm-push-2').hide();
    $('.row-changed').show();
</script>
  
</body></html>

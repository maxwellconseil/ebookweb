<link rel="stylesheet" type="text/css" href="/include_book/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css" defer>
<link rel="stylesheet" type="text/css" href="/include_book/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css" defer> 
<link rel="stylesheet" type="text/css" href="/fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css" >

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" defer>-->
<link rel="stylesheet" type="text/css" href="/include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
<link rel='stylesheet' href='/stylehighlight.css' defer />

<style>
  .abstractText{
      padding: 0 20 0 20;
  }
</style>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <meta name="viewport" content="width=device-width">
</head>
<body>
  <div class="all">
    <ul class="color">
        <span class="new">Highlight</span>
    </ul>
  </div>
</body>
<?php
// setcookie("outline", "true");

// $idBook=$_GET['idbook'];
// // $book = file_get_contents("http://app.iferu.com/book.php?id=".$idBook);
// // $book = file_get_contents("./all_book/".$idBook."-h.html");
// $sumary = file_get_contents("./all_book/".$idBook.".html");
// echo $sumary ;

include("./config.php"); 

require_once __DIR__ . '/vendor/autoload.php';
use simplehtmldom\HtmlWeb;

$idbook =  $_GET['idbook'];
$webParser = new HtmlWeb();
$html = $webParser->load (Backend_Url.'all_book/'.$idbook.'.html');

echo "<div>";
echo "<div style='display: inline-block;'>";
echo "<div id='takeAways' style='font-size:16px;float: left;font-family:Georgia;'>".$html->find(".col-sm-push-3", 0)."</div>";
echo "</div>";
echo "<div id='recomClass'style='display:inline-block;font-size:16px;font-family:Georgia;'>".$html->find(".col-sm-8", 0)."</div>";
echo "<div id='summary' style='display:inline-block;'>";
echo "<div id='summaryTitle' style='margin:20px;font-size:16px;font-family:Georgia;'>".$html->find(".abstractTitle", 0)."</div> ";
echo "<div id='summaryText' style='display:block;font-size:16px;font-family:Georgia;'>".$html->find(".abstractText",0)."</div>";
echo "</div>";
echo "<div id='auteurClass' style='display: inline-block;'>";
echo "<div id='auteurTitle' style='margin: 20px;font-size:16px;font-family:Georgia;'>".$html->find(".abstractTitle", 1)."</div> ";
echo "<div id='auteurText' style='display:block;margin:20px;font-size:16px;font-family:Georgia;'>".$html->find(".stretchedText",0)."</div>";
echo "</div>";
echo "</div>";
//echo $html->find(".abstractText", 0);
?>

<!-- <script src="jquery-3.5.1.min.js"></script> -->
<script src="../include_book/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js" ></script>

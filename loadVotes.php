<?php
session_start(); 

if($_SESSION["connected"]!==TRUE){
  header("Location: ./errors/login.php");
  exit();
}else 
  $corrusername=$_SESSION["Corrusername"];

include("./poc/pdo.php");

$arrayJSON=array();
$idDoc=$_GET['idDoc'];
$number=$_GET["number"];
$stmt = $pdo->prepare("SELECT DISTINCT redactor_correction.id,redactor_correction.correction,redactor_correction.fk_highlight,redactor_highlight.selection,redactor_highlight.idDoc,COUNT(redactor_vote.fk_correction) as NbrVotes FROM redactor_correction LEFT JOIN redactor_highlight ON redactor_correction.fk_highlight = redactor_highlight.id LEFT JOIN redactor_vote ON redactor_vote.fk_correction = redactor_correction.id where idDoc=? GROUP BY redactor_correction.id ORDER BY COUNT(redactor_vote.fk_correction)");
$stmt->execute([$idDoc]);
while($array = $stmt->fetch())
  {
    if($array["NbrVotes"] <= $number){
      $object = new stdClass();
      $object->id=$array["id"];
      $object->idDoc = $array["idDoc"];
      $object->highlight = str_replace(array("\n", "\r"), '', $array["selection"]);
      $object->correction = $array["correction"];
      $object->nombrevotes = $array["NbrVotes"];
      array_push($arrayJSON,$object);
    }
  }
  echo json_encode($arrayJSON, JSON_UNESCAPED_UNICODE);
?>
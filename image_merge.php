<?php
include("./config.php"); 

$id=$_GET['id'];

$dest = imagecreatefromjpeg(Backend_Url.'summary-img/back.jpg');
$src = imagecreatefromjpeg(Backend_Url.'image.php?id='.$id);

imagealphablending($dest, false);
imagesavealpha($dest, true);

imagecopymerge($dest, $src, 300, 0, 0, 0, 627, 627, 100); //have to play with these numbers for it to work for you, etc.

header('Content-Type: image/jpeg');
imagejpeg($dest);

imagedestroy($dest);
imagedestroy($src);
?>
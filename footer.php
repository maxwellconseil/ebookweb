
<?php
$langbrowser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$current_page=basename($_SERVER['PHP_SELF']);
?>




<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<footer class="footer">
  <div class="container">

    
    
    <hr class="bold footer__divider">
    <div class="footer__menu">
      <a href="http://backend.iferu.com/" class="footer__logo footer__anchor">
        <img src="./include/ga_footer-icon_102x102.svg" alt="iferu footer icon" height="102" width="102">
      </a>

      <div class="footer__menu-col panel">
        <a role="button" data-toggle="collapse" href="http://backend.iferu.com/fr/#footer__menu-list-1" aria-expanded="false" aria-controls="footer__menu-list-1">
          <h5 class="footer__menu-title">Entreprise<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
        </a>
        <ul id="footer__menu-list-1" class="footer__menu-list collapse" data-parent=".footer__menu">
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/about-us" class="footer__anchor">À propos de nous</a></li>
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/next-gen-leaders" class="footer__anchor">#NextGenLeaders</a></li>
          
              <li class="footer__list-item"><a href="https://journal.iferu.com/en/iferu-international-book-award-2020/" target="_blank" class="footer__anchor" rel="noopener noreferrer">Livres primés</a></li>
            
          
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/pages/jsp/Careers.jsp" class="footer__anchor">Carrières</a></li>
          
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/how-it-works/faqs" class="footer__anchor">FAQ</a></li>
          <li class="footer__list-item"><a href="http://backend.iferu.com/Support.do" class="footer__anchor">Nous contacter</a></li>
        </ul>
      </div>
      <div class="footer__menu-col panel">
        <a role="button" data-toggle="collapse" href="http://backend.iferu.com/fr/#footer__menu-list-2" aria-expanded="false" aria-controls="footer__menu-list-2">
          <h5 class="footer__menu-title">Ressources<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
        </a>
        <ul id="footer__menu-list-2" class="footer__menu-list collapse" data-parent=".footer__menu">
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/how-it-works/overview" class="footer__anchor">Comment ça marche ?</a></li>
          
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/summary-suggestions" class="footer__anchor">Suggestions de résumés</a></li>
          
          
          
          
            <li class="footer__list-item"><a href="https://journal.iferu.com/en/start" target="_blank" class="footer__anchor">Journal</a></li>
          
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/enterprise/casestudies" class="footer__anchor">Success stories de nos clients</a></li>
          
        </ul>
      </div>
      <div class="footer__menu-col panel">
        <a role="button" data-toggle="collapse" href="http://backend.iferu.com/fr/#footer__menu-list-3" aria-expanded="false" aria-controls="footer__menu-list-3">
          <h5 class="footer__menu-title">Partenaires<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
        </a>
        <ul id="footer__menu-list-3" class="footer__menu-list collapse" data-parent=".footer__menu">
          
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/publishers" class="footer__anchor">Éditeurs</a></li>
          
          
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/media-relations" class="footer__anchor">Relations presse</a></li>
          
          
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/alliance-partners" class="footer__anchor">Partenaires</a></li>
        </ul>
      </div>
      <div class="footer__menu-col panel">
        <a role="button" data-toggle="collapse" href="http://backend.iferu.com/fr/#footer__menu-list-4" aria-expanded="false" aria-controls="footer__menu-list-4">
          <h5 class="footer__menu-title">Produit<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
        </a>
        <ul id="footer__menu-list-4" class="footer__menu-list collapse" data-parent=".footer__menu">
          
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/trial" class="footer__anchor">Essai gratuit</a></li>
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/subscribe/products" class="footer__anchor">Abonnements personnels</a></li>
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/studentpass" class="footer__anchor">Abonnements étudiants</a></li>
            <li class="footer__list-item"><a href="http://backend.iferu.com/fr/subscribe/gifts" class="footer__anchor">Abonnements cadeaux</a></li>
          
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/enterprise/solutions" class="footer__anchor">Solutions pour entreprises</a></li>
          <li class="footer__list-item"><a href="http://backend.iferu.com/fr/custom-summaries" target="_blank" class="footer__anchor">Service personnalisé de résumés</a></li>
          
        </ul>
      </div>
    </div>
    

    <hr class="dark footer__divider">

    <div class="footer__legal">

      
      <div class="footer__language">
        
        <h5 class="sr-only">Langue</h5>
         <ul class="footer__language-list">
           
                <li class="footer__list-item">
                    <form  id="frenchform" action="" method="post">
                        <a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >English</a>	
                        <input type="hidden" name="frenchform"  >
                    </form>      
                    <?php
                      include("change_langage.php");
                    ?>
                </li>

                <li class="footer__list-item">
                    <form  id="frenchform" action="" method="post">
                        <a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Deutsch</a>	
                        <input type="hidden" name="frenchform"  >
                    </form>     
                    <?php
                      include("change_langage.php");
                    ?>
                </li>

                <li class="footer__list-item">
                    <form  id="frenchform" action="" method="post">
                        <a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Español</a>	
                        <input type="hidden" name="frenchform"  >
                    </form> 
                      <?php
                        include("change_langage.php");
                      ?>
                </li>

                <li class="footer__list-item">
                  <form  id="frenchform" action="" method="post">
                      <a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Русский</a>	
                      <input type="hidden" name="frenchform"  >
                  </form>     
                  <?php
                    include("change_langage.php");
                  ?>
                </li>

                <li class="footer__list-item">
                  <form  id="frenchform" action="" method="post">
                      <a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >中文</a>	
                      <input type="hidden" name="frenchform"  >
                  </form>   
                  <?php
                    include("change_langage.php");
                  ?>
                </li>

                <li class="footer__list-item">
                  <form  id="frenchform" action="" method="post">
                      <a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Português</a>	
                      <input type="hidden" name="frenchform"  >
                  </form>   
                  <?php
                    include("change_langage.php");
                  ?>
                </li>

                 <li class="footer__list-item">
                  <strong>Français</strong>
                </li>
                <?php include("change_langage.php");?>   

          <!-- </li> -->

        
            
            <!--  <li class="footer__list-item">
              <a href="http://backend.iferu.com/de/" class="footer__anchor">Deutsch</a>
              </li> -->

        
              
            
              <!-- <li class="footer__list-item">
              <a href="http://backend.iferu.com/es/" class="footer__anchor">Español</a>
              </li> -->

        
            
              <!-- <li class="footer__list-item">
              <a href="http://backend.iferu.com/ru/" class="footer__anchor">Русский</a>
              </li> -->

          
            
              <!-- <li class="footer__list-item">
              <a href="http://backend.iferu.com/zh/" class="footer__anchor">中文</a>
              </li> -->

          
            
              <!-- <li class="footer__list-item">
              <a href="http://backend.iferu.com/pt/" class="footer__anchor">Português</a>
              </li> -->
            
              
          <!-- </ul> -->
          </ul>
        
          
      </div>
        
     
      <hr class="footer__legal-divider footer__divider dark">

      
      <ul class="footer__legal-list">
        
        <li class="footer__list-item"><a href="fr-privacy-policy-single.php" class="footer__anchor" >Politique de confidentialité </a></li>
        <li class="footer__list-item"><a href="fr-terms-of-use.php" class="footer__anchor">Conditions d'utilisation</a></li>
        
            
          
        <li class="footer__list-item"><a href="http://backend.iferu.com/www/docs/tc/iferu-tc-b2c-fr.pdf" target="_blank" class="footer__anchor">Conditions contractuelles</a></li>
        <li class="footer__list-item"><a href="http://backend.iferu.com/fr/accessibility" class="footer__anchor">Accessibilité</a></li>
      </ul>

      
      <div class="footer__social">
        <h5 class="sr-only">Connexion</h5>
        <ul class="footer__social-icons">
          <li class="footer__list-item"><a target="_blank" href="http://www.facebook.com/iferu" title="Retrouvez-nous sur Facebook" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-facebook-official" aria-hidden="true"></i><span class="sr-only">Facebook</span></a></li>
          <li class="footer__list-item"><a target="_blank" href="https://www.linkedin.com/company/iferu-com/" title="Rejoignez notre communauté LinkedIn" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-linkedin-square" aria-hidden="true"></i><span class="sr-only">LinkedIn</span></a></li>
          <li class="footer__list-item"><a target="_blank" href="https://www.instagram.com/iferusolution/" title="Retrouvez-nous sur Instagram" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-instagram" aria-hidden="true"></i><span class="sr-only">Instagram</span></a></li>
          <li class="footer__list-item"><a target="_blank" href="https://twitter.com/IferuSolution" title="Suivez-nous sur Twitter" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-twitter-square" aria-hidden="true"></i><span class="sr-only">Twitter</span></a></li>
          
          
        </ul>
      </div>

      
      <div class="footer__copyright">
        <a href="http://backend.iferu.com/fr/pages/jsp/Copyright.jsp" class="footer__anchor">
          <p class="footer__paragraph">© 1999-2020, iferu</p>
        </a>
      </div>

    </div>

  </div>

</footer>
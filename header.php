
<?php
session_start();
include ("./config.php");

 $langbrowser = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
 $str= $_SERVER['REQUEST_URI'];

 $gg=explode('/',$str);
 $current_page=basename($_SERVER['PHP_SELF']);
 if( substr( $gg[1], 0, 4 ) == 'resu'){
	$bookpage='resume';

 }

 if( substr( $gg[2], 0, 4 ) == 'resu'){
	$chainepage='resume';

 }

?>
<link rel="stylesheet" type="text/css" href="./include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
<!-- 
<script type="text/javascript" >

window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2B700tRsUVsHz9QQ7SAuRg2D6vp3KXyD';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');

</script> -->

<script>
window['_fs_debug'] = false;
window['_fs_host'] = 'fullstory.com';
window['_fs_script'] = 'edge.fullstory.com/s/fs.js';
window['_fs_org'] = '112KSF';
window['_fs_namespace'] = 'FS';
(function(m,n,e,t,l,o,g,y){
    if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
    g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
    o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_script;
    y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
    g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
    g.anonymize=function(){g.identify(!!0)};
    g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
    g.log = function(a,b){g("log",[a,b])};
    g.consent=function(a){g("consent",!arguments.length||a)};
    g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
    g.clearUserCookie=function(){};
    g.setVars=function(n, p){g('setVars',[n,p]);};
    g._w={};y='XMLHttpRequest';g._w[y]=m[y];y='fetch';g._w[y]=m[y];
    if(m[y])m[y]=function(){return g._w[y].apply(this,arguments)};
    g._v="1.3.0";
})(window,document,window['_fs_namespace'],'script','user');
</script>
<script type='text/javascript'>
  window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '2fffc75d2b7aed2dbefefe2ecb34002bdc164046');
</script>
<!-- Hotjar Tracking Code for http://backend.iferu.com/ -->
<!-- <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2288701,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	
	
	$('.notifybars').hide();
</script> -->

<?php

?>
		
<header id="header" class="header header--prospect-cta">
		<nav class="navigation">
			
				<input type="checkbox" id="nav-mobile" class="navigation__mobile-toggle">
			    <label for="nav-mobile" class="navigation__mobile">
			        <i class="ico-menu"></i>
					<span class="sr-only">Menu</span>
			    </label>
			    <div class="navigation__logo">
					<a href="/index.php" class="logo_index"> 
						<img src="/logo_iferu.png" alt="iferu"  class="nav-img">
					</a>
				</div>
				<style>#header{margin-bottom:0;}
				 .slick-next,.slick-prev{background:grey;}
                 .slick-next:hover,.slick-prev:hover{background:grey;}
				.gsb-cover img{height:200px  !important;}
				.nav-img{height: 65px;}
			.header--prospect-cta .navigation__prospect-action {
    display: inline;}
	@media only screen and (max-width: 600px) {
		#web_btn{
			display:none;
		}
		#mobile_btn{ padding-bottom: 1px; } .btn{padding: 5px 5px;}		.nav-img{height:55px;}
		.top-search__button  {display:none;}
		.header--prospect-cta .navigation__btn-login {display:inline;
  }
  		
}
@media (min-width: 768px){
	#mobile_btn{
			display:none;
		}
		#web_btn{
			padding-top: 2%;
			margin-left: -35px;
		}
		
.navigation__search {height: 60.33px;}}
	#lib{display:none;}		
			</style>
	
				<div class="navigation__menu-wrapper">
					<div class="navigation__menu">
						<ul class="navigation__menu-items">
							
							<li><a  href="/fr-explore.php">Explorer</a></li>
							<li><a  href="/fr-enterprise-solutions.php">Solutions pour entreprises</a></li>
							<li><a  href="/fr-subscribe-products.php">S'abonner</a></li>
							<li><a  id="lib" href="/mylibrary.php">Ma bibliothèque</a></li>
														
							<nav class="pou" id="mobile_btn">
								<li class="deroulantt">
									<strong class="taa">Français</strong>
									<ul class="souss">
										<li class="po">
											<form id="frenchform" action="" method="post">
												<a onClick="javascript:document.getElementById('frenchform').submit();" >English</a>
												<input type="hidden" name="frenchform" >
											</form>
										</li>
									</ul>
								</li>	
							</nav>
						</ul>
						<style>
							.taa{
								font-size: small;
							}
							.pou >  li:hover .souss{
							display: block;
							}
							.souss a {
							padding: 10px;
							border-bottom: none;
							}
							.souss a:hover{
							border-bottom: none;
							}
							.deroulantt > .taa::after{
							content: "▼";
							font-size: 12px;
							}
							.souss{
							display: none;
							position: absolute;
							z-index: 1000;
							}
							.pou .po{
							float: left;
							margin: -43%;
							left: -19px;
							font-size: small;
							position: relative;
							}
							</style>
								
						</ul>
<div class="navigation__search">
<style>

.loader{
  background:white;
  position:fixed;
  z-index:99;
  top:0;
  left:0;
  width:100%;
  height:100%;

  display:flex;
  justify-content:center;
  align-items:center;

  
 

}
.loader > img{
  width:200px;
}


  </style>

<form id="search_form" class="top-search js-top-search" onsubmit="return validate(this);" action=<?php if($current_page=="model_contenu_de_chaine.php"){echo "/search.php";}else{echo "/search.php";} ?> data-ga-search="{
		&quot;CHANNEL&quot; : { 
			&quot;endpoint&quot;: &quot;/search/channels&quot;, 
			&quot;title&quot;: &quot;Chaînes&quot;  
		},
		&quot;SUMMARY&quot; : {  
			&quot;endpoint&quot;: &quot;/search/summaries&quot;, 
			&quot;title&quot;: &quot;Résumés&quot;  
		},
		&quot;LGXPS&quot; : { 
			&quot;endpoint&quot;: &quot;/search/lgxps&quot;, 
			&quot;title&quot;: &quot;Expériences d" apprentissage="" au="" format="" condensé"="" }="" }'="" style="padding: 0;">
	<input type="hidden" name="initial" value="true">
	
	<div class="top-search__container">
		
			<input id="searchinput" class="js-top-search-input top-search__input" name="query" type="search" autocomplete="off" placeholder="Rechercher des résumés..." style="margin: 10px 0 0;" >
			<div class="top-search__controls">
				<button type="reset" class="top-search__reset">×</button>
				<button  id="searchbutton" type="submit" class="btn btn-primary js-top-search-button top-search__button">
					
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="top-search__icon">
						<path d="M31.712 30.272l-9.728-9.76c1.824-2.176 2.912-4.96 2.912-8 0-6.912-5.6-12.512-12.512-12.512s-12.48 5.6-12.48 12.512 5.6 12.48 12.48 12.48c3.136 0 6.016-1.152 8.192-3.072l9.728 9.792c0.384 0.384 1.024 0.384 1.408 0 0.384-0.416 0.384-1.024 0-1.44zM12.384 23.008c-5.792 0-10.496-4.704-10.496-10.496s4.704-10.528 10.496-10.528c5.824 0 10.528 4.704 10.528 10.528s-4.704 10.496-10.528 10.496z"></path>
					</svg>
				</button>
			</div>
		
		<div class="top-search__result-container js-top-search-results"></div>
	</div>
</form>
</div>
						
						<div class="navigation__menu-login">
						   <a href="/fr-login.php" class="btn btn-primary">Connexion</a>
					   </div>
							
						
					</div>
					
				</div>
				<div class="navigation__controls">
					
						<div class="navigation__prospect-action">
							<a href="/fr-subscribe-account.php" class="btn btn-warning" id="essai-gratuit" data-ga-prospect-cta="150">Essai gratuit</a>
						</div>
						<div class="navigation__dropdown-container" style="display:none;">
						<button  href="javascript:;" data-ga-dropdown=".js-navigation-dropdown" class="btn btn-link navigation__btn">
									<i class="ico-user"></i>
									
								</button>
								<div  class="navigation__dropdown js-navigation-dropdown" style="display: none;">
									
									<div class="navigation__dropdown-header">
									<?php
										
										include('poc/pdo.php');
												if(isset($_SESSION['username'])){
													$usern = $_SESSION['username'];
													$user="SELECT user_email FROM user_table WHERE user_name = '".$usern."'";
													  $mail = $pdo->prepare($user);
													  $mail->execute();
													  $email=$mail->fetch();
													 if($email > 0){
														 $user_mail = $email;
													 }
													 			
													echo $usern;
													echo '<div><em>'.$user_mail['user_email'].'</em></div>';
													
												}
											?>
									</div><hr>
									<ul>
										
											<li><a href="/moncompte.php">Mon Compte</a></li>
										
									</ul>
									
								
									<hr>
									
									<ul>
										
											<li><a href="#" id="logout">Déconnexion</a></li>
									</ul>
								</div>
						</div>
						
						<div class="navigation__btn-login">
						
							<a href="/fr-login.php" class="btn btn-user navigation__btn" class="nav-link" id="connexion" rel="nofollow">
							
								<i style="margin-right:10px" class="ico-user"></i>
								
							  </a>
						</div>
						
						
						<nav id="web_btn">	
							<ul>
							<li class="dropdown" id="web_btn" style="font-size:small;">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><strong>Français</strong> <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li>
											<form  id="frenchform" action="" method="post">
												<a onClick="javascript:document.getElementById('frenchform').submit();">English</a>
												<input type="hidden" name="frenchform" >
											</form>	
										</li>	
									</ul>		
								</li>	
							</ul>
						</nav>
						
					
					

				
					
							
			
				
				
				<link rel="shortcut icon" href="http://backend.iferu.com/favicon.ico" type="image/x-icon">
				
				
			
		</nav>
	</header>
	<style>
					.dropdown{
						list-style: none;
						top: -3px;
						padding: 13px;
						position: absolute;
						
					}
					.dropdown-menu{
						list-style: none;
						background-color: none; 
						background-clip: none; 
						border: none; 
						border: none; 
						border-radius: none; 
						-webkit-box-shadow: none; 
						box-shadow: none;
						position: relative;
					}
					.open>.dropdown-menu {
						display: contents;
						position: relative;
					}
					a {
    					color: #333;	
						cursor: pointer;
					}
					a:hover, a:focus {
   						color: black;
    					text-decoration: underline;
					}
					b, strong {
    					font-weight: bold;
					}
          </style>
<?php
// if($bookpage=="resume"){
// include("./change_langage.php");}else{include("../../change_langage.php");}
if($chainepage=="resume"){
	include("change_langage.php");}
	
	else{include("../../change_langage.php");}
?>
	
	

	<script src="/fr-enterprise-solutions_files/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js"></script>
	<script src="/cookies.min.js"></script>
<script>
	$(document).ready(function(){  
		$('.footer__menu').hide();
		var myCookie = docCookies.getItem('visited');
		if (myCookie==null){
			docCookies.setItem('visited', 'true');

		}
		else{
			$('.notifybar').hide();
		}
		$('.notifybar__dismiss').click(function(){ 
			
$('.notifybar').hide();
}); 
  var state= $('#session').val();

  
  if(state=='true'){
	
	
    $('.navigation__dropdown-container').show();
    $('#connexion').hide();
	$('#lib').show();
	$('#essai-gratuit').hide();
	$('.logo_index').attr("href","/fr-explore.php");
  }
  $('#logout,#logout_mobile').click(function(){  
       
        var action = "logout"; 
        
        $.ajax({  
             url:"pdo.php",  
             method:"POST",  
             data:{action:action},  
             success:function()  
             {  
                  location.reload();               
             }  
        });  
		//window.location.href="fr-explore.php";
   }); 
}); 

 </script> 

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71717909-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-71717909-1');
</script>

	  
<div style="display:none;" class="loader">
      <img src="/include/loading.gif" alt="loading..."/></div>
     
	  	
<script>
	
			$('#search_form').submit(function(){
				if($('#searchinput').val() == "") {
					return false;
				 }
				$('.loader').show();
				
			});

</script>
		
			
			<script>/*$('#searchbutton').click(function() {
				if($('#searchinput').val() != '') {
					return;
				 }
     $('#searchbutton').prop('disabled', true);
     $('#searchinput').on('input change', function() {
        if($('#searchinput').val() != '') {
           $('#searchbutton').prop('disabled', false);
        }
     });
 });    
		</script>

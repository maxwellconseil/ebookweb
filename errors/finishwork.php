<?php 
    session_start();
    if($_SESSION["connected"]!==TRUE){   //fikri : si l'utilisateur n'est pas connecté alors il est redirigé vers errors/login.php
        $_SESSION["return"]=$_SERVER['QUERY_STRING'];
        header("Location: ./login.php");
        exit();
    }
    else $corrusername=$_SESSION["Corrusername"];
    include("../poc/pdo.php");
    $number=$_GET['number'];
    $id=$_SESSION["task_id"];
    $line= $pdo->prepare("SELECT * FROM redactor_bookworkstatus where id=?");
    $pdo->prepare("UPDATE redactor_account SET last_activity= now() where username= ?")->execute([$corrusername]);
    $line->execute([$id]);
    $linee=$line->fetch();
       //fikri : changement de l'état de la tâche finie (cliquer sur le bouton "je termine mon travail") selon la tâche actuelle
    if($linee['book_state']=="highlight"){    //fikri : changer l'état à "waiting"(attendre le correcteur à finir la correction et donner l'utilisateur une autre tâche)
        $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where id=?")->execute(["waiting",$id]);
    }
    else if($linee['book_state']=="correction"){   //fikri : changer l'état à "ccompleted" (le correcteur à terminé sa correction et il va prendre une autre tâche)
           //fikri : vérifier si tous les highlights affichés selon la valeur de $number sont corrigés
        $query=$pdo->prepare("SELECT * FROM redactor_highlight a left join redactor_correction b on a.id=b.fk_highlight where a.idDoc=? group by a.selection having count(a.selection)>=? and b.correction is null");
        $query->execute([$linee['bookid'],$number]);
        if($query->fetch()){
            $_SESSION["correrror"]="Les selections ne sont pas tous corrigées! Veuillez terminer la correction de toutes les selections.";
            header("Location: ./book.php?idtest=".$linee['bookid']."&mode=2&number=".$number);
            exit();
        }else{
            $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where id=?")->execute(["ccompleted",$id]);
        }
    }
    else if($linee['book_state']=="vote"){   //fikri : changer l'état à "completed" (l'utilisateur à terminé le vote et il va prendre une autre tâche)
        $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where id=?")->execute(["completed",$id]);
           //fikri : trouver les correction dans le document courant ayant 2 votes négatifs ou plus
        $query=$pdo->prepare("SELECT b.id,b.username,count(c.vote) as nvotes FROM redactor_highlight a inner join redactor_correction b on a.id=b.fk_highlight inner join redactor_vote c on b.id=c.fk_correction where a.idDoc=? and c.vote<0 group by b.id having nvotes>1");
        $query->execute([$linee['bookid']]);
        while($arr=$query->fetch()){
            $pdo->prepare("DELETE from redactor_correction WHERE id = ?")->execute([$arr['id']]);
               //fikri : trouver le correcteur du doc courant
            $cuser=$pdo->prepare("SELECT username from redactor_bookworkstatus where bookid=? AND book_state=?");
            $cuser->execute([$linee['bookid'],"ccompleted"]);
            $cuserdata=$cuser->fetch();
               //fikri : trouver les membres du groupe travaillant sur le doc courant et les classer selon leurs notes
            $query1=$pdo->prepare("SELECT c.username, sum(a.vote) note FROM (select username from redactor_bookworkstatus where bookid=?) c left join redactor_correction b on c.username=b.username left join redactor_vote a on a.fk_correction=b.id group by c.username ORDER BY note desc");
            $query1->execute([$linee['bookid']]);
            $arr1=$query1->fetchAll();
               //fikri : trouver l'utilisateur qui suit le correcteur courant selon la note, si ce dernier était le dernier, le nouveau correcteur sera le meilleur dans le groupe
            for ($i = 0; $i < count($arr1); $i++) {
                if($arr1[$i]['username']==$cuserdata['username']){
                    if($i==count($arr1)-1){
                        $newcorrector=$arr1[0]['username'];
                    }
                    else{
                        $newcorrector=$arr1[$i+1]['username'];
                    }
                }
            }
               //fikri : mettre tous les membre du groupe en attente et le nouveau correcteur en état "correction"
            echo $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state = ? WHERE bookid=?")->execute(["waiting",$linee['bookid']]);
            $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state = ? WHERE bookid=? and username=?")->execute(["correction",$linee['bookid'],$newcorrector]);
        }
    }
    header("Location: ./getNextWork.php");
    exit();
?>
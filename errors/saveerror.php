<?php
session_start(); 
if($_SESSION["connected"]!==TRUE){
 header("Location: ./login.php");
 exit();
 }
 else $corrusername=$_SESSION["Corrusername"];
include("../poc/pdo.php");
// if($_GET['action']=="delete"){
//     $table = fopen('votes-log.csv','r');
// $temp_table = fopen('table_temp.csv','w');

// print_r($sentdata['select']);
// print_r($sentdata);
// $selection = $sentdata['select'] ;
// $correction = $_GET['correction']; // the name of the column you're looking for
// $ip=$sentdata['ip'];
// while (($data = fgetcsv($table, 1000)) !== FALSE){
//     if(similar($data[0],$selection) > 60){
//         $data[3] = $correction;
//         // this is if you need the first column in a row
//         continue;
//     }
//     fputcsv($temp_table,$data);
// }
//  fclose($table);
//  fclose($temp_table);
// rename('table_temp.csv','votes-log.csv');

// }


function similar(string $a, string $b) {
    $equivalency = 0;
    $minLength = (strlen($a) > strlen($b)) ? strlen($b) : strlen($a);    
    $maxLength = (strlen($a)< strlen($b)) ? strlen($b) : strlen($a);    
    for($i = 0; $i < $minLength; $i++) {
        if($a[i] == $b[i]) {
            $equivalency++;
        }
    }
    $weight = $equivalency / $maxLength;
    return ($weight * 100);
}

if($_GET['action']=="remove"){
    $data = $_GET['voteData'];
    $id = $data['id'];
    $user = $corrusername;
    $pdo->prepare("DELETE from redactor_highlight WHERE id = ? AND user = ?")->execute([$id, $user]);
}

else{
    $data = $_GET['voteData'];
    $user= $corrusername;
    $sel = trim($data["select"]);
    $pdo->beginTransaction();
    $pdo->prepare("INSERT INTO redactor_highlight (selection, user, idDoc) VALUES (?,?,?)")->execute([$sel, $user, $data["idDoc"]]);
    $id = $pdo->lastInsertId();
    echo $id;
    $pdo->commit();
}

?>
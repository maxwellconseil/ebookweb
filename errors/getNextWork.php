<?php
    session_start(); 
    if($_SESSION["connected"]!==TRUE){   //fikri : si l'utilisateur n'est pas connecté alors il est redirigé vers errors/login.php
        header("Location: ./login.php");
        exit();
    }
    else $corrusername=$_SESSION["Corrusername"];
    include("../poc/pdo.php");
    $assign_new_task=TRUE;
    $number=5;
    $num=2;
    echo "test1 ";
       //fikri : trouver tous les tâches non terminés (differents de "completed" et "ccompleted")
    $redactor_has_task = $pdo->prepare("SELECT a.id,b.priority,a.username,a.bookid,a.book_state FROM redactor_bookworkstatus a left join redactor_allbook b on a.bookid = b.book_id where username = ? and book_state <> ? and book_state <> ? order by priority");
    $redactor_has_task->execute([$corrusername,"completed","ccompleted"]);
    echo "test2 ";
    while($array = $redactor_has_task->fetch()){
           //fikri : si la tâche est de type "highlight", mettre à jour la date de dernière activité et le rediriger vers le document correspondant
        if($array['book_state']=="highlight"){
            echo "test 4 ";
            $assign_new_task=FALSE;
            $_SESSION["task_id"]=$array['id'];
            header("Location: ./book.php?idtest=".$array['bookid']."&mode=1");
            exit();
        }
           //fikri : si la tâche est de type "correction", mettre à jour la date de dernière activité et le rediriger vers le document correspondant
        else if($array['book_state']=="correction"){
            $query=$pdo->prepare("SELECT * FROM redactor_highlight a left join redactor_correction b on a.id=b.fk_highlight where a.idDoc=? group by a.selection having count(a.selection)>=? and b.correction is null");
            $query->execute([$linee['bookid'],$num]);
            if($query->fetch()){
                $assign_new_task=FALSE;
                echo "test5 ";
                $_SESSION["task_id"]=$array['id'];
                header("Location: ./book.php?idtest=".$array['bookid']."&mode=2");
                exit();
            }else{
                $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where id=?")->execute(["ccompleted",$array['id']]);
                header('Location: ./getNextWork.php');
                exit();
            }
        }
           //fikri : si la tâche est de type "vote", mettre à jour la date de dernière activité et le rediriger vers le document correspondant
        else if($array['book_state']=="vote"){
            $assign_new_task=FALSE;
            echo "test6 ";
            $_SESSION["task_id"]=$array['id'];
            header("Location: ./book.php?idtest=".$array['bookid']."&mode=3");
            exit();
        }
           //fikri : si la tâche est de type "waiting" (en attente d'une correction)
        if($array['book_state']=="waiting"){
            echo "test7 ";
               //fikri : trouver le nombre des membres du groupe ayant l'état "waiting"
            $query=$pdo->prepare("SELECT count(*) as nbr FROM redactor_bookworkstatus where bookid=? and book_state=?");
            $query->execute([$array['bookid'],"waiting"]);
            if($arr=$query->fetch()){
                   //fikri : vérifier si le nombre des membres du groupe ayant l'état "waiting" est égale à $number (le correcteur n'est pas encore affecté)
                if($arr['nbr']==$number){
                    $assign_new_task=FALSE;
                       //fikri : trouver le membre du groupe ayant la meilleure note
                    $query1=$pdo->prepare("SELECT b.username, sum(a.vote) note FROM redactor_correction b left join redactor_vote a on a.fk_correction=b.id where b.username in (select username from redactor_bookworkstatus where bookid=?) group by b.username order by note desc limit 1");
                    $query1->execute([$array['bookid']]);
                       //fikri : s'il y en a un, on lui affecter la tâche de correction, sinon on l'affecte à l'utilisateur courant(dernier utilisateur finissant le highlight)
                    if($arr1=$query1->fetch()){
                       $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where bookid=? and username=?")->execute(["correction",$array['bookid'],$arr1['username']]); 
                    }else{
                        $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where id=?")->execute(["correction",$array['id']]);
                    }
                       //fikri : si le correcteur est l'utilisateur courant, on le redirige vers sa tâche
                    if($arr1['username']==$corrusername){
                        $_SESSION["task_id"]=$array['id'];
                        header("Location: ./book.php?idtest=".$array['bookid']."&mode=2");
                        exit();
                    }else{
                        header('Location: ./getNextWork.php');
                        exit();
                    }
                }
                   //fikri : si le nombre des membres ayant l'état "waiting" est différent de $number (la correction est terminée ou le groupe n'est pas complet)
                else{
                    $query=$pdo->prepare("SELECT * FROM redactor_bookworkstatus where bookid=? and book_state=?");
                    $query->execute([$array['bookid'],"ccompleted"]);
                       //fikri : la correction est terminée
                    if($arr=$query->fetch()){
                        $assign_new_task=FALSE;
                        $pdo->prepare("UPDATE redactor_bookworkstatus SET book_state=? where id=?")->execute(["vote",$array['id']]);
                        $_SESSION["task_id"]=$array['id'];
                        header("Location: ./book.php?idtest=".$array['bookid']."&mode=3");
                        exit();
                    }
                }
            }else{
                $assign_new_task=FALSE;
                echo "Un erreur a été servenu, Veuillez ressayer plus tard.";
            }
            
        }
    }
    echo "test3 ";
       //fikri : le groupe n'est pas complet
    if($assign_new_task){
        echo "test8 ";
           //fikri : trouver les tâches dans lequel l'utilisateur n'est pas connecté depuis 3 jours
        $users=$pdo->prepare("SELECT b.username,b.id,b.bookid,b.book_state FROM redactor_account a inner join redactor_bookworkstatus b on a.username=b.username where timestampdiff(day,a.last_activity,now())>=3 and bookid not in (select bookid from redactor_bookworkstatus where username = ?) and book_state <> ? and book_state <> ? order by book_state,last_activity limit 1");
        $users->execute([$corrusername,"completed","ccompleted"]);
        if($user=$users->fetch()){
            echo "test9 ";
               //fikri : prendre la place de cet utilisateur et le rediriger selon le mode
            $pdo->prepare("UPDATE redactor_bookworkstatus SET username= ? where id= ?")->execute([$corrusername,$user['id']]);
            if($user['book_state']=="highlight"){
                $_SESSION["task_id"]=$user['id'];
                $mode=1;
                header("Location: ./book.php?idtest=".$user['bookid']."&mode=".$mode);
                exit();
            }
            else if($user['book_state']=="correction"){
                $_SESSION["task_id"]=$user['id'];
                $mode=2;
                header("Location: ./book.php?idtest=".$user['bookid']."&mode=".$mode);
                exit();
            }
            else if($user['book_state']=="vote"){
                $_SESSION["task_id"]=$user['id'];
                $mode=3;
                header("Location: ./book.php?idtest=".$user['bookid']."&mode=".$mode);
                exit();
            }
            else if($user['book_state']=="waiting"){
                header("Location: ./getNextWork.php");
                exit();
            }
        }
           //fikri : si personne n'a été déconnecté pendant 3 jours, on lui affecter une nouvelle tâche
        $assign_new_task = $pdo->prepare("SELECT a.book_id,a.priority,b.username,count(b.bookid) nb_user_associated FROM redactor_allbook a left join redactor_bookworkstatus b on b.bookid=a.book_id where a.book_id not in (select bookid from redactor_bookworkstatus where username = ?) GROUP by book_id having nb_user_associated < ? order by a.priority limit 1");
        $assign_new_task->execute([$corrusername,$number]);
        $line3=$assign_new_task->fetch();
        $pdo->beginTransaction();
        $pdo->prepare("INSERT INTO redactor_bookworkstatus (username, bookid, book_state) VALUES (?,?,?)")->execute([$corrusername,$line3['book_id'],"highlight"]);
        $id = $pdo->lastInsertId();
        $pdo->commit();
        $_SESSION["task_id"]=$id;
        header("Location: ./book.php?idtest=".$line3['book_id']."&mode=1");
        exit();
    }
    
?>
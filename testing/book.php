<?php
    session_start();
    if($_SESSION["connected"]!==TRUE){
        $_SESSION["return"]=$_SERVER['QUERY_STRING'];
        header("Location: ./login.php");
        exit();
    }
    if(isset($_GET['number'])) $number=$_GET['number'];
    else $number=2;
    if(isset($_GET['nbrusers'])){
        $nbrusers=$_GET['nbrusers'];
    }
    else $nbrusers=1;
    if(!$_GET['idtest']){
        header("Location: ./getNextWork.php");
        exit();
    }
    
?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2B700tRsUVsHz9QQ7SAuRg2D6vp3KXyD';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->

<style>
    html,body{
        margin:0;
    }
    #header {
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    
    background-color: #fafafa;
    height: 55px;
    border-bottom: 1px solid #d3d3d3;
    overflow: auto;
}

#finish-btn{
    z-index:10000;
    background-color:rgb(74, 74, 212);
    font-size: 14pt;
    padding: 7px 10px;
    border-radius: 6px;
    color: white;
    text-decoration:none;
}
#header-middle {
    width: 50%;
    float: left;
    margin-left: auto;
    margin-right: auto;
    
}

#header-right {
    float: right;
    width: 50%;
    text-align: center;
}
#header-doc{
    display:flex;
    flex-direction:row;
    justify-content:center;
    align-items:center;
    margin:0 8px;
}
#username{
    white-space:nowrap;
    font-weight:bold
}
#logout-link{
    float:right;
    font-weight:bold;
    white-space:nowrap;
    color:white;
    background-color:rgb(208, 37, 32);
    padding: 5px 10px;
    border-radius: 6px;
    text-decoration:none;
}
</style>

<div id="header-doc">
    <div style="width:0;margin-right:auto"><p id="username">Nom d'utilisateur : <?=$_SESSION["Corrusername"]?></p></div>
    <?php if(isset($_SESSION["task_id"]))

    echo "<a id='finish-btn' href='./finishwork.php?number=".$number."'>Je termine le travail</a>";
    ?>
    <div style="width:0;margin-left:auto"><a id="logout-link" href='./logout.php'>Se déconnecter</a></div>
</div>

<?php
if($_GET['mode']==1){
    echo "<div style=\"margin: 10px 20px;color:#FC7307\"><span style=\"font-weight:bold\">Instructions pour la tâche de surlignage :</span> votre objectif ici est de surligner toutes les phrases problématiques qui, selon vous, ont des problèmes de sens/style. Sélectionnez une partie de la phrase et cliquez sur le bouton \"Mark as an error\" pour la marquer comme une erreur. Pour annuler un surlignage, il suffit de cliquer dessus.</div>";
}
else if($_GET['mode']==2){
    echo "<div style=\"margin: 10px 20px;color:#5A18C9\"><span style=\"font-weight:bold\">Instructions de la tâche de correction :</span> votre objectif est de proposer une correction à toutes les phrases surlignées. Passez simplement la souris sur la phrase à corriger et écrivez votre correction dans la fenêtre popup noire.</div>";
}
else if($_GET['mode']==3){
    echo "<div style=\"margin: 10px 20px;color:#98CA32\"><span style=\"font-weight:bold\">Instructions de la tâche de Vote :</span> votre objectif ici est de voter si une correction proposée est acceptable ou non. Pour voter pour une correction, passez votre souris sur la phrase originale surlignée (en jaune), une fenêtre pop-up avec la correction proposée s'affichera, puis cliquez sur oui si vous pensez que c'est correct et sur non si c'est le contraire. </div>";
}
?>



<div id="header">

    <div id="header-middle">
<h1>Ceci est le texte à corriger</h1>
    </div>
    <div id="header-right">
<h1>Ceci est le texte correct mais en anglais</h1>
    </div>
    
</div>

 <?php
$mode = $_GET[mode];
$loadmyhighlight = null;
if(isset($_GET[loadmyhighlight])){
    $loadmyhighlight = $_GET[loadmyhighlight];
}
$ido=$_GET[idtest] - 1500000;
$idt=$_GET[idtest];

?>

<body onresize="sectionAdapter()">
<iframe id="fr_sum" src="/book1.php?outline=true&id=<?php echo $idt ?>&mode=<?php echo $mode?>&number=<?php echo $number?>&loadmyhighlight=<?php echo $loadmyhighlight ?>" onload="func()" frameborder="0" scrolling="no"                           
                                    style="height: 6000%; 
                                                width: 49%; float: left;" height="6000%" width="49%"
                                   align="left">
                                  </iframe>  

    <iframe id="en_sum" src="/book1.php?outline=true&id=<?=$ido ?>&mode=<?=$mode?>&number=<?=$number?>" onload="func()" frameborder="0" scrolling="yes"  
                                        style="overflow: hidden; height: 6000%;

                                        width: 49%;" height="6000%" width="49%"                                 
                                         align="right">
                                        </iframe>
<script>
    var loaded=0
    function func(){
        if(++loaded===2) sectionAdapter()
    }
    var enDoc=document.querySelector("#en_sum").contentWindow.document
    var frDoc=document.querySelector("#fr_sum").contentWindow.document
    var enSec=enDoc.querySelectorAll(".abstract>.col-sm-8")
    var frSec=frDoc.querySelectorAll(".abstract>.col-sm-8")
    var enP=enDoc.querySelectorAll("#abstractText > p, #abstractText > blockquote, #abstractText > ol, #abstractText > ul")
    var frP=frDoc.querySelectorAll("#abstractText > p, #abstractText > blockquote, #abstractText > ol, #abstractText > ul")
    function sectionAdapter(){
        for(var i=2;i<4;i++){
            enDoc=document.querySelector("#en_sum").contentWindow.document
            frDoc=document.querySelector("#fr_sum").contentWindow.document
            enSec=enDoc.querySelectorAll(".abstract>.col-sm-8")
            frSec=frDoc.querySelectorAll(".abstract>.col-sm-8")
            
            if(frSec[i].offsetTop>enSec[i].offsetTop){
                enSec[i].style.marginTop=frSec[i].offsetTop-enSec[i].offsetTop+"px"
            }
            else{
                frSec[i].style.marginTop=enSec[i].offsetTop-frSec[i].offsetTop+"px"
            }
        }
        var l2=enDoc.querySelectorAll("#abstractText > p, #abstractText > blockquote, #abstractText > ol, #abstractText > ul").length
        var l1=frDoc.querySelectorAll("#abstractText > p, #abstractText > blockquote, #abstractText > ol, #abstractText > ul").length
        var i=l1/2,j=l2/2
        while(i<l1 && j<l2){
            enDoc=document.querySelector("#en_sum").contentWindow.document
            frDoc=document.querySelector("#fr_sum").contentWindow.document
            enP=enDoc.querySelectorAll("#abstractText > p, #abstractText > blockquote, #abstractText > ol, #abstractText > ul")
            frP=frDoc.querySelectorAll("#abstractText > p, #abstractText > blockquote, #abstractText > ol, #abstractText > ul")
            if(frP[i].offsetTop>enP[j].offsetTop){
                enP[j].style.marginTop=frP[i].offsetTop-enP[j].offsetTop+"px"
            }
            else{
                frP[i].style.marginTop=enP[j].offsetTop-frP[i].offsetTop+"px"
            }
            enP[j].style.paddingBottom=enP[j].style.marginBottom;enP[j].style.marginBottom=0
            frP[i].style.paddingBottom=frP[i].style.marginBottom;frP[i].style.marginBottom=0
            i++;j++
        }
        var entitleSec=enDoc.querySelectorAll(".abstractTitle")[3]
        var frtitleSec=frDoc.querySelectorAll(".abstractTitle")[3]
        if(frtitleSec.offsetTop>entitleSec.offsetTop){
            entitleSec.style.marginTop=frtitleSec.offsetTop-entitleSec.offsetTop+"px"
        }
        else{
            frtitleSec.style.marginTop=entitleSec.offsetTop-frtitleSec.offsetTop+"px"
        }
    }
</script>
</body>

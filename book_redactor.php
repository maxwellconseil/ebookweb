<?php   
include("../config.php"); 

 session_start(); 
 $corrusername=$_SESSION["Corrusername"];
 ?> 

<!DOCTYPE html>
<!-- saved from url=(0068)/fr/resume/intelligence-economique/32972? -->
<html class="js no-touch" lang="fr">

<head>


<?php
  include("./poc/pdo.php");

      // Select Title of the book 
      if(isset($_SESSION['username'])){
        $usern = $_SESSION['username'];
        $pd = $pdo->prepare("SELECT * FROM user_table WHERE user_name = '".$usern."'");
        $pd->execute();
        $user_data = $pd->fetch();
        $user_date=$user_data['start_date'];   //get the date of registration of the user from the database
        $current_date=gmdate('Y-m-d H:i:s');    //date in gmt 
        $date1 = strtotime($current_date); 
        // $diff = abs(strtotime($user_date)-time());
        //  $diff = abs($date1 - $date2);     //calculate the difference between the two
        // echo $current_date."<br />";
        // echo $user_date."<br />" ;
        // echo $diff."<br />";
        }
      $str= $_SERVER['REQUEST_URI'];

      $gg=explode('/',$str);
      $gg[2]=preg_replace('/(\d+).*/', '$1', $gg[2]);

      $id = $gg[2] ;
      if( empty($gg[2])){ 
        $id=$_GET['id'];
      }     
      $titre = $pdo->prepare("SELECT titre as t FROM Document Where id= ".$id);
      $titre->execute();
      $title = $titre->fetch();

      // Select description of the book
      $desc = $pdo->prepare("SELECT description as descr FROM Document Where id= ".$id);
      $desc->execute();
      $description = $desc->fetch();

       // Select Image URL of the book
       $img = $pdo->prepare("SELECT image FROM Document Where id= ".$id);
       $img->execute();
       $image = $img->fetch();
       $image1=explode('/',$image['image']);
				$ff=explode('.',$image1[2]);
      $originalID=$ff[0];

      // Select authors of the book
      $at="select GROUP_CONCAT(DISTINCT A.nom separator ', ') AS author FROM Auteur A, Document D, Doc_Aut DA WHERE DA.idDoc =".$id." and A.id= DA.idAut";
       $auteur = $pdo->prepare($at);
      $auteur->execute();
      $author = $auteur->fetch(); 
    
    
$des = $description['descr'];
?>

<?php



$str= $_SERVER['REQUEST_URI'];

$gg=explode('/',$str);



if( empty($gg[2])){
  $gg[2]=$_GET['id'];


}
$gg[2]=preg_replace('/(\d+).*/', '$1', $gg[2]);

if(strpos($title['t'],'?' ) !== false){

  $title['t'] = str_replace('?', '', $title['t']);

}
if(strpos($title['t'],'%' ) !== false){

  $title['t'] = str_replace('%', '', $title['t']);

}
if(strpos($title['t'],'/' ) !== false){

  $title['t'] = str_replace('/', '', $title['t']);

}
if(strpos($title['t'],'#' ) !== false){

  $title['t'] = str_replace('#', '', $title['t']);

}

if(strpos($title['t'],'#' ) !== false){

  $title['t'] = str_replace('#', '', $title['t']);

}

if(strpos($title['t'],'/' ) !== false){

  $title['t'] = str_replace('/', '', $title['t']);

}

if(strpos($title['t'],'\\' ) !== false){

  $title['t'] = str_replace('\\', '', $title['t']);

}






$outline=$_GET['outline'];
echo "<input type='hidden' value='$outline'  id='outline'></input>";



 if( substr( $gg[1], 0, 4 ) == 'book' and $outline!='true' or empty(substr( $gg[1], 7, 1) )  ){
  $name = str_replace(' ', '-', $title['t']);

 
     


  
header("Location: /resume-".$name."/".$gg[2], true, 301);
exit();
}
$idaudio = $gg[2] ;

  echo "<input type='hidden' value='$idaudio' id='idaudio'></input>";
if(isset($_GET['number'])) $number=$_GET['number'];
  

   
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Iferu Book : <?php  echo $title['t']; ?></title>
<meta name="description" property="og:description" content="<?php  echo $description['descr']; ?>">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="og:title" content="<?php  echo $title['t']; ?>">
<meta property="og:type" content="article">
<meta property="og:url" content="<?php  echo '/book.php?id='.$id; ?>">
<meta property="og:image" content="<?php  echo "/image_merge.php?id=".$originalID; ?>" >
<meta property="og:image:type" content="image/jpeg" />
<meta name="author" content="<?php echo $author['author'];?>">
<meta property="og:site_name" content="iferu">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@iferu">
<meta name="twitter:title" content="<?php  echo $title['t']; ?>">
<meta name="twitter:description" content="<?php   echo $description['descr'];  ?>">
<meta name="twitter:image" content="<?php  echo "http://iferu.com/image_merge.php?id=".$id; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:app_id" content="143891362343386">
<!-- .col-sm-8.col-sm-push-2{display:none;} -->
<style>.ix--bar{display:none;}.slick-container{display:flex;}</style>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->

<!-- jQuery library -->
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>  -->

<!-- Latest compiled JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<!-- Latest compiled and minified CSS -->
<!--
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<!-- Optional theme -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
 -->
<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!-- <link rel="alternate" href="/oembed?url=https%3A%2F%2Fbackend.iferu.com%2Fsummary%2F32972&amp;format=json" type="application/json+oembed" title="Intelligence économique">
<link rel="alternate" href="/oembed?url=https%3A%2F%2Fbackend.iferu.com%2Fsummary%2F32972&amp;format=xml" type="text/xml+oembed" title="Intelligence économique">
 --><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" defer>
<link rel="search" href="/opensearch.xml" type="application/opensearchdescription+xml" title="iferu" defer>
 <script src="jquery-3.5.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="../include_book/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css" defer>
<link rel="stylesheet" type="text/css" href="../include_book/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css" defer> 
<link rel="stylesheet" type="text/css" href="/fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css" >

<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" defer>-->
<link rel="stylesheet" type="text/css" href="/include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
<?php


  echo "<input type='hidden' value='true' id='session'></input>";
  $idDoc = $gg[2] ;
  if( empty($gg[2])){ 
    $idDoc=$_GET['id'];
  }  
  

  echo "<input type='hidden' value='$idDoc' id='idDoc'></input>";
  $user_name = $_SESSION["username"] ;
  echo "<input type='hidden' value='$user_name' id='user_name'></input>";
  
  /* Hide share button when the user has already sent a book */
  $sent = "SELECT COUNT(*) as n FROM user_sent_books WHERE user_name='".$_SESSION['username']."'";
  $u = $pdo->prepare($sent);
  $u->execute();
  $res = $u->fetch();

  if($res['n'] > 0){
    echo '<style>.ico-share-alt{display:none;}</style>';
   
    
  }
    
  $flag = "SELECT available FROM Document WHERE id=".$idDoc;
  $u = $pdo->prepare($flag);
  $u->execute();
  $res = $u->fetch();

  // if($res['available'] > 0){
  //   echo "<script>window.location.href='/non-book.php';</script>";
  // }

?>
<script>
  function HtmlEncode(s)
{
  var el = document.createElement("div");
  el.innerText = el.textContent = s;
  s = el.innerHTML;
  return s;
}
	window.VWO = window.VWO || [];
	window.VWO.push(['tag', 'Login', 'true', 'session']); 
	_vis_opt_check_segment = {'95': true };
	window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
	window.anonymous = 'true'; /* Used for VWO segmentation */
	</script>
	<script>
	var _vwo_code=(function(){
	var account_id=5083,
	settings_tolerance=2000,
	library_tolerance=2500,
	use_existing_jquery=false,
	// DO NOT EDIT BELOW THIS LINE
	f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script><script src="../include_book/j.php" type="text/javascript"></script>


<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->

<script src="../include_book/f.txt"></script>
<script src="../include_book/f(1).txt"></script>
<script src="../include_book/va-0ca7acdf418d8c12f3819dda65c35024.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include_book/track-0ca7acdf418d8c12f3819dda65c35024.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include_book/opa-56761856850233eb41e36332d7e3cf79.js" crossorigin="anonymous" type="text/javascript"></script><!--<style type="text/css">
  
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }
  }
</style>--></head>
<body class="" data-new-gr-c-s-check-loaded="14.982.0"><!--<div id="hs-feedback-fetcher">
<iframe frameborder="0" src="../include_book/feedback-web-fetcher.html"></iframe></div>


		<script>
			dataLayer = [{
				"PageView": "<?php  echo "/fr/resume/".$title['t']."/".$id; ?>"
				
			}];
		</script>

	-->
  
	<div class="sr-only">
  <a href="<?php  echo "/en/resume/".$title['t']."/".$id; ?>?#main-content">Skip navigation</a>
  </div>	
<?php 
 include ("header.php")

?>

<style>
.navigation__search {
  height: 60px; 
}

</style>

<!--- START MAIN_BODY -->



<ol class="tools" style="background:none;border:none;padding:0;">
<?php
if($_GET['outline']=='true'){

  echo '    <li><button id="btnHighlight" style="color:white;background-color:#c4001d;width:200px;height:30px;border:1px;" class="highlightingText" onclick="highlightingText(event);">Mark as Error</button></li>';
}
else{
  echo '    <li><button class="highlightingText" >Highlight Text</button></li> ';
}
?>
  
    <!-- <li><button class="deleteHighlight">Delete Highlight</button></li> -->
    <!-- <li><button class="postNote">Add Notes</button></li> -->
 </ol> 
<div id="divCorr" class="correction" style="position:absolute;z-index: 10;background-color:rgba(46, 45, 45, 0.856);display:none;padding-left:30px;padding-right:30px;padding-bottom:20px;padding-top:20px;border-top-left-radius: 5px; border-bottom-left-radius: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px; width:450px;"> 
<p id="currentCorr"></p>
<textarea id="txtCorrection" name="correction"rows="6" cols="47" style="width:100%;"></textarea>
<div style="display:flex;flex-direction:row;justify-content:center">
<button style="background-color:#242728; margin-right:5%;margin-top:15px;border: none;color: white;padding: 5px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;" class="correctBtn" onclick="sendCorrection(event)">Corriger</button>
<button style="background-color:#242728; margin-left:5%;margin-top:15px;border: none;color: white;padding: 5px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;" class="correctBtn" onclick="this.parentElement.style.dispay='none'">Fermer</button>

</div>
</div>
<div id="divVote" class="voter" style="width:450px;position:absolute;z-index: 10;background-color:rgba(46, 45, 45, 0.856);display:none;padding-left:30px;padding-right:30px;margin-right:30px;padding-bottom:20px;padding-top:20px;border-top-left-radius: 5px; border-bottom-left-radius: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;"> 
  <p id="setencep" style="color:white;"> La correction propose est : </p> 
  
  <div style="">
    <div class="text" style="color:green;" >est ce que vous etes d'accord avec cette correction : </div>
    <div>
      <div style="display:flex;flex-direction:row;justify-content:center">
        <button id="yes" style="border: none;color: white;padding: 5px 20px;margin:10px 5%;text-align: center;text-decoration: none;display: inline;font-size: 16px; background-color:#242728;" class="voteYes" onclick="sendVoteYes()">Oui</button>
        <button id="no" style="border: none;color: white;padding: 5px 20px;margin:10px 5%;text-align: center;text-decoration: none;display: inline;font-size: 16px; background-color:#242728;" class="voteNo" onclick="sendVoteNo()">Non</button>
      </div><div></div>
    </div>
  </div>
  <div style="display:flex;flex-direction:row;justify-content:center">
    <button style="background-color:#242728;margin-top:15px;border: none;color: white;padding: 5px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;" class="correctBtn" onclick="this.parentElement.style.dispay='none'">Fermer</button>
  </div>
</div>

  <ul class="color">
    <span class="dot" onclick="Highlight('yellow')"></span>
    <!-- <span class="dot2" onclick="Highlight('transparent')"> -->
  </span>
    <!-- <img class="dot3" id='imagex' onclick="Highlight('rgb(3, 242, 250)')">
    <img class="dot4" id='imagex' onclick="Highlight('rgb(255, 0, 212)')"> -->
    <img class="dot2" id='imagex' src="../x-mark.png" onclick="Highlight('transparent')" >
  </ul>
<style>
  .container
{
  width:200px;
  border:1px solid green; 
  overflow: hidden;
}
.text
{
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    display: block; 
}
  /* .correction{
    display:none;
  } */
  .affix {
    position: sticky;
}
</style>
<?php 

$gg[2]=preg_replace('/(\d+).*/', '$1', $gg[2]);

if( empty($gg[2])){
  $gg[2]=$_GET['id'];


}
$book = file_get_contents("./all_book/".$gg[2]."-h.html");
$sumary = file_get_contents("./all_book/".$gg[2].".html");
$res = explode('<div class="clearfix" itemprop="review" itemscope itemtype="http://schema.org/Review">',$book);

 //echo $sumary;
 $url='backend.iferu.com/en/en-book.php?id='.$id;
$url1 = urlencode($url);


echo  $res[0];

//echo '<div class="multi-button"><button><i class="fa fa-amazon"></i><a href="http://www.facebook.com/sharer.php?u='.$url.'" target="_blank">Facebook</a></button>';
//echo '<button><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url='.$url.'"  target="_blank">LinkedIn</a></button>';
//echo '<button><a href="https://twitter.com/share?url='.$url.'"  target="_blank">Twitter</a></button></div>';
?>
<h2 style="text-align:center">Partager via :</h2><br>
<div class="shareButtons" style="margin-left:44%">
<a style="margin-right:15px"  href="http://www.facebook.com/sharer.php?u=<?php echo $url1?>" target="_blank">
<button>
<i class="fa fa-facebook-official"></i></button></a>
<a style="margin-right:15px;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url1 ?>" 
target="_blank">
<button><i class="fa fa-linkedin-square"></i></button></a>
<a href="https://twitter.com/share?url=<?php echo $url1?>" target="_blank">
<button><i class="fa fa-twitter-square"></i></button></a>
 </div>
 <hr id="hr" style="display:none;">

 <div class="alert-danger" style="display:none; text-align:center;" id="data-msg"></div>
  <form style="display:none; text-align:center;" id="usernameForm" method="post">
    <label>Receiver</label>
    <input type="text" name="usernameS" id="usernameS">
    <input type="submit" id="sbmt" value="OK">
  </form>

  <?php
  if(1){//3days in seconds
    ?>
    <!-- <br></br><br></br> -->
      <!-- <h2 id="paypal_price">4.90 euros/mois</h2> -->
      <!-- <div style="margin: 0 auto;" id="paypal-button-container-P-9X052767J83845001MCZ7QMA"></div> -->
    <!-- <script src="https://www.paypal.com/sdk/js?client-id=AdzlnX9JURZ70uKzi5FPUhIXUaoiJM7CkOm-K9Oe8DEGJjllUa5S-S2Enkl_km5DN47Z9eUVCb9-FDUH&vault=true&intent=subscription" data-sdk-integration-source="button-factory"></script> 
    <script> paypal.Buttons({ style: { shape: 'rect', color: 'gold', layout: 'vertical', label: 'subscribe' }, createSubscription: function(data, actions) { return actions.subscription.create({ /* Creates the subscription */ plan_id: 'P-9X052767J83845001MCZ7QMA' }); }, onApprove: function(data, actions) { alert(data.subscriptionID); } }).render('#paypal-button-container-P-9X052767J83845001MCZ7QMA'); // Renders the PayPal button </script>  -->
    <style>
/* #paypal-button-container-P-9X052767J83845001MCZ7QMA{
  width: 50%;
    margin: 0 auto;
    
}#paypal_price{text-align:center;}
li.footer__list-item::before{
  display:none;
} */
</style>


  <link rel='stylesheet' href='/stylehighlight.css' defer />

<!-- <input type="button" id="toggleBold" value="Toggle BOLD" /> -->

  <!-- <ul class="note" id="note" >
    <li>
      <textarea autofocus></textarea>
    </li>
    <li>
      <button>Add Note</button>
    </li>
  </ul> -->
<!-- 
<nav class="navbar">
      <li class="closeBtn" style="padding: 0 0 0 0">&times;</li>
      <h1>Notes</h1>
      <div class="notes" id="notes">

      </div>
    </nav>
    <div id="bars" style="width: 30px; cursor: pointer;">
        <i class="fa fa-bars" aria-hidden="true" style="font-size: 35px"></i>
    </div> -->

     <?php


echo $sumary ;
echo explode('<div class="clear"></div>',$res[1])[1];
} 



  
  ?>
 
 <script>

$("h3 ").text('Chaînes associées').hide();
    $('.ga-channel-box').hide();
    //$('#shr').insertAfter('.sump-buttons');
    //$('#divShare').insertAfter('#shr');
    $('.ga-channel-box-changed').show();
    $('.chaine-changed').show();
    $('.row-changed').show();
    // $('.col-sm-8.col-sm-push-2').hide();
    $('.row-changed').show();
</script>
<?php 


 include './chaine_dynamique.php';
  include './commenter_resumer.php';
?>
 


 <script defer>
  if($('div.col-sm-12.message').length){
    $('div.text-center').hide();
    $('h2').text('Partager via :').hide();
      $('h2').text('Partager via :').nextAll().hide();
  }
</script>



<!--- END MAIN_BODY -->
<!--- END WRAPPER -->

<div id="fb-root"></div>


<script src="../include_book/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js" defer></script>
<script src="../include_book/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js" defer></script>
<script src="../include_book/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js" defer></script>
<script src="../include_book/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js" defer></script>
<script src="../include_book/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js" defer></script>

<script src="../include_book/plugins-cb-rqhn3ki7bf6j5lwmkcckeu85uqiuj47.js" defer></script>
<script src="../include_book/main-cb-tqm751dbrn8x06irjukcpdj53han94l.js" defer></script>

<script>
	var hy = document.createElement('script'); hy.type = 'text/javascript'; hy.setAttribute("async", "async");
	hy.src = "/www/js/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js";
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hy, s);
</script>
<script defer>
  if($('div.col-sm-12.message').length){
    $('div.text-center').hide();
    $('h2').text('Partager via :').hide();
      $('h2').text('Partager via :').nextAll().hide();
  }
</script>
<script>
 	
 	$(function(){
 		$.gaNotificationInit("/fr/notification/check?jsp=%2fWEB-INF%2fviews%2fabstracts%2fsummary-page.jsp", "/fr/notification/dismiss/{notificationId}");
 	});
</script>
<style>#logout{display:none;}

.overallRating {
    visibility:collapse;
}
.overallRating span {
    visibility:visible;
}
.abstract ul{
  margin:0;
}
.abstract p, .abstract .abstractText{
  margin-bottom:0;
  padding-bottom:1em;
  position : relative;
}
/* .abstract {
  display:none;
} */
</style>

 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.0/jspdf.umd.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.2.6/purify.min.js"></script>  -->
<!-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jspdf-html2canvas@latest/dist/jspdf-html2canvas.min.js"></script> -->
<style> span {
  cursor: pointer;
}
h2{margin-top:0}
</style>
<script>  

var url_string = window.location.href; 
var url = new URL(url_string);
var c = url.searchParams.get("mode");
var loadmyhighlight=0;
if(c == 1){
loadmyhighlight=1;
}


var pageX;
var pageY;
var editor;
var voteSel;
var ipCorr;
var countCorr = 0;
var countVotes = 0;
var corrs = [];
var selcs = [];
var votes = [];
var highlights = [];
var lstCorrs = [];
var selRange;
var correction;
var txtHtml;
var target;
var idTables = [];
var strongSel = null;
var txtSell = null;
var txt = document.getElementsByTagName("body")[0];
document.getElementsByTagName("body")[0].normalize()
txt.innerHTML = txt.innerHTML.split('<em> iferu').join(' <em>Iferu').split('iferu').join('Iferu');
$(document).ready(function () {
  // mafkoud : chargement du Body element.
  
  // mafkoud : chagement de iferu par Iferu.
  

  

  // alert($("blockquote").text().length);
  // alert($("[itemprop='reviewRating']:eq(2)" ).html());
  $("[itemprop='reviewRating']")
  .contents()
  .filter(function() {
    return this.nodeType == 3; //Node.TEXT_NODE
  }).remove();
  $(".recommendation")
  .contents()
  .filter(function() {
    return this.nodeType == 3; //Node.TEXT_NODE
  }).remove();
  // $( "[itemprop='reviewRating']:eq(2)" ).css( "color", "red" );
   $(".abstractText strong").find("strong").parent().map(function() { 
        $(this).contents().unwrap();
        return $(this).parent().html(); 
      }).get();
      console.log($(".abstract").find("strong").map(function() {
        if( $(this).text().length>100){
          $(this).contents().unwrap();
        }
      return $(this).text().length; 
    }).get()); 
  var tempoutline=$('#outline').val();
      const name = "outline=";
      const cDecoded = decodeURIComponent(document.cookie); //to be careful
      const cArr = cDecoded .split('; ');
      var outlineFunc;
      cArr.forEach(val => {
          if (val.indexOf(name) === 0) outlineFunc = val.substring(name.length);
      })
  if( tempoutline=='true'){
    outlineFunc='true';
  }

  if(outlineFunc=='true'){

  $('body > :not(.abstract)').hide(); //hide all nodes directly under the body
  var abstractClean=$('.abstract').clone();
  $(abstractClean).appendTo('body');
  $('.shareButtons').hide();


  $('h2[style="text-align:center"]').hide();
}
  var $nav = $('.navbar');
  var $bars = $('#bars');
  var $closeBtn = $('.closeBtn');
  var $main = $('.abstract');
  
  $bars.click(function () {
    $nav.css('width', "250px");
    $main.css({'marginLeft':'250px'});
    $nav.css('backgroundColor',"black")
  });
  $closeBtn.click(function () {
    $nav.css('width', '0');
    $main.css('marginLeft', '3%');
    $nav.css('backgroundColor',"rgba(0,0,0,0)")
  });
  
  $(document).on("mousedown", function (e) {
    
    pageX = e.pageX;
    pageY = e.pageY;

    $('ol.tools').fadeOut(200);
  });
  function fragmentToString(frag){
    var div=document.createElement('div')
    div.appendChild(frag)
    return div.innerHTML
  }
  function isbackward(sel){
    var range = document.createRange();
      range.setStart(sel.anchorNode, sel.anchorOffset);
      range.setEnd(sel.focusNode, sel.focusOffset);

      var backwards = range.collapsed;
      range.detach();
      return backwards
  }
  $('.abstract').on('mouseup touchend', function () {
    noteHighlight();                    //
    $('textarea.note').fadeOut(200);    //so that any existing instance of note and noteHighlight will be removed when new highlight is selected
    $('ol.tools').fadeOut(200);         //
    var sel;

// Check for existence of window.getSelection() and that it has a
// modify() method. IE 9 has both selection APIs but no modify() method.

  if (window.getSelection && (sel = window.getSelection()).modify) {
    sel = window.getSelection();
    if (!sel.isCollapsed) {
      // Detect if selection is backwards
      var range = document.createRange();
      range.setStart(sel.anchorNode, sel.anchorOffset);
      range.setEnd(sel.focusNode, sel.focusOffset);

      var backwards = range.collapsed;
      range.detach();

      // modify() works on the focus of the selection

      var endNode = sel.focusNode, endOffset = sel.focusOffset - 4;
      sel.collapse(sel.anchorNode, sel.anchorOffset);
      if (backwards) {
        sel.modify("move", 'backward', "character");
        sel.modify("move", 'forward', "sentence");
        sel.extend(endNode, endOffset);
        sel.modify("extend", 'forward', "character");
        sel.modify("extend",'backward', "sentence");
      } else {
          sel.modify("move", 'forward', "character");
          sel.modify("move", 'backward', "sentence");
          sel.extend(endNode, endOffset);
          sel.modify("extend", 'backward', "character");
          sel.modify("extend", 'forward', "sentence");
      }
      
           
    }
  } else if ( (sel = document.selection) && sel.type != "Control") {
    
      var textRange = sel.createRange();
      if (textRange.text) {
          textRange.expand("word");
          // Move the end back to not include the word's trailing space(s),
          // if necessary
          while (/\s$/.test(textRange.text)) {
              textRange.moveEnd("character", -1);
          }
          textRange.select();
      }
  }
  var sel = window.getSelection();
  var range = sel.getRangeAt(0);
  strongSel = sel;
  var selection=fragmentToString(range.cloneContents());
  console.log(selection)
  var ind=0,notfound=txt.innerHTML.indexOf(selection)<0;
  console.log(notfound)
  while(notfound && ind<10){
    console.log("yesyes")
    if(selection.search(/^\<[^>]+>[^\<]*\<\/[^>]+>/)>=0){
      if(isbackward(sel)){
        sel.modify("extend", 'backward', "sentence")
      }else{
        sel.modify("move", 'backward', "sentence")
        sel.modify("move", 'backward', "sentence")
        sel.modify("extend", 'forward', "sentence")
        sel.modify("extend", 'forward', "sentence")
      }
      
    }
    else if(selection.search(/\<[^>]+>[^\<]*\<\/[^>]+>$/)>=0){
      if(isbackward(sel)){
        sel.modify("extend", 'forward', "sentence")
        sel.modify("extend", 'forward', "sentence")
      }else{
        sel.modify("extend", 'forward', "sentence")
      }
    }
    range=sel.getRangeAt(0)
    selection=fragmentToString(range.cloneContents())
    notfound=txt.innerHTML.indexOf(selection)<0
    ind++
  }
  voteSel = sel.toString();
  selRange = range;

  var offsets = range.getBoundingClientRect();
  var bwidth = document.getElementById("btnHighlight").offsetWidth;

  editor = { "startContainer": range.startContainer, "startOffset": range.startOffset, "endContainer": range.endContainer, "endOffset": range.endOffset };
  <?php if($_GET['id']>1500000 || $_GET['outline']!='true'): ?> //render highlight functionality only if the summary is in french  (Début)
  if (sel != '' ) {
    if(c==1){
      $('ol.tools').css({
        'left': offsets.right * 0.5 - (bwidth/2) - 50,
        'top': offsets.top + window.scrollY - 40,
      }).fadeIn(200);
    }
  } else {
    sel.removeAllRanges();
    $('ol.tools').fadeOut(200);
    // $('ul.color').fadeOut(200);
    noteHighlight();
    $('.note').fadeOut(200);
    $('textarea').val('');
  }
  <?php endif; ?> //render highlight functionality only if the summary is in french (Fin)
  });
});
<?php if($_GET['id']>1500000 || $_GET['outline']!='true'): ?> //render highlight functionality only if the summary is in french (Début)
if(c == 1){
  if(loadmyhighlight == null || loadmyhighlight == 1){
    userHighlights();
  }
  function highlightingText(event){
    Highlight('yellow');
    var finalSel = null;
    selRange.startContainer.normalize()
    var nodes = selRange.startContainer.childNodes;
    var array = Array.prototype.slice.call(nodes);
    
    for(i=0;i<array.length;i++){
      var txt = array[i].textContent.replace(/\s+/g, "");
      var tstSel = voteSel.replace(/\s+/g, "");
      if(txt.includes(tstSel) || txt == tstSel){
        finalSel = array[i].innerHTML.toString();
      }
    }
   if(finalSel == null){
     finalSel = voteSel;
   }

    // if(typeof commonAncestor.innerHTML != "undefined"){
    //     decoupage du paragraph en plusieurs phrase
    //     var sentences = commonAncestor.innerHTML.split(/([.])/);
    //     for(i=0;i < sentences.length ;i++){
    //       supression de tt les tag html
    //       var strSel = sentences[i].replace(/(<([^>]+)>)/ig, "");
    //       if(strSel.includes(voteSel.substring(0,voteSel.length - 2))){
    //         finalSel = sentences[i].concat('. ');
    //         break;
    //       }
    //     }
    // }
  
    // finalSel = finalSel.replace(/\<(?!strong|em|i|sub|sup|mark|small|del|ins|a|b|\/strong|\/em|\/i|\/sub|\/sup|\/mark|\/small|\/del|\/ins|\/b|\/a).*?\>/g,'');
   
    if(selRange != null){
      var idDoc= $('#idDoc').val();
      var usr = 'redacteur1';
      while(finalSel.endsWith(' ')){
        finalSel = finalSel.substring(0,finalSel.length - 1);
      }
      while(finalSel.startsWith(' ')){
        finalSel = finalSel.substring(1,finalSel.length);
      }
      var voteData= {
        "select":finalSel,
        "idDoc": idDoc
      };

      $.ajax({
        url: '../errors/saveerror.php',
        data:{voteData:voteData}, 
          type: 'GET'
      });

    }
  }

  $(document).on('click', 'span', function (event) {
    voteSel = event.target.innerHTML;
    Unhighlight(voteSel);
    $(this).contents().unwrap();
  });

  
}else if(c == 2){

  $(document).on('click',  function (event) {
    hidePopUp();
  });

  function hover(element) {
    if(element.style.backgroundColor != 'green'){
      element.style = "background-color:red;";
    }
  }
  function unhover(element) {
    if(element.style.backgroundColor != 'green'){
      element.style = "background-color:yellow;";
    }
  }
  loadCorrections1(highlightErrors);
}else if(c == 3){
  $(document).on('click', function (event) {
    hidePopUp();
  });
  function hover(element) {
    var color = true;
    if(element.style.backgroundColor == 'green'){
      color = false;
    }
    if(element.style.backgroundColor == 'red'){
      color = false;
    }
    if(color == true){
      element.style = "background-color:#33BEFF;";
    }
  }
  function unhover(element) {
    var color = true;
    if(element.style.backgroundColor == 'green'){
      color = false;
    }
    if(element.style.backgroundColor == 'red'){
      color = false;
    }
    if(color == true){
      element.style = "background-color:yellow;";   
    }
  }
  highlightVotes();
}

function loadHighlights(){
  var usr = '<?php echo $corrusername?>';
  $.ajaxSetup({
      async: false
  });
  $.getJSON('/loadCorrections.php?number=<?php echo $number?>&idDoc='+$('#idDoc').val()+'&loadmyhighlight='+loadmyhighlight, function(data) {
    highlights = [];
    $.each(data, function(index) {
      if(data[index].username == usr){
        highlights.push(data[index].highlight);
        idTables.push(data[index].id);
      }
    });
  });
  
}
function userHighlights(){
  loadHighlights();
  $('.correction').css({
    'hidden': true,
  }).fadeOut(0);
  var txt = document.getElementsByTagName("body")[0];
  var innerHTML = txt.innerHTML;
  for(i=0;i<highlights.length;i++){
    var text = HtmlEncode(highlights[i]).replace(/&lt;/g,"<").replace(/&gt;/g,">");
    if(text.endsWith(' ')){
      text = text.substring(0,text.length - 1);
    }
    if(text.startsWith(' ')){
      text = text.substring(1,text.length);
    }
    var index = innerHTML.indexOf(text);
    console.log(text)
    if (index >= 0) { 
      
        innerHTML = innerHTML.replace(text,"<span style='background-color: yellow;'>"+text+"</span>");
        txt.innerHTML = innerHTML;
    }else{console.log("hhh")}
  }
  $("#vote").attr('checked', true);
}

function Unhighlight(selection) {
  var action="remove";
  var idDoc= $('#idDoc').val();
  var usr = '<?php echo $corrusername?>';
  var idHighlight = null;

  selection = selection.split("<span style='background-color: yellow;'>").join('');
  selection = selection.split("</span>").join('');

  $.ajaxSetup({
        async: false
  });

  $.getJSON('/loadCorrections.php?number=1&idDoc='+$('#idDoc').val()+"&loadmyhighlight=1", function(data) {
    $.each(data, function(index) {
      if(data[index].highlight == selection && data[index].username == usr ){
        idHighlight = data[index].id;
        return false;
      }
    });
  });
  
  var voteData= {
    "id":idHighlight,
  };

  $.ajax({
    url: './errors/saveerror.php',
    data:{voteData:voteData,action:action}, 
    type: 'GET'
  });
}

function highlightErrors(highlight){
  var txt = document.getElementsByTagName("body")[0];
  var innerHTML = txt.innerHTML;
    var inner = innerHTML;
    //supression d espce a la fin de la phrase
    var text = highlight
    while(text.endsWith(' ')){
      text = text.substring(0,text.length - 1);
    }
    //supression d espce en debut de phrase
    while(text.startsWith(' ')){
      text = text.substring(1,text.length);
    }
    console.log(text)
    var index = inner.indexOf(text);
    console.log(index)
    if (index >= 0) { 
      innerHTML = inner.replace(text,"<span class='thisSpan' onclick='Unhighlight(); return false' onmouseenter='correctSentence(event);hover(this);' onmouseout='unhover(this);' style='background-color: yellow;'>"+text+"</span>");
      txt.innerHTML = innerHTML;
    }
  $("#correct").attr('checked', true);
}

function highlightVotes(){
  loadCorrections2();
  $('.correction').css({
    'hidden': true,
  }).fadeOut(0);
  var txt = document.getElementsByTagName("body")[0];
  var innerHTML;
  for(i=0;i<highlights.length;i++){
    innerHTML = txt.innerHTML
    var text = highlights[i];
    // var text = highlights[i].replace('<em>','');
    // text = text.replace('</em>','');
    // var inner = innerHTML.replace('<em>','');
    // inner = inner.replace('</em>','');
    if(text.endsWith(' ')){
      text = text.substring(0,text.length - 1);
    }
    if(text.startsWith(' ')){
      text = text.substring(1,text.length);
    }
    var index = innerHTML.indexOf(text);
    if (index >= 0) { 
        innerHTML = innerHTML.replace(text,"<span class='thisSpan' onclick='Unhighlight(); return false' onmouseenter='correctSentence(event);hover(this);' onmouseout='unhover(this);' style='background-color: yellow;'> "+text+" </span>");
        txt.innerHTML = innerHTML;
    }
  }
  $("#vote").attr('checked', true);
}

function hidePopUp(){
  if(c == 2){
    $('.correction').css({
      'hidden': true,
    }).fadeOut(0);
  }
  if(c == 3){
    $('.voter').css({
      'hidden': true,
    }).fadeOut(0);
  }
 
}
function correctSentence(event){
  var currentCorrs = null;
  var curredId = null;

  if(c == 2){
    target = event.currentTarget;
    txtSell = event.target.innerHTML.replace(/(<([^>]+)>)/ig, "");
    voteSel = event.target.innerHTML;

    if(voteSel.startsWith(' ')){
      voteSel = voteSel.substring(1,voteSel.length);
    }

    for(i=0;i<corrs.length;i++){
      if(selcs[i] == voteSel){
        currentCorrs = corrs[i];
        break;
      }
    }

    if(currentCorrs != null){
      document.getElementById("currentCorr").textContent = "La correction propose est : ".concat(currentCorrs.replace(/(<([^>]+)>)/ig, ""));
      document.getElementById("currentCorr").style.color = "white";
      document.getElementById("txtCorrection").value = txtSell;
      document.getElementById("txtCorrection").style.color = "red";
    }else{
      document.getElementById("currentCorr").textContent = "La phrase a corriger est : ".concat(voteSel.replace(/(<([^>]+)>)/ig, ""));
      document.getElementById("txtCorrection").value = txtSell;
      document.getElementById("txtCorrection").style.color = "red";
      document.getElementById("currentCorr").style.color = "red";
    }
    currentCorrs = null;
    curredId = null;
    var offsets = event.currentTarget.getBoundingClientRect();
    
    var top = offsets.top;
    var theight = document.getElementById("txtCorrection").offsetHeight;
    var pheight = document.getElementById("currentCorr").offsetHeight;
    $('.correction').css({
      'hidden': false,
      'left': 200,
      'top':  top - 110 - theight - pheight,
    }).fadeIn(200);
  
  }
  else if(c == 3){
    voteSel = event.target.innerText;
    target = event.currentTarget;
    loadVotes(voteSel);
    document.getElementById("setencep").textContent = "La correction proposer est : ".concat(correction);
    var offsets = event.currentTarget.getBoundingClientRect();
    var pheight = document.getElementById("setencep").offsetHeight;
    var top = offsets.top;
    $('.voter').css({
      'hidden': false,
      'left': 200,
      'top':  top - 130 - pheight,
    }).fadeIn(200);
  }  
}

function similar(a, b) {
    var equivalency = 0;
    var minLength = (a.length > b.length) ? b.length : a.length;    
    var maxLength = (a.length < b.length ) ? b.length : a.length;   

    for(i = 0; i < minLength; i++) {
        if(a[i] == b[i]) {
            equivalency++;
        }
    }

    var weight = equivalency / maxLength;
    return (weight * 100);
}

function loadVotes(s){
  //fix bug correction load when a highlight and correction highlight 
  //doesnt match because of a space in the end of a string.
  
  while(s.endsWith(' ')){
    s = s.substring(0,s.length - 1);
  }
  if(s.startsWith(' ')){
    s = s.substring(1,s.length);
  }
  for(i=0;i < highlights.length ;i++){
    var txtTst = highlights[i].replace(/(<([^>]+)>)/ig, "");
    var txt = s.replace(/(<([^>]+)>)/ig, "");
    txt = txt.replace(/\s+/g, "");
    txtTst = txtTst.replace(/\s+/g, "");
    if(txtTst == txt || txtTst.includes(txt)){
      correction = lstCorrs[i].replace(/(<([^>]+)>)/ig, "");
    }
  }
}
function loadCorrections1(callback){
  
  $.ajaxSetup({
      async: false
  });

  $.getJSON('/loadCorrections.php?m=1&number=<?php echo $number?>&idDoc='+$('#idDoc').val(), function(data) {
    highlights = [];
    var txt = document.getElementsByTagName("body")[0];
    var innerHTML = txt.innerHTML;
    console.log(data)
    $.each(data, function(index) {
        //test si la correction pour chaque highlight est null
        if(data[index].highlight != null && data[index].correction == null){
          var text = HtmlEncode(data[index].highlight).replace(/&lt;/g,"<").replace(/&gt;/g,">");
          console.log(text)
          highlights.push(text);
          idTables.push(data[index].id);
          callback(text);
         
        }
      });
  });
  
}
function loadCorrections2(){
  
  $.ajaxSetup({
      async: false
  });
  $.getJSON('/loadVotes.php?number=<?php echo $number?>&idDoc='+$('#idDoc').val(), function(data) {
    highlights = [];
    $.each(data, function(index) {
      lstCorrs.push(data[index].correction);
      highlights.push(HtmlEncode(data[index].highlight).replace(/&lt;/g,"<").replace(/&gt;/g,">"));
      idTables.push(data[index].id);
    });
  });
  
}
function sendVoteYes(){
  target.innerHTML = target.innerHTML.split("<span class=\"thisSpan\" onclick=\"Unhighlight(); return false\" onmouseover=\"correctSentence(event);hover(this);\" onmouseout=\"unhover(this);\" style=\"background-color: yellow;\">").join('');
  target.innerHTML = target.innerHTML.split("</span>").join('');
  if(target != null){
    target.style.backgroundColor = 'green';
  }
  var idT = null;
  while(voteSel.endsWith(' ')){
    voteSel = voteSel.substring(0,voteSel.length - 1);
  }
  
  for(i=0;i < highlights.length;i++){
    var txtTst = highlights[i].replace(/(<([^>]+)>)/ig, "");
    if(txtTst.endsWith(' ')){
      txtTst = txtTst.substring(0,txtTst.length - 1);
    }
    if(voteSel.endsWith(' ')){
      voteSel = voteSel.substring(0,voteSel.length - 1);
    }
    if(txtTst == voteSel.replace(/(<([^>]+)>)/ig, "")){
      idT = idTables[i];
    }
  }
  var idDoc= $('#idDoc').val();
  var usr = 'redacteur1';
  var sentence = voteSel.split("<span class='thisSpan' style='background-color: yellow;'>").join('');
  sentence = sentence.split("</span>").join('');
 
  var voteData= {
  "vote": 1,
  "id": idT,
  };
  $.ajax({
    url: './errors/savevote.php',
    data:{voteData:voteData}, 
    type: 'GET'
  });
 
  $('.voter').css({
    'hidden':true,
  }).fadeOut(200);
  }
function sendVoteNo(){
  target.innerHTML = target.innerHTML.split("<span class=\"thisSpan\" onclick=\"Unhighlight(); return false\" onmouseover=\"correctSentence(event);hover(this);\" onmouseout=\"unhover(this);\" style=\"background-color: yellow;\">").join('');
  target.innerHTML = target.innerHTML.split("</span>").join('');
  if(target != null){
    target.style.backgroundColor = 'red';
  }
  var idT = null;
  for(i=0;i < highlights.length;i++){
    var txtTst = highlights[i].replace(/(<([^>]+)>)/ig, "");
    if(txtTst.endsWith(' ')){
      txtTst = txtTst.substring(0,txtTst.length - 1);
    }
    if(voteSel.endsWith(' ')){
      voteSel = voteSel.substring(0,voteSel.length - 1);
    }
    if(txtTst == voteSel.replace(/(<([^>]+)>)/ig, "")){
      idT = idTables[i];
    }
  }

  var idDoc= $('#idDoc').val();
  var usr = 'redacteur1';
  var sentence = voteSel.split("<span class='thisSpan' style='background-color: yellow;'>").join('');
  sentence = sentence.split("</span>").join('');

  var voteData= {
    "vote": -1,
    "id":idT,
  };
  $.ajax({
    url: './errors/savevote.php',
    data:{voteData:voteData}, 
    type: 'GET'
  });
  $('.voter').css({
    'hidden':true,
  }).fadeOut(200);
}

function sendCorrection(){
  var input = document.getElementById("txtCorrection").value;
  if(target != null){
    target.style.backgroundColor = 'green';
  }

  var idT = null;

  for(i=0;i < highlights.length;i++){
    var text = highlights[i].replace(/(<([^>]+)>)/ig, "");
    var txtSelec = voteSel.replace(/(<([^>]+)>)/ig, "");
    if(text.endsWith(' ')){
      text = text.substring(0,text.length - 1);
    }
    if(txtSelec.endsWith(' ')){
      txtSelec = txtSelec.substring(0,txtSelec.length - 1);
    }
    if(text.startsWith(' ')){
      text = text.substring(1,text.length);
    }
    if(txtSelec.startsWith(' ')){
      txtSelec = txtSelec.substring(1,txtSelec.length);
    }
    if(text == txtSelec){
      idT = idTables[i];
    }
  }

  var isCorrected = false;

  if(corrs.length != null){
    for(i=0;i < corrs.length;i++){
      if(selcs[i] == voteSel){
        isCorrected == true;
        corrs[i] = input;
      }
    }
    if(!isCorrected){
      corrs.push(input);
      selcs.push(voteSel);
    }
  }else{
    corrs.push(input);
    selcs.push(voteSel);
  }
 
  var idDoc= $('#idDoc').val();
  var usr = '<?php echo $corrusername?>';
    var voteData= {
      "idDoc": idDoc, 
      "correction": input,
      "id": idT,
    };
    $.ajax({
      url: '../errors/savecorrection.php',
      data:{voteData:voteData}, 
      type: 'GET'
    });
  
  $('.correction').css({
    'hidden':true,
  }).fadeOut(200);
}
// $('.correctText').click(function(e){
//   var div=document.createElement("div")
//   div.style="position:absolute;width:200px;height:200px;background-color:blue;top:"+e.pageY+"px"+";left:"+e.pageX
  
// });

// function getCorrections(){
//   corrs = [];
//   votes = [];
//   countVotes = 0;
//   countCorr = 0;
//   $.ajaxSetup({
//     async: false
//   });
    
//   $.getJSON('/loadVotes.php?username=redacteur1', function(data) {
    
//     $.each(data, function(index) {
//         var tst = true;
//         if(data[index].idDoc ==  $('#idDoc').val() && data[index].highlight == voteSel){
//           countVotes = 0;
//           for(i=0;i<corrs.length;i++){
//             if(corrs[i] == data[index].correction){
//               countVotes = votes[i];
//               countVotes++;
//               votes[i] = countVotes;
//               tst = false;
//             }
//           }
//           if(tst){
//             countVotes++;
//             corrs.push(data[index].correction);
//             votes.push(countVotes);
//             countCorr++;
//           }
//         }

//       });
//   });

//   $.ajaxSetup({
//       async: true
//   });
// }



// var thisCorr;

//      else if(selectedValue == "correct"){
//     getCorrections();
//     var userSelection = window.getSelection().getRangeAt(0);
//     var rect = userSelection.getBoundingClientRect();
//     document.getElementById("pCorr").innerHTML = "Nombre de correction : " + countCorr;
//     textEditor();
//     var ul = document.getElementById("votesLi");
//     if (ul) {
//       while (ul.firstChild) {
//         ul.removeChild(ul.firstChild);
//       }
//     }
//     for(i=0;i<corrs.length;i++){
//       var hyper = document.createElement('a');
//       var li=document.createElement('li');
//       hyper.className = "addVote";
//       hyper.id = "hyper"+i;
//       li.appendChild(hyper);
//       ul.appendChild(li);
//       hyper.innerHTML=corrs[i] + " : "+votes[i];
//       hyper.style = "height:15px;color:white;";
      
//       hyper.onclick = function(event){
//         var index = parseInt(event.target.id.replace('hyper',''));
//         var vt = corrs[index].toString();
//         var idDoc= $('#idDoc').val();
//         var usr = 'redacteur1';
//         var voteData= {
//           "ip":ipCorr,
//           "select": voteSel,
//           "username": usr,
//           "idDoc": idDoc, 
//           "correction": vt,
//         };
//           $.ajax({
//             url: '../errors/savevote.php',
//             data:{voteData:voteData}, 
//             type: 'GET'
//           });
//           $('.correction').css({
//             'hidden':true,
//           }).fadeOut(200);
//         };
      
//     }
  
//     $('.correction').css({
//           'hidden': false,
//           'left': rect.right /2 - 150,
//           'top': rect.top + window.scrollY - 240 - (corrs.length * 15),
//         }).fadeIn(200);
//   }else if(selectedValue == "vote"){

//   }

// $('.deleteHighlight').click(function(){
//   Highlight('transparent');
  
// });
// $(document).on('click', 'span', function () {
//   Unhighlight($(this).html());
  
//   $(this).contents().unwrap();
//   $('.correction').css({
//     'hidden':true,
//   }).fadeOut(200);
// });

// $('.postNote').click(function(){
//   note();
// });

function textEditor() {
  var sel = window.getSelection();
  if (sel != '') {
    // $('ul.color').css({
    //   'left': pageX - 50,
    //   'top': pageY - 90
    // }).fadeIn(200);
    Highlight('yellow');
  }
}

function Highlight(color) {
  document.designMode = "on";
  var sel = window.getSelection();
  sel.removeAllRanges();
  var range = document.createRange();
 
//  try{
//   range.setStart(editor.startContainer, editor.startOffset);
//   range.setEnd(editor.endContainer, editor.endOffset);
//  }catch{

//   range.setStart(editor.startContainer, editor.startOffset);
//   range.setEnd(editor.endContainer, editor.endOffset);
//  }
  // range.setEnd(editor.endContainer, editor.endOffset);
  // sel.addRange(range);
  // selection=sel.toString();
  range.setStart(editor.startContainer, editor.startOffset);
  var endoffset=editor.endOffset+1
  var selection;
  do{
    endoffset--
    range.setEnd(editor.endContainer, endoffset);
    sel.addRange(range);
    selection=sel.toString();
  }while(selection.endsWith(' '))
  
  var place=range.startOffset;
  
  var ipdata="";
 
  $.get("https://ipinfo.io", function(response) {

    var ipdata=response.ip;
    var idDoc= $('#idDoc').val();
    ipCorr = ipdata;
    var place=range.startOffset;

    var someData = {
      "ip":ipdata,
      "select":selection,
      "idDoc":idDoc
      };
    jsonString = someData;
    var action="delete";

    if(color=='transparent'){
      Unhighlight(selection);
    }
    var tempoutline=$('#outline').val();
    if(tempoutline=="true"){
      $.ajax({
        url: '../errors/saveerror.php',
        data:{jsonString:jsonString}, 
        type: 'GET'
      });
    }
  else{
      $.ajax({
        url: '../log/savemark.php',
        data:{jsonString:jsonString}, 
        type: 'GET'
      });
    }
}, "json");

  if (!sel.isCollapsed) {
    document.execCommand("hiliteColor", false,color);
    // wrappedselection = '<span class="zacklight" style="background-color:yellow;">'+ sel +'</span>';
    // document.execCommand('insertHTML', true, wrappedselection);
    // if (!document.execCommand("HiliteColor", false, color)) {
    //   document.execCommand("BackColor", false, color);
    // }
  }
  sel.removeAllRanges();
  // $('ul.color').fadeOut(200);
  $('ol.tools').fadeOut(200);
  noteHighlight();
  $('.note').fadeOut(200);
  $('#textarea').val = '';
  document.designMode = "off";
}

var notes = [];
function note(){
  var sel = window.getSelection();
  var range = sel.getRangeAt(0);
  range.setStart(editor.startContainer, editor.startOffset);
  range.setEnd(editor.endContainer, editor.endOffset);
  if (sel != '') {
    // $('ul.color').fadeOut(200);
    $('ol.tools').fadeOut(200);
    $('ul.note').css({
      'left': pageX - 200,
      'top': pageY - 120
    }).fadeIn(200);
    var $span = $('<span>').addClass('blueHighlight').append(range.extractContents());
    range.insertNode($span[0]);
    $('textarea').focus();
  }
}

function noteHighlight(){
  $('#pages').find('.blueHighlight').each(function(){
    $(this)[0].outerHTML = $(this).text();
  });
}
function addNote() {
  // if(tyepof(Storage) !== "undefined"){
  //   if(localStorage.notes){
  //     localStorage.notes
  //   } else {
  //     localStorage.notes = 
  //   }

  localStorage.setItem()
  noteHighlight();
  $('.note').fadeOut(200);
}

//checking if selection exists in notes range
Number.prototype.between = function (min, max) {
  return this > min && this < max;
};

function Notes(location, note) {
  this.location = location;
  this.note = note;
}

// $('#makeBold').click(function () {
//     makeBold();
// });
// $('#toggleBold').click(function () {
//     toggleBold();
// });

// function toggleBold() {
//     var range, sel;
//     if (window.getSelection) {
//         // Non-IE case
//         sel = window.getSelection();
//         if (sel.getRangeAt) {
//             range = sel.getRangeAt(0);
//         }
//         document.designMode = "on";
//         if (range) {
//             sel.removeAllRanges();
//             sel.addRange(range);
//         }
//         document.execCommand("bold", null, false);
//         document.designMode = "off";
//     } else if (document.selection && document.selection.createRange && document.selection.type != "None") {
//         // IE case
//         range = document.selection.createRange();
//         range.execCommand("bold", null, false);
//     }
// }

// function makeBold() {
//     var selection = window.getSelection();
//     if (selection.rangeCount) {
//         var range = selection.getRangeAt(0).cloneRange();
//         var newNode = document.createElement("b");
//         range.surroundContents(newNode);
//         selection.removeAllRanges();
//         selection.addRange(range);
//     }
// }
<?php endif; ?> //render highlight functionality only if the summary is in french (Fin)
$(document).ready(function(){  

  var countaudio=0;
  $(".jp-controls").click(function(){
		//do something here
    // event.preventDefault();
    var idaudio= $('#idaudio').val();

if(countaudio==0){
    setTimeout(function(){   $("#jp_audio_0").attr("src", "http://audio.iferu.com/"+idaudio+".mp3"); $(".jp-play").click()
  }, 1000);
}
  countaudio++;
  
    

	});
 
  $(".fullSummaryShare").hide();$(".ix--bar").hide();$(".ix-bar").hide();

  $(".sump-formats li:eq(0)").click(function(event){
		//do something here
    event.preventDefault();

    pdfcreator();
	});
  $(".sump-formats li:eq(1)").click(function(event){
		//do something here
    event.preventDefault();

    epubcreator();
	});
  $(".sump-formats li:eq(3)").click(function(event){
		//do something here
    event.preventDefault();
    var idaudio= $('#idaudio').val();


    var link = document.createElement('a');
        link.href = "http://audio.iferu.com/"+idaudio+".mp3";
        link.download = idaudio+'.mp3';
  
        link.dispatchEvent(new MouseEvent('click'));
	});

function pdfcreator() {
 

     var resume= $('div[itemtype^="http://schema.org/"]').clone().find('h2[style="text-align:center"]').remove().end().find('.hidden').remove().end().find('.sump-cover-container').css('text-align','center').end().find('.affix-top,.furtherReading,.slick-list,.col-sm-push-2,.annot-section,.abstractCopyrightLine').remove().end().find('.scover--m').height('400px').end().find('hr').remove().end().html();



    var idDoc= $('#idDoc').val();
    if(idDoc==undefined){
      alert("merci de vous connecter afin de télécharger le pdf OU EPUB");
    }
    else{
    var path="../pdfs/"+idDoc+".pdf"
		$.ajax({  
			 url:"../test.php",  
			 method:"POST",  
			 data:{resume:resume,idDoc:idDoc},  
			 success:function()  
			 {  
			   
        var link = document.createElement('a');
        link.href = path;
        link.download = idDoc+'.pdf';
        link.dispatchEvent(new MouseEvent('click'));
				  
			 }  
		});  }

   
}

  function pdfgenerator(){
    var resume= $('div[itemtype^="http://schema.org/"]').clone().find('h2[style="text-align:center"]').remove().end().find('.hidden').remove().end().find('.affix-top,.furtherReading,.slick-list,.col-sm-push-2,.annot-section,.abstractCopyrightLine').remove().end().find('hr').remove().end().html();

var idDoc= $('#idDoc').val();
if(idDoc==undefined){
  alert("merci de vous connecter afin de télécharger le pdf OU EPUB");
}
else{
var path="../pdfs/"+idDoc+".pdf"
$.ajax({  
   url:"../test.php",  
   method:"POST",  
   data:{resume:resume,idDoc:idDoc},  
   success:function()  
   {  
     
 
      
   }  
});  }
  }
function epubcreator(){
  pdfgenerator();

  
    var path="../pdfs/"+idDoc+".epub"
		$.ajax({  
			 url:"../pdfs/pdftoepub.php",  
			 method:"GET",  
			 data:{idDoc:idDoc},  
			 success:function()  
			 {  
        myVar = setTimeout(window.open(path, "_blank"), 2000);
        
				  
			 }  
		});
}


  
  var state= $('#session').val();
  var idDoc= $('#idDoc').val();
  var user_name= $('#user_name').val();
  
  
  if(state=='true'){
    $('#logout').show();
    $('#connexion').hide();
  }

// Pour les livres aimés
   $(".ico-heart-empty").click(function(event){
     event.preventDefault();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{user_name:user_name,idDoc:idDoc},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
       
	}); 



  // ajouter pour lire plus tard
  $(".ico-bookmark-empty").click(function(event){
    event.preventDefault();
    var id= $('#idDoc').val();
  var user= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{user:user,id:id},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});

  /* Dislike book */
  $(".fa-heart").click(function(event){
    event.preventDefault();
    var idL= $('#idDoc').val();
    var userN= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userN:userN,idL:idL},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
       
	});
  // supprimer de la liste à lire plus tard
  $(".fa-bookmark").click(function(event){
    event.preventDefault();
    var idl= $('#idDoc').val();
  var usern= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{usern:usern,idl:idl},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});


// ajouter à ma chaine
$(".ico-plus").click(function(event){
  event.preventDefault();
    var idLivre= $('#idDoc').val();
  var userName= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userName:userName,idLivre:idLivre},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});
 
// partager un livre avec un ami
$(".ico-share-alt").click(function(event){
  event.preventDefault();
  $("#usernameForm").show();
  $("#hr").show();});

  $('#sbmt').click(function(){
    var idlivre= $('#idDoc').val();
  var userNames= $('#user_name').val();
  var user_receiver = $("#usernameS").val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userNames:userNames,idlivre:idlivre,user_receiver:user_receiver},  
             success:function(data)  
             { 
               
               $('#data-msg').html(data) ;   
             }  
        });  
  });
 
  

$('.btn-link').click(function(){
  $('.navigation__dropdown').toggle("active");
});
      $('#login_button').click(function(){ 
        
           var username = $('#userName').val();  
           var password = $('#passWord').val();  
           if(username != '' && password != '')  
           {  
                $.ajax({  
                     url:"../pdo.php",  
                     method:"POST",  
                     data: {username:username, password:password},  
                     success:function(data)  
                     {  
                          
                          if(data == 'No')  
                          { 
                               alert("Username or password wrong");  
                               
                          }  
                          else  
                          { 
                               $('#loginModal').hide();  
                               
                               location.reload();  
                          }  
                          
                     }  
                });  
           }  
           else  
           {  
                alert("username and password are  Required");  
           }  
      });  
      $('.sumLoggingForm button').click(function(event){ $(this).removeAttr("data-target"); window.location.href="en-login.php";  });
      $('#logout').click(function(event){  
        
           var action = "logout"; 
           
           $.ajax({  
                url:"../pdo.php",  
                method:"POST",  
                data:{action:action},  
                success:function()  
                {  
                  
                     location.reload();  
                     
                }  
           });  
      });  
 });  
 </script>  
	
	<?php
/* Button pour aimer un livre */
// $like = "SELECT count(*) as nombre FROM user_like WHERE user_name='".$_SESSION['username']."' AND idDoc=".$idDoc;
// $u = $pdo->prepare($like);
// $u->execute();
// $res = $u->fetch();

// if($res['nombre'] <> 0){
//  echo '<script>$(".ico-heart-empty").attr("class","fa fa-heart");</script>';     
// } 

/* Button pour ajouter à lire plus tard */
$later = "SELECT count(*) as nombre FROM user_read_later WHERE username='".$_SESSION['username']."' AND idDoc=".$idDoc;
$la = $pdo->prepare($later);
$la->execute();
$resL = $la->fetch();

if($resL['nombre'] <> 0){
 echo '<script>$(".ico-bookmark-empty").attr("class","fa fa-bookmark");</script>';     
} 
  ?>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam.nr-data.net","licenseKey":"996526df3c","agent":"","beacon":"bam.nr-data.net","applicationTime":107,"applicationID":"3375187","transactionName":"ZlwEbUVTCENRAhVQWV8WNUlFWwhXcw4PTUReVQpcRR0VRV0MAEtPHkICWENTL1RNQUl+c2UQ","queueTime":0}</script>
<div id="stats" data-ga-analytics="21" data-ga-analytics-vt-attributes="{ &quot;id&quot; : &quot;32972&quot;}" data-ga-analytics-meta="">
	<noscript>
		
		<img src="/gastat.gif?pv=21&id=32972&url=https%3a%2f%2fbackend.iferu.com%2ffr%2fresume%2fintelligence-economique%2f32972&iframe=false&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
	</noscript>
</div><script type="text/javascript" id="" src="../include_book/100323.js"></script><script type="text/javascript" id="">(function(){function t(){++g;50==g&&(k=!0);l()}function u(){m=!0;l()}function l(){k&&m&&!n&&(n=!0,dataLayer.push({event:"nobounce",scrollCount:g}))}var d=(new Date).getTime(),b=0,p=0,c=!0,h=!1,m=!1,k=!1,n=!1,g=0;setTimeout(u,3E4);var q=function(){p=(new Date).getTime();b+=p-d;c=!0},e=function(b){c&&(c=!1,d=(new Date).getTime(),h=!1);window.clearTimeout(r);r=window.setTimeout(q,5E3)},a=function(b,a){window.addEventListener?window.addEventListener(b,a):window.attachEvent&&window.attachEvent("on"+b,
a)},f=function(a){c||(b+=(new Date).getTime()-d);!h&&0<b&&36E5>b&&window.dataLayer.push({event:"nonIdle",nonIdleTimeElapsed:b});c&&(h=!0);a&&"beforeunload"===a.type&&window.removeEventListener("beforeunload",f);b=0;d=(new Date).getTime();window.setTimeout(f,15E3)};a("mousedown",e);a("keydown",e);a("mousemove",e);a("beforeunload",f);a("scroll",e);a("scroll",t);var r=window.setTimeout(q,5E3);window.setTimeout(f,15E3)})();</script><script type="text/javascript" id="">!function(d,e){var b="0059a9de7316e9b74d58489d3ccdef8d87";if(d.obApi){var c=function(a){return"[object Array]"===Object.prototype.toString.call(a)?a:[a]};d.obApi.marketerId=c(d.obApi.marketerId).concat(c(b))}else{var a=d.obApi=function(){a.dispatch?a.dispatch.apply(a,arguments):a.queue.push(arguments)};a.version="1.1";a.loaded=!0;a.marketerId=b;a.queue=[];b=e.createElement("script");b.async=!0;b.src="//amplify.outbrain.com/cp/obtp.js";b.type="text/javascript";c=e.getElementsByTagName("script")[0];
c.parentNode.insertBefore(b,c)}}(window,document);obApi("track","PAGE_VIEW");</script><script type="text/javascript" id="">window._tfa=window._tfa||[];window._tfa.push({notify:"event",name:"page_view"});!function(a,b,d,c){document.getElementById(c)||(a.async=1,a.src=d,a.id=c,b.parentNode.insertBefore(a,b))}(document.createElement("script"),document.getElementsByTagName("script")[0],"//cdn.taboola.com/libtrc/unip/1154722/tfa.js","tb_tfa_script");</script>
<noscript>
  <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="hs-script-loader" src="../include_book/4918719(2).js"></script><script type="text/javascript" id="">!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version="1.1",a.queue=[],b=e.createElement(f),b.async=!0,b.src="//static.ads-twitter.com/uwt.js",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,"script");twq("init","o0jtb");twq("track","PageView");</script><script type="text/javascript" id="">!function(b){var a=b.clearbit=b.clearbit||[];if(!a.initialize)if(a.invoked)b.console&&console.error&&console.error("Clearbit snippet included twice.");else{a.invoked=!0;a.methods="trackSubmit trackClick trackLink trackForm pageview identify reset group track ready alias page once off on".split(" ");a.factory=function(c){return function(){var d=Array.prototype.slice.call(arguments);d.unshift(c);a.push(d);return a}};for(b=0;b<a.methods.length;b++){var e=a.methods[b];a[e]=a.factory(e)}a.load=function(c){var d=
document.createElement("script");d.async=!0;d.src=("https:"===document.location.protocol?"https://":"http://")+"x.clearbitjs.com/v1/"+c+"/clearbit.min.js";c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(d,c)};a.SNIPPET_VERSION="3.1.0";a.load("pk_381e766f277206e0822dcd305318bae2");a.page()}}(window);</script><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon807437191762"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon765245014009" width="0" height="0" alt="" src="../include_book/0"></div><script>window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>
<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
<script>$(".fullSummaryShare").hide();$(".sump-audio").hide();$(".ix--bar").hide();window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>

</body></html>

<?php   
 session_start(); 

 ?> 
<!DOCTYPE html>
<!-- saved from url=(0068)http://backend.iferu.com/fr/resume/intelligence-economique/32972? -->
<html class="js no-touch" lang="fr">

<head>


<?php
  

    include("../poc/pdo.php");


    if(isset($_GET['id']) && !empty($_GET['id'])){
      if(isset($_SESSION['username'])){
      $usern = $_SESSION['username'];
      $pd = $pdo->prepare("SELECT * FROM user_table WHERE user_name = '".$usern."'");
      $pd->execute();
      $user_data = $pd->fetch();
      $user_date=$user_data['start_date'];   //get the date of registration of the user from the database
      $current_date=gmdate('Y-m-d H:i:s');    //date in gmt 
      //$date1 = strtotime($current_date); 
      // $diff = abs(strtotime($user_date)-time());
       //$diff = abs($date1 - $date2);     //calculate the difference between the two
      //echo $current_date."<br />";
      //echo $user_date."<br />" ;
      //echo $diff."<br />";
      }
      



      // Select Title of the book 
      $id= $_GET['id'];    
      $titre = $pdo->prepare("SELECT titre as t FROM Document Where id= ".$id);
      $titre->execute();
      $title = $titre->fetch();

      // Select description of the book
      $desc = $pdo->prepare("SELECT description as descr FROM Document Where id= ".$id);
      $desc->execute();
      $description = $desc->fetch();

      // Select Image URL of the book
      $img = $pdo->prepare("SELECT image FROM Document Where id= ".$id);
      $img->execute();
      $image = $img->fetch();
      $image1=explode('/',$image['image']);
       $ff=explode('.',$image1[2]);
     $originalID=$ff[0];

      // Select authors of the book
      $at="select GROUP_CONCAT(DISTINCT A.nom separator ', ') AS author FROM Auteur A, Document D, Doc_Aut DA WHERE DA.idDoc =".$id." and A.id= DA.idAut";
       $auteur = $pdo->prepare($at);
      $auteur->execute();
      $author = $auteur->fetch(); 
    
    }
$des = $description['descr'];
?>

<?php
$str= $_SERVER['REQUEST_URI'];

$gg=explode('/',$str);
if( substr( $gg[1], 0, 4 ) == 'en'){
  $gg[2]=$_GET['id'];


}
$gg[2]=preg_replace('/(\d+).*/', '$1', $gg[2]);

 if( substr( $gg[1], 0, 4 ) == 'en'  or empty(substr( $gg[1], 8, 1) )){
  $name = str_replace(' ', '-', $title['t']);
  
header("Location: /summary_".$name."/".$gg[2], true, 301);
exit();
}
$idaudio = $gg[2] ;
  echo "<input type='hidden' value='$idaudio' id='idaudio'></input>";
?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> Iferu Book : <?php  echo $title['t']; ?></title>
<meta name="description" property="og:description" content="<?php  echo $description['descr']; ?>">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="og:title" content="<?php  echo $title['t']; ?>">
<meta property="og:type" content="article">
<meta property="og:url" content="<?php  echo 'http://backend.iferu.com/book.php?id='.$id; ?>">
<meta property="og:image" content="<?php  echo "http://backend.iferu.com/image_merge.php?id=".$originalID; ?>" >
<meta property="og:image:type" content="image/jpeg" />
<meta name="author" content="<?php echo $author['author'];?>">
<meta property="og:site_name" content="iferu">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@iferu">
<meta name="twitter:title" content="<?php  echo $title['t']; ?>">
<meta name="twitter:description" content="<?php   echo $description['descr'];  ?>">
<meta name="twitter:image" content="<?php  echo "http://backend.iferu.com/image_merge.php?id=".$id; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:app_id" content="143891362343386">
<style>.col-sm-8.col-sm-push-2{display:none;}.ix--bar{display:none;}.slick-container{display:flex;}</style>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->

<!-- jQuery library -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

<!-- Latest compiled JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" defer>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<!-- Optional theme -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
 -->
<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!-- <link rel="alternate" href="http://backend.iferu.com/oembed?url=https%3A%2F%2Fbackend.iferu.com%2Fsummary%2F32972&amp;format=json" type="application/json+oembed" title="Intelligence économique">
<link rel="alternate" href="http://backend.iferu.com/oembed?url=https%3A%2F%2Fbackend.iferu.com%2Fsummary%2F32972&amp;format=xml" type="text/xml+oembed" title="Intelligence économique">
 --><link rel="shortcut icon" href="http://backend.iferu.com/favicon.ico" type="image/x-icon">
<link rel="search" href="http://backend.iferu.com/opensearch.xml" type="application/opensearchdescription+xml" title="iferu" defer>
<script src="jquery-3.5.1.min.js"></script>
<!--
<script async="" src="../include_book/insight.old.min.js"></script><script type="text/javascript" async="" src="../include_book/insight.min.js"></script><script type="text/javascript" src="../include_book/996526df3c"></script><script src="../include_book/nr-1184.min.js"></script><script src="../include_book/4918719.js" type="text/javascript" id="hs-analytics"></script><script src="../include_book/fb.js" type="text/javascript" id="hs-ads-pixel-4918719" data-ads-portal-id="4918719" data-ads-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../include_book/4918719(1).js" type="text/javascript" id="cookieBanner-4918719" data-cookieconsent="ignore" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../include_book/leadflows.js" type="text/javascript" id="LeadFlows-4918719" crossorigin="anonymous" data-leadin-portal-id="4918719" data-leadin-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../include_book/feedbackweb-new.js" type="text/javascript" id="hs-feedback-web-4918719" crossorigin="anonymous" data-hubspot-feedback-portal-id="4918719" data-hubspot-feedback-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script async="" src="../include_book/insight.old.min.js"></script><script async="" src="../include_book/clearbit.min.js"></script><script async="" src="../include_book/uwt.js"></script><script async="" src="../include_book/fbevents.js"></script><script async="" src="../include_book/tfa.js" id="tb_tfa_script"></script><script async="" src="../include_book/obtp.js" type="text/javascript"></script><script type="text/javascript" async="" src="../include_book/bat.js"></script><script type="text/javascript" async="" src="../include_book/insight.min.js"></script><script type="text/javascript" async="" src="../include_book/analytics.js"></script><script type="text/javascript" async="" src="../include_book/dc.js"></script><script async="" src="../include_book/gtm.js"></script><script type="text/javascript" async="async" src="../include_book/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js"></script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"996526df3c",applicationID:"3375187"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(event){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(event){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,y=["click","keydown","mousedown","pointerdown","touchstart"];y.forEach(function(event){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?f(e,c,o):o()}function n(n,r,i,o){if(!p.aborted||o){e&&e(n,r,i);for(var a=t(i),c=v(n),f=c.length,u=0;u<f;u++)c[u].apply(a,r);var d=s[w[n]];return d&&d.push([b,n,r,a]),a}}function l(e,t){h[e]=v(e).concat(t)}function m(e,t){var n=h[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return h[e]||[]}function g(e){return d[e]=d[e]||i(n)}function y(e,t){u(e,function(e,n){t=t||"feature",w[n]=t,t in s||(s[t]=[])})}var h={},w={},b={on:l,addEventListener:l,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:y,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(p.aborted=!0,s=p.backlog={})}var c="nr@context",f=e("gos"),u=e(6),s={},d={},p=t.exports=i();p.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!E++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(h,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var y=""+location,h={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1184.min.js"},w=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:y,features:{},xhrWrappable:w,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var E=0},{}],"wrap-function":[function(e,t,n){function r(e){return!(e&&e instanceof Function&&e.apply&&!e[a])}var i=e("ee"),o=e(7),a="nr@original",c=Object.prototype.hasOwnProperty,f=!1;t.exports=function(e,t){function n(e,t,n,i){function nrWrapper(){var r,a,c,f;try{a=this,r=o(arguments),c="function"==typeof n?n(r,a):n||{}}catch(u){p([u,"",[r,a,i],c])}s(t+"start",[r,a,i],c);try{return f=e.apply(a,r)}catch(d){throw s(t+"err",[r,a,d],c),d}finally{s(t+"end",[r,a,f],c)}}return r(e)?e:(t||(t=""),nrWrapper[a]=e,d(e,nrWrapper),nrWrapper)}function u(e,t,i,o){i||(i="");var a,c,f,u="-"===i.charAt(0);for(f=0;f<t.length;f++)c=t[f],a=e[c],r(a)||(e[c]=n(a,u?c+i:i,o,c))}function s(n,r,i){if(!f||t){var o=f;f=!0;try{e.emit(n,r,i,t)}catch(a){p([a,n,r,i])}f=o}}function d(e,t){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(e);return n.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(r){p([r])}for(var i in e)c.call(e,i)&&(t[i]=e[i]);return t}function p(t){try{e.emit("internal-error",t)}catch(n){}}return e||(e=i),n.inPlace=u,n.flag=a,n}},{}]},{},["loader"]);</script>
--><link rel="stylesheet" type="text/css" href="../include_book/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css" defer>
<link rel="stylesheet" type="text/css" href="../include_book/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css" defer> 
<link rel="stylesheet" type="text/css" href="../fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css" defer>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" defer>-->
<link rel="stylesheet" type="text/css" href="../include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css" defer>
<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
<?php
if(isset($_SESSION['username']))
{
  echo "<input type='hidden' value='true' id='session'></input>";
  $idDoc = $gg[2] ;
  echo "<input type='hidden' value='$idDoc' id='idDoc'></input>";
  $user_name = $_SESSION["username"] ;
  echo "<input type='hidden' value='$user_name' id='user_name'></input>";
  
  /* Hide share button when the user has already sent a book */
  $sent = "SELECT COUNT(*) as n FROM user_sent_books WHERE user_name='".$_SESSION['username']."'";
  $u = $pdo->prepare($sent);
  $u->execute();
  $res = $u->fetch();

  if($res['n'] > 0){
    echo '<style>.ico-share-alt{display:none;}</style>';
   
    
  }
    
  $flag = "SELECT available FROM Document WHERE id=".$idDoc;
  $u = $pdo->prepare($flag);
  $u->execute();
  $res = $u->fetch();

  // if($res['available'] > 0){
  //   echo "<script>window.location.href='/en/no-book.php';</script>";
  // }
}
?>
<script>
	window.VWO = window.VWO || [];
	window.VWO.push(['tag', 'Login', 'true', 'session']); 
	_vis_opt_check_segment = {'95': true };
	window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
	window.anonymous = 'true'; /* Used for VWO segmentation */
	</script>
	<script>
	var _vwo_code=(function(){
	var account_id=5083,
	settings_tolerance=2000,
	library_tolerance=2500,
	use_existing_jquery=false,
	// DO NOT EDIT BELOW THIS LINE
	f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script><script src="../include_book/j.php" type="text/javascript"></script>


<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->




<script src="../include_book/f.txt"></script>
<script src="../include_book/f(1).txt"></script>
<script src="../include_book/va-0ca7acdf418d8c12f3819dda65c35024.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include_book/track-0ca7acdf418d8c12f3819dda65c35024.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include_book/opa-56761856850233eb41e36332d7e3cf79.js" crossorigin="anonymous" type="text/javascript"></script><!--<style type="text/css">
  
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }
  }
</style>--></head>
<body class="" data-new-gr-c-s-check-loaded="14.982.0"><!--<div id="hs-feedback-fetcher">
<iframe frameborder="0" src="../include_book/feedback-web-fetcher.html"></iframe></div>
-->

		<script>
			dataLayer = [{
				"PageView": "<?php  echo "/fr/resume/".$title['t']."/".$id; ?>"
				
			}];
		</script>

	<div class="sr-only">
  <a href="<?php  echo "http://backend.iferu.com/en/resume/".$title['t']."/".$id; ?>?#main-content">Skip navigation</a>
  </div>	
<?php 
 include ("header_eng.php")

?>

<style>
.navigation__search {
  height: 53px; 
}

</style>

<!--- START MAIN_BODY -->

<style>
  .affix {
    position: sticky;
}
</style>

<?php 
$gg[2]=preg_replace('/(\d+).*/', '$1', $gg[2]);

$book = file_get_contents("../all_book/$gg[2]-h.html");
$sumary = file_get_contents("../all_book/$gg[2].html");



$res = explode('<div class="clearfix" itemprop="review" itemscope itemtype="http://schema.org/Review">',$book);

 //echo $sumary;
 $url='backend.iferu.com/book.php?id='.$id;
$url1 = urlencode($url);

  
echo  $res[0];

//echo '<div class="multi-button"><button><i class="fa fa-amazon"></i><a href="http://www.facebook.com/sharer.php?u='.$url.'" target="_blank">Facebook</a></button>';
//echo '<button><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url='.$url.'"  target="_blank">LinkedIn</a></button>';
//echo '<button><a href="https://twitter.com/share?url='.$url.'"  target="_blank">Twitter</a></button></div>';
?>

<h2 data-text-lang style="text-align:center">Share to :</h2><br>
<div style="margin-left:44%">
<a style="margin-right:15px"  href="http://www.facebook.com/sharer.php?u=<?php echo $url1?>" target="_blank">
<button>
<i class="fa fa-facebook-official"></i></button></a>
<a style="margin-right:15px;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url1 ?>" 
target="_blank">
<button><i class="fa fa-linkedin-square"></i></button></a>
<a href="https://twitter.com/share?url=<?php echo $url1?>" target="_blank">
<button><i class="fa fa-twitter-square"></i></button></a>
 </div>
 <hr id="hr" style="display:none;">

 <div class="alert-danger" style="display:none; text-align:center;" id="data-msg"></div>
  <form style="display:none; text-align:center;" id="usernameForm" method="post">
    <label data-text-lang>Receiver</label>
    <input type="text" name="usernameS" id="usernameS">
    <input type="submit" id="sbmt" value="OK">
  </form>


  <?php
  //  echo $current_date."<br />";
  //     echo $user_date."<br />" ;
  //     echo $diff."<br />";
  if($diff > 259200)://3days in seconds
    
    ?>
    <br></br><br></br>
      <h2 id="paypal_price">4.90 euros/mois</h2>
      <div style="margin: 0 auto;" id="paypal-button-container-P-9X052767J83845001MCZ7QMA"></div>
    <script src="https://www.paypal.com/sdk/js?client-id=AdzlnX9JURZ70uKzi5FPUhIXUaoiJM7CkOm-K9Oe8DEGJjllUa5S-S2Enkl_km5DN47Z9eUVCb9-FDUH&vault=true&intent=subscription" data-sdk-integration-source="button-factory"></script> 
    <script> paypal.Buttons({ style: { shape: 'rect', color: 'gold', layout: 'vertical', label: 'subscribe' }, createSubscription: function(data, actions) { return actions.subscription.create({ /* Creates the subscription */ plan_id: 'P-9X052767J83845001MCZ7QMA' }); }, onApprove: function(data, actions) { alert(data.subscriptionID); } }).render('#paypal-button-container-P-9X052767J83845001MCZ7QMA'); // Renders the PayPal button </script> 
    <style>
#paypal-button-container-P-9X052767J83845001MCZ7QMA{
  width: 50%;
    margin: 0 auto;
    
}#paypal_price{text-align:center;}
li.footer__list-item::before{
  display:none;
}


















</style>
  <?php
  include '../en_chaine_dynamique.php';
  include 'en_commenter_resumer.php';
 
else:
 
  
echo $sumary ;
echo explode('<div class="clear"></div>',$res[1])[1];


  
  ?>
  
  <script>

/* $("h3 ").text('Linked channels').hide();
    $('.ga-channel-box').hide();
   //$('#sharing').hide();
    //$('#sharing').insertAfter('#shareBox');
    $('.ga-channel-box-changed').show();
    $('.chaine-changed').show();
    $('.row-changed').show();
    $('.col-sm-8.col-sm-push-2').hide();
    $('.row-changed').show(); */
</script>
<?php 
  

 include '../en_chaine_dynamique.php';
  include 'en_commenter_resumer.php';
   endif;
?>
 <script defer>
   if($('div.col-sm-12.message').length){
     $('div.col-sm-12.message').text('We are awaiting publication rights for this book. The summary will be available as soon as we get them. This procedure may take some time. We thank you for your patience.');
    $('div.text-center').hide();
    $('h2').text('Share to :').hide();
      $('h2').text('Share to :').nextAll().hide();
  }
</script>






<!--- END MAIN_BODY -->
<!--- END WRAPPER -->

<div id="fb-root"></div>

<script src="../include_book/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js" defer></script>
<script src="../include_book/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js" defer></script>
<script src="../include_book/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js" defer></script>
<script src="../include_book/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js" defer></script>
<script src="../include_book/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js" defer></script>

<script src="../include_book/plugins-cb-rqhn3ki7bf6j5lwmkcckeu85uqiuj47.js" defer></script>
<script src="../include_book/main-cb-tqm751dbrn8x06irjukcpdj53han94l.js" defer></script>

<script>
	var hy = document.createElement('script'); hy.type = 'text/javascript'; hy.setAttribute("async", "async");
	hy.src = "/www/js/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js";
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hy, s);
</script>
<!--
<script>
 	
 	$(function(){
 		$.gaNotificationInit("/fr/notification/check?jsp=%2fWEB-INF%2fviews%2fabstracts%2fsummary-page.jsp", "/fr/notification/dismiss/{notificationId}");
 	});
</script>
<style>#logout{display:none;}</style>
-->
 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.0/jspdf.umd.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.2.6/purify.min.js"></script>  -->
<!-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jspdf-html2canvas@latest/dist/jspdf-html2canvas.min.js"></script> -->
<!-- 
<script src="html2pdf.bundle.js"></script> -->

<script>  

 $(document).ready(function(){  
 var countaudio=0;
  $(".jp-controls").click(function(){
		//do something here
    // event.preventDefault();
    var idaudio= $('#idaudio').val();

if(countaudio==0){
    setTimeout(function(){   $("#jp_audio_0").attr("src", "http://audio.iferu.com/"+idaudio+".mp3"); $(".jp-play").click()
  }, 1000);
}
  countaudio++;
  
    

	});
  
 
  $(".fullSummaryShare").hide();$(".ix--bar").hide();

  $(".sump-formats li:eq(0)").click(function(event){
		//do something here
    event.preventDefault();

    pdfcreator();
	});
  $(".sump-formats li:eq(1)").click(function(event){
		//do something here
    event.preventDefault();

    epubcreator();
	});
  $(".sump-formats li:eq(3)").click(function(event){
		//do something here
    event.preventDefault();
    var idaudio= $('#idaudio').val();


    var link = document.createElement('a');
        link.href = "http://audio.iferu.com/"+idaudio+".mp3";
        link.download = idaudio+'.mp3';
  
        link.dispatchEvent(new MouseEvent('click'));
	});


function pdfcreator() {
 

     var resume= $('div[itemtype^="http://schema.org/"]').clone().find('h2[style="text-align:center"]').remove().end().find('.hidden').remove().end().find('.sump-cover-container').css('text-align','center').end().find('.affix-top,.furtherReading,.slick-list,.col-sm-push-2,.annot-section,.abstractCopyrightLine').remove().end().find('.scover--m').height('400px').end().find('hr').remove().end().html();



    var idDoc= $('#idDoc').val();
    console.log(idDoc);
    if(idDoc==undefined){
      alert("merci de vous connecter afin de télécharger le pdf OU EPUB");
    }
    else{
    var path="../pdfs/"+idDoc+".pdf"
		$.ajax({  
			 url:"../test.php",  
			 method:"POST",  
			 data:{resume:resume,idDoc:idDoc},  
			 success:function()  
			 {  
			   
        var link = document.createElement('a');
        link.href = path;
        link.download = idDoc+'.pdf';
        link.dispatchEvent(new MouseEvent('click'));
				  
			 }  
		});  }

   
}

  function pdfgenerator(){
    var resume= $('div[itemtype^="http://schema.org/"]').clone().find('h2[style="text-align:center"]').remove().end().find('.hidden').remove().end().find('.affix-top,.furtherReading,.slick-list,.col-sm-push-2,.annot-section,.abstractCopyrightLine').remove().end().find('hr').remove().end().html();


var idDoc= $('#idDoc').val();
if(idDoc==undefined){
  alert("merci de vous connecter afin de télécharger le pdf OU EPUB");
}
else{
var path="../pdfs/"+idDoc+".pdf"
$.ajax({  
   url:"../test.php",  
   method:"POST",  
   data:{resume:resume,idDoc:idDoc},  
   success:function()  
   {  
     
 
      
   }  
});  }
  }
function epubcreator(){
  pdfgenerator();

  
    var path="../pdfs/"+idDoc+".epub"
		$.ajax({  
			 url:"../pdfs/pdftoepub.php",  
			 method:"GET",  
			 data:{idDoc:idDoc},  
			 success:function()  
			 {  
        myVar = setTimeout(window.open(path, "_blank"), 2000);
        
				  
			 }  
		});
}


  
  var state= $('#session').val();
  var idDoc= $('#idDoc').val();
  var user_name= $('#user_name').val();
  
  
  if(state=='true'){
    $('#logout').show();
    $('#connexion').hide();
  }

// Pour les livres aimés
   $(".ico-heart-empty").click(function(event){
     event.preventDefault();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{user_name:user_name,idDoc:idDoc},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
       
	}); 



  // ajouter pour lire plus tard
  $(".ico-bookmark-empty").click(function(event){
    event.preventDefault();
    var id= $('#idDoc').val();
  var user= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{user:user,id:id},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});

  /* Dislike book */
  $(".fa-heart").click(function(event){
    event.preventDefault();
    var idL= $('#idDoc').val();
    var userN= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userN:userN,idL:idL},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
       
	});
  // supprimer de la liste à lire plus tard
  $(".fa-bookmark").click(function(event){
    event.preventDefault();
    var idl= $('#idDoc').val();
  var usern= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{usern:usern,idl:idl},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});


// ajouter à ma chaine
$(".ico-plus").click(function(event){
  event.preventDefault();
    var idLivre= $('#idDoc').val();
  var userName= $('#user_name').val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userName:userName,idLivre:idLivre},  
             success:function(data)  
             {  
              
              $('#data-msg').html(data);
             }  
        });  
	});
 
// partager un livre avec un ami
$(".ico-share-alt").click(function(event){
  event.preventDefault();
  $("#usernameForm").show();
  $("#hr").show();});

  $('#sbmt').click(function(){
    var idlivre= $('#idDoc').val();
  var userNames= $('#user_name').val();
  var user_receiver = $("#usernameS").val();
    $.ajax({  
             url:"../pdo.php",  
             method:"POST",  
             data:{userNames:userNames,idlivre:idlivre,user_receiver:user_receiver},  
             success:function(data)  
             { 
               
               $('#data-msg').html(data) ;   
             }  
        });  
  });

$('.btn-link').click(function(){
  $('.navigation__dropdown').toggle("active");
});
      $('#login_button').click(function(){ 
        
           var username = $('#userName').val();  
           var password = $('#passWord').val();  
           if(username != '' && password != '')  
           {  
                $.ajax({  
                     url:"../pdo.php",  
                     method:"POST",  
                     data: {username:username, password:password},  
                     success:function(data)  
                     {  
                          
                          if(data == 'No')  
                          { 
                               alert("Username or password wrong");  
                               
                          }  
                          else  
                          { 
                               $('#loginModal').hide();  
                               
                               location.reload();  
                          }  
                          
                     }  
                });  
           }  
           else  
           {  
                alert("username and password are  Required");  
           }  
      });  
      $('.sumLoggingForm button').click(function(event){ $(this).removeAttr("data-target"); window.location.href="en-login.php";  });
      $('#logout').click(function(event){  
        
           var action = "logout"; 
           
           $.ajax({  
                url:"../pdo.php",  
                method:"POST",  
                data:{action:action},  
                success:function()  
                {  
                  
                     location.reload();  
                     
                }  
           });  
      });  
 });  
 </script>  
	
	<?php
/* Button pour aimer un livre */
$like = "SELECT count(*) as nombre FROM user_like WHERE user_name='".$_SESSION['username']."' AND idDoc=".$idDoc;
$u = $pdo->prepare($like);
$u->execute();
$res = $u->fetch();

if($res['nombre'] <> 0){
 echo '<script>$(".ico-heart-empty").attr("class","fa fa-heart");</script>';     
} 

/* Button pour ajouter à lire plus tard */
$later = "SELECT count(*) as nombre FROM user_read_later WHERE username='".$_SESSION['username']."' AND idDoc=".$idDoc;
$la = $pdo->prepare($later);
$la->execute();
$resL = $la->fetch();

if($resL['nombre'] <> 0){
 echo '<script>$(".ico-bookmark-empty").attr("class","fa fa-bookmark");</script>';     
} 
  ?>
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam.nr-data.net","licenseKey":"996526df3c","agent":"","beacon":"bam.nr-data.net","applicationTime":107,"applicationID":"3375187","transactionName":"ZlwEbUVTCENRAhVQWV8WNUlFWwhXcw4PTUReVQpcRR0VRV0MAEtPHkICWENTL1RNQUl+c2UQ","queueTime":0}</script>
<div id="stats" data-ga-analytics="21" data-ga-analytics-vt-attributes="{ &quot;id&quot; : &quot;32972&quot;}" data-ga-analytics-meta="">
	<noscript>
		
		<img src="/gastat.gif?pv=21&id=32972&url=https%3a%2f%2fbackend.iferu.com%2ffr%2fresume%2fintelligence-economique%2f32972&iframe=false&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
	</noscript>
</div><script type="text/javascript" id="" src="../include_book/100323.js"></script><script type="text/javascript" id="">(function(){function t(){++g;50==g&&(k=!0);l()}function u(){m=!0;l()}function l(){k&&m&&!n&&(n=!0,dataLayer.push({event:"nobounce",scrollCount:g}))}var d=(new Date).getTime(),b=0,p=0,c=!0,h=!1,m=!1,k=!1,n=!1,g=0;setTimeout(u,3E4);var q=function(){p=(new Date).getTime();b+=p-d;c=!0},e=function(b){c&&(c=!1,d=(new Date).getTime(),h=!1);window.clearTimeout(r);r=window.setTimeout(q,5E3)},a=function(b,a){window.addEventListener?window.addEventListener(b,a):window.attachEvent&&window.attachEvent("on"+b,
a)},f=function(a){c||(b+=(new Date).getTime()-d);!h&&0<b&&36E5>b&&window.dataLayer.push({event:"nonIdle",nonIdleTimeElapsed:b});c&&(h=!0);a&&"beforeunload"===a.type&&window.removeEventListener("beforeunload",f);b=0;d=(new Date).getTime();window.setTimeout(f,15E3)};a("mousedown",e);a("keydown",e);a("mousemove",e);a("beforeunload",f);a("scroll",e);a("scroll",t);var r=window.setTimeout(q,5E3);window.setTimeout(f,15E3)})();</script><script type="text/javascript" id="">!function(d,e){var b="0059a9de7316e9b74d58489d3ccdef8d87";if(d.obApi){var c=function(a){return"[object Array]"===Object.prototype.toString.call(a)?a:[a]};d.obApi.marketerId=c(d.obApi.marketerId).concat(c(b))}else{var a=d.obApi=function(){a.dispatch?a.dispatch.apply(a,arguments):a.queue.push(arguments)};a.version="1.1";a.loaded=!0;a.marketerId=b;a.queue=[];b=e.createElement("script");b.async=!0;b.src="//amplify.outbrain.com/cp/obtp.js";b.type="text/javascript";c=e.getElementsByTagName("script")[0];
c.parentNode.insertBefore(b,c)}}(window,document);obApi("track","PAGE_VIEW");</script><script type="text/javascript" id="">window._tfa=window._tfa||[];window._tfa.push({notify:"event",name:"page_view"});!function(a,b,d,c){document.getElementById(c)||(a.async=1,a.src=d,a.id=c,b.parentNode.insertBefore(a,b))}(document.createElement("script"),document.getElementsByTagName("script")[0],"//cdn.taboola.com/libtrc/unip/1154722/tfa.js","tb_tfa_script");</script>
<noscript>
  <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="hs-script-loader" src="../include_book/4918719(2).js"></script><script type="text/javascript" id="">!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version="1.1",a.queue=[],b=e.createElement(f),b.async=!0,b.src="//static.ads-twitter.com/uwt.js",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,"script");twq("init","o0jtb");twq("track","PageView");</script><script type="text/javascript" id="">!function(b){var a=b.clearbit=b.clearbit||[];if(!a.initialize)if(a.invoked)b.console&&console.error&&console.error("Clearbit snippet included twice.");else{a.invoked=!0;a.methods="trackSubmit trackClick trackLink trackForm pageview identify reset group track ready alias page once off on".split(" ");a.factory=function(c){return function(){var d=Array.prototype.slice.call(arguments);d.unshift(c);a.push(d);return a}};for(b=0;b<a.methods.length;b++){var e=a.methods[b];a[e]=a.factory(e)}a.load=function(c){var d=
document.createElement("script");d.async=!0;d.src=("https:"===document.location.protocol?"https://":"http://")+"x.clearbitjs.com/v1/"+c+"/clearbit.min.js";c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(d,c)};a.SNIPPET_VERSION="3.1.0";a.load("pk_381e766f277206e0822dcd305318bae2");a.page()}}(window);</script><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon807437191762"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon765245014009" width="0" height="0" alt="" src="../include_book/0"></div><script>window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>
<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
<script>$(".fullSummaryShare").hide();$(".ix--bar").hide();window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>

  
</body></html>

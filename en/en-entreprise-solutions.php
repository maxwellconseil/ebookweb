<?php 
			 session_start();  ?>
			
<!DOCTYPE html>
<html class="js no-touch" lang="en"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Discover iferu’s corporate solutions</title>
<meta property="og:title" content="Learning at the Speed of Business ">
<meta property="og:type" content="website">
<meta property="og:image" content="/www/images/en/welcome-share-ad.jpg">
<meta property="og:url" content="http://backend.iferu.com">
<meta property="og:site_name" content="iferu">
<meta name="description" content="Prepare your employees to consistently make informed and intelligent decisions by giving them ongoing access to up-to-date and relevant knowledge.">
<meta name="keywords" content="corporate training, corporate university, leadership development, leadership training, executive training, executive development.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="fb:app_id" content="143891362343386">
<link rel="shortcut icon" href="http://backend.iferu.com/favicon.ico" type="image/x-icon">
<link rel="search" href="http://backend.iferu.com/opensearch.xml" type="application/opensearchdescription+xml" title="iferu">
<!--
<link rel="stylesheet" type="text/css" href="../include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
<script src="../fr-enterprise-solutions_files/feedbackweb-new.js" type="text/javascript" id="hs-feedback-web-4918719" crossorigin="anonymous" data-hubspot-feedback-portal-id="4918719" data-hubspot-feedback-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../fr-enterprise-solutions_files/leadflows.js" type="text/javascript" id="LeadFlows-4918719" crossorigin="anonymous" data-leadin-portal-id="4918719" data-leadin-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../fr-enterprise-solutions_files/4918719.js" type="text/javascript" id="cookieBanner-4918719" data-cookieconsent="ignore" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../fr-enterprise-solutions_files/4918719(1).js" type="text/javascript" id="hs-analytics"></script><script src="../fr-enterprise-solutions_files/fb.js" type="text/javascript" id="hs-ads-pixel-4918719" data-ads-portal-id="4918719" data-ads-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script async="" src="../fr-enterprise-solutions_files/clearbit.min.js"></script><script async="" src="../fr-enterprise-solutions_files/uwt.js"></script><script src="../fr-enterprise-solutions_files/832786183443261" async=""></script><script async="" src="../fr-enterprise-solutions_files/fbevents.js"></script><script async="" src="../fr-enterprise-solutions_files/tfa.js" id="tb_tfa_script"></script><script async="" src="../fr-enterprise-solutions_files/obtp.js" type="text/javascript"></script><script type="text/javascript" async="" src="../fr-enterprise-solutions_files/bat.js"></script><script type="text/javascript" async="" src="../fr-enterprise-solutions_files/insight.min.js"></script><script type="text/javascript" async="" src="../fr-enterprise-solutions_files/analytics.js"></script><script type="text/javascript" async="" src="../fr-enterprise-solutions_files/dc.js"></script><script async="" src="../fr-enterprise-solutions_files/gtm.js"></script><script type="text/javascript" async="async" src="../fr-enterprise-solutions_files/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js"></script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"996526df3c",applicationID:"3375187"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,y=["click","keydown","mousedown","pointerdown","touchstart"];y.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?f(e,c,o):o()}function n(n,r,i,o){if(!p.aborted||o){e&&e(n,r,i);for(var a=t(i),c=v(n),f=c.length,u=0;u<f;u++)c[u].apply(a,r);var d=s[w[n]];return d&&d.push([b,n,r,a]),a}}function l(e,t){h[e]=v(e).concat(t)}function m(e,t){var n=h[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return h[e]||[]}function g(e){return d[e]=d[e]||i(n)}function y(e,t){u(e,function(e,n){t=t||"feature",w[n]=t,t in s||(s[t]=[])})}var h={},w={},b={on:l,addEventListener:l,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:y,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(p.aborted=!0,s=p.backlog={})}var c="nr@context",f=e("gos"),u=e(6),s={},d={},p=t.exports=i();p.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!E++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(h,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var y=""+location,h={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1184.min.js"},w=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:y,features:{},xhrWrappable:w,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var E=0},{}],"wrap-function":[function(e,t,n){function r(e){return!(e&&e instanceof Function&&e.apply&&!e[a])}var i=e("ee"),o=e(7),a="nr@original",c=Object.prototype.hasOwnProperty,f=!1;t.exports=function(e,t){function n(e,t,n,i){function nrWrapper(){var r,a,c,f;try{a=this,r=o(arguments),c="function"==typeof n?n(r,a):n||{}}catch(u){p([u,"",[r,a,i],c])}s(t+"start",[r,a,i],c);try{return f=e.apply(a,r)}catch(d){throw s(t+"err",[r,a,d],c),d}finally{s(t+"end",[r,a,f],c)}}return r(e)?e:(t||(t=""),nrWrapper[a]=e,d(e,nrWrapper),nrWrapper)}function u(e,t,i,o){i||(i="");var a,c,f,u="-"===i.charAt(0);for(f=0;f<t.length;f++)c=t[f],a=e[c],r(a)||(e[c]=n(a,u?c+i:i,o,c))}function s(n,r,i){if(!f||t){var o=f;f=!0;try{e.emit(n,r,i,t)}catch(a){p([a,n,r,i])}f=o}}function d(e,t){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(e);return n.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(r){p([r])}for(var i in e)c.call(e,i)&&(t[i]=e[i]);return t}function p(t){try{e.emit("internal-error",t)}catch(n){}}return e||(e=i),n.inPlace=u,n.flag=a,n}},{}]},{},["loader"]);</script>
	<link rel="stylesheet" type="text/css" href="../fr-enterprise-solutions_files/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css">
--><link rel="stylesheet" type="text/css" href="../fr-enterprise-solutions_files/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css" defer>

	<link rel="stylesheet" type="text/css" href="../fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css" defer>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" defer>
<link rel="stylesheet" type="text/css" href="../include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css" defer>

<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
	<script>
	window.VWO = window.VWO || [];
	window.VWO.push(['tag', 'Login', 'true', 'session']); 
	_vis_opt_check_segment = {'95': true };
	window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
	window.anonymous = 'true'; /* Used for VWO segmentation */
	</script>
	<script>
	var _vwo_code=(function(){
	var account_id=5083,
	settings_tolerance=2000,
	library_tolerance=2500,
	use_existing_jquery=false,
	// DO NOT EDIT BELOW THIS LINE
	f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script><script src="../fr-enterprise-solutions_files/j.php" type="text/javascript"></script>


<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->
<!--
<script src="../fr-enterprise-solutions_files/f.txt"></script><script src="../fr-enterprise-solutions_files/f(1).txt"></script><script src="../fr-enterprise-solutions_files/va-ffd39e015e5d25ce3182fc10ac34feab.js" crossorigin="anonymous" type="text/javascript"></script><script src="../fr-enterprise-solutions_files/track-ffd39e015e5d25ce3182fc10ac34feab.js" crossorigin="anonymous" type="text/javascript"></script><script src="../fr-enterprise-solutions_files/opa-e3db69dc6d0af05a6f9f8b749ec76384.js" crossorigin="anonymous" type="text/javascript"></script><style type="text/css">
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }
  }
</style>--></head>
<body class="">
<?php 
if(isset($_SESSION["username"])){
  echo "<input type='hidden' value='true' id='session'></input>";
}?>
		<script>
			dataLayer = [{
				"PageView": "\x2Ffr\x2Fenterprise\x2Fsolutions"
				
			}];
		</script>
		
	<div class="sr-only"><a href="http://backend.iferu.com/fr/enterprise/solutions#main-content" tabindex="-1">Ignorer la navigation</a></div>

    <div class="corpsol" id="main-content" role="main">
	<?php 
 include ("header_eng.php")
 
?>
<style>
.navigation__search{
	height:53px;
}
</style>
<!--- START MAIN_BODY -->
<div class="pageheader corpsol-pageheader" style="background-image: url(/www/images/enterprise/v3_corpsol/head.jpg)">
	
	<div class="pageheader__container pageheader__container--light pageheader__container--jumbo container">
		<div class="row">
		<div class="col-lg-offset-2 col-lg-8">
			
		<h1> 
			Learning at the Speed of Business 
		</h1>
		<p>
			iferu's corporate solution enables companies to foster learning
 cultures by providing tools that allow users to learn as they work, 
rather than carving out time in their over-packed schedules 
		</p>
		<!--<a href="./en-subscribe-products.php" class="btn btn-warning">Request a Demo </a>-->
	
		</div>
		</div>
	</div>
</div>

<div class="container">
			<div class="page-section
	 page-section--light
	 page-section--overlap-top
	
	
	
	 page-section--shadow
	 corpsol-section-empower corpsol-section">
				
     <h2>
			Empower Your Learning Culture 
		</h2>
		<div class="empower-illustration">
			<div class="empower-fields">
            <div class="empower-field">
					<img src="page_solution_entrprise_fichiers/empower_003.svg">
					<p>
						Transform skills 
					</p>
				</div>
				<div class="empower-field">
					<img src="page_solution_entrprise_fichiers/empower.svg">
					<p>
						Update knowledge  
					</p>
				</div>
				<div class="empower-field">
					<img src="page_solution_entrprise_fichiers/empower_002.svg">
					<p>
						Stay current
					</p>
				</div>
			</div>
		</div>
		<p>
			Rapidly evolving technology has created a new imperative for people to reskill and upskill throughout their lives. <br>
Help your employees stay competitive by providing them with the knowledge they need, summarized into concise abstracts. 
		</p>
	
			</div>
		</div>
        <hr class="visible-xs-block">
        <div class="container">
			<div class="page-section
	 page-section--light
	
	
	
	
	
	 upskill-section corpsol-section">
				
		<h2 class="js-cta-trigger">
			Upskill Your Workforce 
		</h2>
		<p>
			Today's business environment is changing so fast that it's hard to 
keep up. iferu keeps your employees informed on the hot topics 
shaping the business world - and enables them to update their skills in 
the flow of work. 
		</p>
		<div class="hidden-xs">
			<img class="upskill-section__img" src="page_solution_entrprise_fichiers/upskill.svg">
		</div>
	
			</div>
		</div>
        <div class="gensec corpsol-stories corpsol-section corpsol-section--blue">
	<div class="container">
      <div class="row">
                     <div class="corpsol-stories__header">
                     <h2 class="corpsol-stories__title">
				Customer Success Story
			</h2>
		<!--	<a class="corpsol-stories__view-all-link" href="http://backend.iferu.com/en/enterprise/casestudies" target="blank">Learn more about other customer stories here≫</a>
                -->    </div>
                   <div class="slick-container" style="display:flex;" data-slick='{
                            "arrows": true,
                            "dots" : false,
                            "autoplay " : false,
                            "slidesToShow":1,
                            "slidesToScroll": 1,
                            "adaptiveHeight": true,
                            "infinite": false
                        }'>
						<div class="slick-item">
				<div class="corpsol-stories__story">
					<div class="corpsol-stories__logo visible-xs">
						<img class="corpsol-stories__logo-img" src="page_solution_entrprise_fichiers/logo_004.svg">
					</div>
					<div class="corpsol-stories__challenge">
						<div class="corpsol-stories__quote-box">
							<p class="corpsol-stories__quote">“iferu certainly supports our goal of making learning 'easy, attractive, social and timely'.”</p>
							<p>Peter Yarrow, Global Head of Learning &amp; Proposition </p>
						</div>
						<div class="corpsol-stories__challenge-box">
							<h3>Challenge</h3>
							<p>Standard Life Aberdeen recognized that when going through a 
sustained period of change, it was essential to provide relevant 
development support. </p>
						</div>
					</div>
					<div class="corpsol-stories__results">
						<div class="corpsol-stories__results-title">
							<h3>Results</h3>
							<div class="corpsol-stories__logo-img-box--desktop">
								<img class="corpsol-stories__logo-img--desktop hidden-xs" src="page_solution_entrprise_fichiers/logo_004.svg">
							</div>
						</div>
						<div class="corpsol-stories__results-list">
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_009.svg">
								<p>70% growth in usage </p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_005.svg">
								<p>Self-directed learning as part of the change management process </p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_010.svg">
								<p>Quick Access to compressed expert knowledge </p>
							</div>
						</div>
						<!--<strong class="corpsol-stories__results-link"><a href="http://backend.iferu.com/www/docs/casestudies/standardlife.pdf" tabindex="-1">Learn more here &gt;&gt; </a></strong>
					--></div>
				</div>
			</div><div class="slick-item">
					<div class="corpsol-stories__story">
						<div class="corpsol-stories__logo visible-xs">
							<img class="corpsol-stories__logo-img" src="page_solution_entrprise_fichiers/logo_003.svg">
						</div>
						<div class="corpsol-stories__challenge">
							<div class="corpsol-stories__quote-box">
								<p class="corpsol-stories__quote">“iferu is my daily 
digest. The summaries are easy to read and easy to use. This allows us 
to raise awareness among employees about relevant topics.”</p>
								<p>Frédéric Hebert, Head of Digital Learning</p>
							</div>
							<div class="corpsol-stories__challenge-box">
								<h3>Challenge</h3>
								<p>Frédéric has thus grappled with a challenge that many L&amp;D
 experts share: How do you promote curiosity and foster a culture of 
learning among a diverse, global workforce?</p>
							</div>
						</div>
						<div class="corpsol-stories__results">
							<div class="corpsol-stories__results-title">
								<h3>Results</h3>
								<div class="corpsol-stories__logo-img-box--desktop">
									<img class="corpsol-stories__logo-img--desktop hidden-xs" src="page_solution_entrprise_fichiers/logo_003.svg">
								</div>
							</div>
							<div class="corpsol-stories__results-list">
								<div class="corpsol-stories__result">
									<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_002.svg">
									<p>Fostered a learning culture</p>
								</div>
								<div class="corpsol-stories__result">
									<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_009.svg">
									<p>Increased iferu usage by 138%</p>
								</div>
								<div class="corpsol-stories__result">
									<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_005.svg">
									<p>Supports multinational teams</p>
								</div>
							</div>
							<!--<strong class="corpsol-stories__results-link"><a href="http://backend.iferu.com/www/docs/casestudies/danone.pdf" tabindex="0">Learn more here &gt;&gt; </a></strong>
						--></div>
					</div>
				</div><div class="slick-item">
				<div class="corpsol-stories__story">
					<div class="corpsol-stories__logo visible-xs">
						<img class="corpsol-stories__logo-img" src="page_solution_entrprise_fichiers/logo.svg">
					</div>
					<div class="corpsol-stories__challenge">
						<div class="corpsol-stories__quote-box">
							<p class="corpsol-stories__quote">“iferu brings a huge 
amount of knowledge, fresh ideas and new perspectives into our learning 
programs at Atlassian, and it does so in a simple and concise manner.”</p>
							<p>B.J. Schone | Digital Learning Manager</p>
						</div>
						<div class="corpsol-stories__challenge-box">
							<h3>Challenge</h3>
							<p>Atlassian faces the challenge of integrating significant 
numbers of new employees into its rapidly growing workforce every month.
   </p>
						</div>
					</div>
					<div class="corpsol-stories__results">
						<div class="corpsol-stories__results-title">
							<h3>Results</h3>
							<div class="corpsol-stories__logo-img-box--desktop">
								<img class="corpsol-stories__logo-img--desktop hidden-xs" src="page_solution_entrprise_fichiers/logo.svg">
							</div>
						</div>
						<div class="corpsol-stories__results-list">
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_008.svg">
								<p>Facilitates self-directed learning</p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result.svg">
								<p>1/3 of employees use iferu </p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_007.svg">
								<p>Easy integration into LMS </p>
							</div>
						</div>
					<!--	<strong class="corpsol-stories__results-link"><a href="http://backend.iferu.com/www/docs/casestudies/atlassian.pdf" tabindex="-1">Learn more here &gt;&gt; </a></strong>
					--></div>
				</div>
			</div><div class="slick-item">
				<div class="corpsol-stories__story">
					<div class="corpsol-stories__logo visible-xs">
						<img class="corpsol-stories__logo-img" src="page_solution_entrprise_fichiers/logo_002.svg">
					</div>
					<div class="corpsol-stories__challenge">
						<div class="corpsol-stories__quote-box">
							<p class="corpsol-stories__quote">“The client relationship we have with iferu allows us to get the most benefit from our investment.”</p>
							<p>Treacy Stewart, Director, Global Learning and Performance </p>
						</div>
						<div class="corpsol-stories__challenge-box">
							<h3>Challenge</h3>
							<p>To create a new culture of employee engagement, Mondelez faces
 the challenge of developing the  company?s global workforce and 
encouraging curiosity among employees. </p>
						</div>
					</div>
					<div class="corpsol-stories__results">
						<div class="corpsol-stories__results-title">
							<h3>Results</h3>
							<div class="corpsol-stories__logo-img-box--desktop">
								<img class="corpsol-stories__logo-img--desktop hidden-xs" src="page_solution_entrprise_fichiers/logo_002.svg">
							</div>
						</div>
						<div class="corpsol-stories__results-list">
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_003.svg">
								<p>Trust-based Partnership with Customer Success Team</p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_006.svg">
								<p>Blended Learning through online- and offline activities </p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_004.svg">
								<p>Seamless SSO Integration</p>
							</div>
						</div>
						<!--<strong class="corpsol-stories__results-link"><a href="http://backend.iferu.com/www/docs/casestudies/mondelez.pdf" tabindex="-1">Learn more here &gt;&gt; </a></strong>
					--></div>
				</div>
			</div><div class="slick-item">
				<div class="corpsol-stories__story">
					<div class="corpsol-stories__logo visible-xs">
						<img class="corpsol-stories__logo-img" src="page_solution_entrprise_fichiers/logo_004.svg">
					</div>
					<div class="corpsol-stories__challenge">
						<div class="corpsol-stories__quote-box">
							<p class="corpsol-stories__quote">“iferu certainly supports our goal of making learning 'easy, attractive, social and timely'.”</p>
							<p>Peter Yarrow, Global Head of Learning &amp; Proposition </p>
						</div>
						<div class="corpsol-stories__challenge-box">
							<h3>Challenge</h3>
							<p>Standard Life Aberdeen recognized that when going through a 
sustained period of change, it was essential to provide relevant 
development support. </p>
						</div>
					</div>
					<div class="corpsol-stories__results">
						<div class="corpsol-stories__results-title">
							<h3>Results</h3>
							<div class="corpsol-stories__logo-img-box--desktop">
								<img class="corpsol-stories__logo-img--desktop hidden-xs" src="page_solution_entrprise_fichiers/logo_004.svg">
							</div>
						</div>
						<div class="corpsol-stories__results-list">
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_009.svg">
								<p>70% growth in usage </p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_005.svg">
								<p>Self-directed learning as part of the change management process </p>
							</div>
							<div class="corpsol-stories__result">
								<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_010.svg">
								<p>Quick Access to compressed expert knowledge </p>
							</div>
						</div>
						<!--<strong class="corpsol-stories__results-link"><a href="http://backend.iferu.com/www/docs/casestudies/standardlife.pdf" tabindex="-1">Learn more here &gt;&gt; </a></strong>
					--></div>
				</div>
			</div><div class="slick-item">
					<div class="corpsol-stories__story">
						<div class="corpsol-stories__logo visible-xs">
							<img class="corpsol-stories__logo-img" src="page_solution_entrprise_fichiers/logo_003.svg">
						</div>
						<div class="corpsol-stories__challenge">
							<div class="corpsol-stories__quote-box">
								<p class="corpsol-stories__quote">“iferu is my daily 
digest. The summaries are easy to read and easy to use. This allows us 
to raise awareness among employees about relevant topics.”</p>
								<p>Frédéric Hebert, Head of Digital Learning</p>
							</div>
							<div class="corpsol-stories__challenge-box">
								<h3>Challenge</h3>
								<p>Frédéric has thus grappled with a challenge that many L&amp;D
 experts share: How do you promote curiosity and foster a culture of 
learning among a diverse, global workforce?</p>
							</div>
						</div>
						<div class="corpsol-stories__results">
							<div class="corpsol-stories__results-title">
								<h3>Results</h3>
								<div class="corpsol-stories__logo-img-box--desktop">
									<img class="corpsol-stories__logo-img--desktop hidden-xs" src="page_solution_entrprise_fichiers/logo_003.svg">
								</div>
							</div>
							<div class="corpsol-stories__results-list">
								<div class="corpsol-stories__result">
									<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_002.svg">
									<p>Fostered a learning culture</p>
								</div>
								<div class="corpsol-stories__result">
									<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_009.svg">
									<p>Increased iferu usage by 138%</p>
								</div>
								<div class="corpsol-stories__result">
									<img class="corpsol-stories__result-img hidden-xs" src="page_solution_entrprise_fichiers/result_005.svg">
									<p>Supports multinational teams</p>
								</div>
							</div>
						<!--	<strong class="corpsol-stories__results-link"><a href="http://backend.iferu.com/www/docs/casestudies/danone.pdf" tabindex="-1">Learn more here &gt;&gt; </a></strong>
					-->	</div>
					</div>
				</div>
</div>         
</div> 
</div>
</div>	
</div>

<div class="advantages-section corpsol-section">
<div class="bullet-point bullet-point--left">
	<div class="bullet-point__img-mobile">
		
		
			<img src="page_solution_entrprise_fichiers/advantages_004.jpg">
		
	</div>
	<div class="container">
		<div class="bullet-point__container">
			<div class="bullet-point__img-desk">
				<div class="bullet-point__bluebar"></div>
				
				
					<img src="page_solution_entrprise_fichiers/advantages_004.jpg" class="bullet-point__img-desk-pic lazyload">
				
			</div>
			<div class="bullet-point__description"><div class="bullet-point__circle"></div>
				<h2>
					Curated Knowledge 
				</h2>
				<p>
					Our expert editorial team curates and summarizes the best business 
knowledge so your workforce can rely on trusted, current content
				</p>
			</div>
		</div>
	</div>
</div>
<div class="bullet-point ">
	<div class="bullet-point__img-mobile">
		
			<a href="http://backend.iferu.com/enterprise/tools">
			<img src="../fr-enterprise-solutions_files/advantages.tools.jpg"></a>
		
		
	</div>
	<div class="container">
		<div class="bullet-point__container">
			<div class="bullet-point__img-desk">
				<div class="bullet-point__bluebar"></div>
				
					<a href="http://backend.iferu.com/enterprise/tools"><img src="../fr-enterprise-solutions_files/advantages.tools.jpg" class="bullet-point__img-desk-pic lazyload"></a>
				
				
			</div>
			<div class="bullet-point__description"><div class="bullet-point__circle"></div>
				<h2>
					Engaging Tools 
				</h2>
				<p>
					Our unique suite of products takes employee learning to the next 
level by developing learning habits, encouraging discussions and guiding
 abstract selection.  <a href="http://backend.iferu.com/enterprise/tools">Learn more &gt;&gt;</a>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="bullet-point bullet-point--left">
	<div class="bullet-point__img-mobile">
		
		
			<img src="page_solution_entrprise_fichiers/advantages.jpg">
		
	</div>
	<div class="container">
		<div class="bullet-point__container">
			<div class="bullet-point__img-desk">
				<div class="bullet-point__bluebar"></div>
				
				
					<img src="page_solution_entrprise_fichiers/advantages.jpg" class="bullet-point__img-desk-pic lazyload">
				
			</div>
			<div class="bullet-point__description"><div class="bullet-point__circle"></div>
				<h2>
					Dedicated Guidance 
				</h2>
				<p>
					Our trained Customer Success Managers work with you to suggest 
solutions based on your company's needs and provide ongoing support as 
you create learning initiatives. 
				</p>
			</div>
		</div>
	</div>
</div>




 

<div class="bullet-point ">
	<div class="bullet-point__img-mobile">
		
		
			<img src="../fr-enterprise-solutions_files/advantages.custom.jpg">
		
	</div>
	<div class="container">
		<div class="bullet-point__container">
			<div class="bullet-point__img-desk">
				<div class="bullet-point__bluebar"></div>
				
				
					<img src="../fr-enterprise-solutions_files/advantages.custom.jpg" class="bullet-point__img-desk-pic lazyload">
				
			</div>
			<div class="bullet-point__description"><div class="bullet-point__circle"></div>
				<h2>
					Customized Experiences 
				</h2>
				<p>
					Our Corporate Portal enables you to customize your employees' 
learning journey to match your company's initiatives and goals.    
				</p>
			</div>
		</div>
	</div>
</div>




 

<div class="bullet-point bullet-point--left">
	<div class="bullet-point__img-mobile">
		
		
			<img src="page_solution_entrprise_fichiers/advantages_005.jpg">
		
	</div>
	<div class="container">
		<div class="bullet-point__container">
			<div class="bullet-point__img-desk">
				<div class="bullet-point__bluebar"></div>
				
				
					<img src="page_solution_entrprise_fichiers/advantages_005.jpg" class="bullet-point__img-desk-pic lazyload">
				
			</div>
			<div class="bullet-point__description"><div class="bullet-point__circle"></div>
				<h2>
					Ever-Growing Library 
				</h2>
				<p>
					Our library is constantly expanding, with new content added every 
day. iferu's concise summaries span a broad range of topics from 
sources such as books, videos, articles and podcasts. With content 
offered in seven languages, our library caters to a global user base.  
				</p>
			</div>
		</div>
	</div>
</div>
	</div>

	<hr class="container">
	
	





<div class="gensec integration-section corpsol-section">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			 	<div class="gensec-content">
					
		<!--<a href="http://backend.iferu.com/en/enterprise/request-demo" target="blank">-->
			<div class="description">
				<h2>
					Seamless Integration 
				</h2>
				<p>
					Our content integrates into the top LMS and LXP systems, opening 
the possibility to blend iferu content into established learning 
initiatives or building new experiences based on abstracts. 
				</p>
			</div>
			<div class="integrations">
				<div class="integration">
					<img src="page_solution_entrprise_fichiers/LinkedinLearning.svg">
				</div>
				<div class="integration">
					<img src="page_solution_entrprise_fichiers/SAP.svg">
				</div>
				<div class="integration">
					<img src="page_solution_entrprise_fichiers/Crossknowledge.svg">
				</div>
				<div class="integration">
					<img src="page_solution_entrprise_fichiers/edcast.svg">
				</div>
				<div class="integration">
					<img src="page_solution_entrprise_fichiers/cornerstone.svg">
				</div>
				<div class="integration">
					<img src="page_solution_entrprise_fichiers/degreed.svg">
				</div>
			</div>
		<!--</a>-->
	
				</div>			
			</div>
		</div>
	</div>
</div>







<div class="gensec corpsol-section corpsol-section-learn corpsol-section--blue">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			 	<div class="gensec-content">
					
		<div class="corpsol-section-learn__img">
			<img src="../fr-enterprise-solutions_files/culture.png">
		</div>
		<div class="corpsol-section-learn__desc">
			<h2>
				Learn how to develop a high-impact learning culture
			</h2>
			<p>
				</p><p>For the past twenty years, we've helped
organizations build cultures that
prioritize learning and growth. Download
the white paper to:</p>
<ul><li>Learn what makes a high-impact
learning culture unique.</li>
<li>Learn why your organization needs a
high-impact learning culture.</li>
<li>See five steps to developing your own
learning culture.</li></ul>
			<p></p>
	
			<div class="corpsol-section-learn__desc-learn-link">
				<a class="btn btn-primary" target="blank" href="../iferu_guide_learningculture_french_final.pdf">Download White Paper</a>
			</div>
		</div>
	
				</div>			
			</div>
		</div>
	</div>
</div>
<div class="gensec unlock-section corpsol-section">
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			 	<div class="gensec-content">
					
		<div class="container">
			





<div class="corpclients-box">
    
        <h2 class="corpclients-box__title"><!--<a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->Unlock Your Company's Potential </a></h2>
    
    
        <p class="corpclients-box__text">iferu is the trusted learning solution for one third of the Fortune 500. </p>
    
    <div class="corpclients-box__logos">
	    <!--<a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	    	<div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/eon.svg" alt="logo-eon" class="corpclients-box__img"></div>
	    </a> 
	   <!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/ferrero.svg" alt="logo-ferrero" class="corpclients-box__img"></div>
	    </a>
	   <!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/mastercard.svg" alt="logo-mastercard" class="corpclients-box__img"></div>
	    </a>
	   <<!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/microsoft.svg" alt="logo-microsoft" class="corpclients-box__img"></div>
	    </a>
	   <!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/adobe.svg" alt="logo-adobe" class="corpclients-box__img"></div>
	    </a>
	   <!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/ey.svg" alt="logo-ey" class="corpclients-box__img"></div>
	    </a>
	   <!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/daimler.svg" alt="logo-daimler" class="corpclients-box__img"></div>
	    </a>
	    <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/ge.svg" alt="logo-ge" class="corpclients-box__img"></div>
	    </a>
	    <!--<a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/facebook.svg" alt="logo-facebook" class="corpclients-box__img"></div>
	    </a>
	   <!-- <a href="http://backend.iferu.com/enterprise/casestudies" class="corpclients-box__link">-->
	        <div class="corpclients-box__img-box"><img src="page_solution_entrprise_fichiers/unilever.svg" alt="logo-unilever" class="corpclients-box__img"></div>
	    </a>
	</div>
</div>

		</div>
	
				</div>			
			</div>
		</div>
	</div>
</div>

<div class="gensec contactform-section corpsol-section" style="color: white;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-6 col-lg-offset-3">
					<div class="gensec-content">
					<div class="b2b-lead-form">
							<h2>
								Contact us
							</h2>
							<form id="contactForm" action="/en/enterprise/contact" method="post">
							<div class="alert alert-info text-center">Companies worldwide are 
						facing new challenges: We offer a free trial to qualifying organizations
						that want to empower their workforce with curated expert knowledge. 
						Contact us to learn more.</div>
						<h2 style="color:black;" > Contact us: contact@iferu.com </h2>
</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div><!--- END MAIN BODY -->
<!--- END WRAPPER -->
<?php include('../en/en-footer.php'); ?>
<!--
<script src="../fr-enterprise-solutions_files/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js"></script>
<script src="../fr-enterprise-solutions_files/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js"></script>-->
<script src="../fr-enterprise-solutions_files/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js" defer></script>
<script src="../fr-enterprise-solutions_files/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js" defer></script>
<script src="../fr-enterprise-solutions_files/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js" defer></script>

<script src="../fr-enterprise-solutions_files/plugins-cb-263spjwuo2ql4ws8p9pvlrp95thejfn.js" defer></script>
<script src="../fr-enterprise-solutions_files/main-cb-jelr67bnj55qdbl7da6fg4blydtxduv.js" defer></script>

<script src="../fr-enterprise-solutions_files/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js" defer></script>
</body>
</html>
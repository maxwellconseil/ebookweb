<div id="not_available" class="col-sm-offset-3 col-sm-8">
      <div class="previewInfo previewInfo-blank">
        <div class="row">
          <div class="col-sm-12 message">
            <p>
            We are awaiting publication rights for this book. The summary will be available as soon as we get them. This procedure may take some time. We thank you for your patience.
  </p>
  </div></div></div></div>
  <script>
      $('#not_available').nextAll().hide();
      </script>


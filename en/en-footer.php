

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<footer class="footer">
	<div class="container">

		
		
		<hr class="bold footer__divider">
		<div class="footer__menu">
		
			<div class="footer__menu-col panel">
				<a role="button" data-toggle="collapse" href="http://backend.iferu.com/en/explore#footer__menu-list-1" aria-expanded="false" aria-controls="footer__menu-list-1">
					<h5 class="footer__menu-title">Company<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
				</a>
				<ul id="footer__menu-list-1" class="footer__menu-list collapse" data-parent=".footer__menu">
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/about-us" class="footer__anchor">About Us</a></li>
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/next-gen-leaders" class="footer__anchor">#NextGenLeaders</a></li>
					
							<li class="footer__list-item"><a href="https://journal.iferu.com/en/iferu-international-book-award-2020/" target="_blank" class="footer__anchor" rel="noopener noreferrer">Book Award</a></li>
						
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/pages/jsp/Careers.jsp" class="footer__anchor">Careers</a></li>
					
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/how-it-works/faqs" class="footer__anchor">FAQs</a></li>
					<li class="footer__list-item"><a href="http://backend.iferu.com/Support.do" class="footer__anchor">Contact Us</a></li>
				</ul>
			</div>
			<div class="footer__menu-col panel">
				<a role="button" data-toggle="collapse" href="http://backend.iferu.com/en/explore#footer__menu-list-2" aria-expanded="false" aria-controls="footer__menu-list-2">
					<h5 class="footer__menu-title">Resources<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
				</a>
				<ul id="footer__menu-list-2" class="footer__menu-list collapse" data-parent=".footer__menu">
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/how-it-works/overview" class="footer__anchor">How It Works</a></li>
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/summary-suggestions" class="footer__anchor">Wish List</a></li>
					
					
					
					
						<li class="footer__list-item"><a href="https://journal.iferu.com/en/start" target="_blank" class="footer__anchor">Journal</a></li>
					
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/enterprise/casestudies" class="footer__anchor">Customer Success Stories</a></li>
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/press" class="footer__anchor">Press Kit</a></li>
					
				</ul>
			</div>
			<div class="footer__menu-col panel">
				<a role="button" data-toggle="collapse" href="http://backend.iferu.com/en/explore#footer__menu-list-3" aria-expanded="false" aria-controls="footer__menu-list-3">
					<h5 class="footer__menu-title">Partners<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
				</a>
				<ul id="footer__menu-list-3" class="footer__menu-list collapse" data-parent=".footer__menu">
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/publishers" class="footer__anchor">Publishers</a></li>
					
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/media-relations" class="footer__anchor">Media Relations</a></li>
					
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/affiliate-program" class="footer__anchor">Affiliate Partners</a></li>
					
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/alliance-partners" class="footer__anchor">Alliance Partners</a></li>
				</ul>
			</div>
			<div class="footer__menu-col panel">
				<a role="button" data-toggle="collapse" href="http://backend.iferu.com/en/explore#footer__menu-list-4" aria-expanded="false" aria-controls="footer__menu-list-4">
					<h5 class="footer__menu-title">Product<i class="ico-arrow-down footer__icon" aria-hidden="true"></i></h5>
				</a>
				<ul id="footer__menu-list-4" class="footer__menu-list collapse" data-parent=".footer__menu">
					
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/trial" class="footer__anchor">Free Trial</a></li>
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/subscribe/products" class="footer__anchor">Personal Subscriptions</a></li>
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/studentpass" class="footer__anchor">Student Subscriptions</a></li>
						<li class="footer__list-item"><a href="http://backend.iferu.com/en/subscribe/gifts" class="footer__anchor">Gift Subscriptions</a></li>
					
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/enterprise/solutions" class="footer__anchor">Corporate Solutions</a></li>
					<li class="footer__list-item"><a href="http://backend.iferu.com/en/custom-summaries" target="_blank" class="footer__anchor">Custom Summary Service</a></li>
					
				</ul>
			</div>
		</div>
		

		<hr class="dark footer__divider">

		<div class="footer__legal">

			
			<div class="footer__language">
				
				<h5 class="sr-only">Language</h5>

				<ul class="footer__language-list"> 
				
								
						<li class="footer__list-item">
						<strong>English</strong>
						</li>

						<li class="footer__list-item">
						<form  id="frenchform" action="" method="post">
									<a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Deutsch </a>	
										<input type="hidden" name="frenchform" >
									</form>
						<!-- <a href="http://backend.iferu.com/de/" class="footer__anchor">Deutsch</a> -->
						</li>
					
						<li class="footer__list-item">
						<form  id="frenchform" action="" method="post">
									<a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Español </a>	
										<input type="hidden" name="frenchform" >
									</form>
						<!-- <a href="http://backend.iferu.com/es/" class="footer__anchor">Español</a> -->
						</li>
					
						<li class="footer__list-item">
						<form  id="frenchform" action="" method="post">
									<a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Русский </a>	
										<input type="hidden" name="frenchform" >
									</form>
						<!-- <a href="http://backend.iferu.com/ru/" class="footer__anchor">Русский</a> -->
						</li>
					
						<li class="footer__list-item">
						<form  id="frenchform" action="" method="post">
								<a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >中文</a>	
								<input type="hidden" name="frenchform" >
						</form>
						<!-- <a href="http://backend.iferu.com/zh/" class="footer__anchor">中文</a> -->
						</li>
					
						<li class="footer__list-item">
						<form  id="frenchform" action="" method="post">
									<a  class="footer__anchor" onClick="javascript:document.getElementById('frenchform').submit();" >Português </a>	
										<input type="hidden" name="frenchform" >
									</form>
						<!-- <a href="http://backend.iferu.com/pt/" class="footer__anchor">Português</a> -->
						</li>
					
						<li class="footer__list-item">
						
						<form  id="englishform" action="" method="post">
									<a  class="footer__anchor" onClick="javascript:document.getElementById('englishform').submit();" >Francais </a>	
										<input type="hidden" name="englishform" >
									</form>
									<?php 
									include("../change_langage.php");?>
						</li>
					
				</ul>
			
			</div>

	
			

			<hr class="footer__legal-divider footer__divider dark">

			
			<ul class="footer__legal-list">
				
				<li class="footer__list-item"><a href="en-privacy-policy-single.php" class="footer__anchor">Privacy Policy</a></li>
				<li class="footer__list-item"><a href="en-terms-of-use.php" class="footer__anchor">Terms of Use</a></li>
				
						
					
				<li class="footer__list-item"><a href="http://backend.iferu.com/www/docs/tc/iferu-tc-b2c-en.pdf" target="_blank" class="footer__anchor">Operating Agreement</a></li>
				<li class="footer__list-item"><a href="http://backend.iferu.com/en/accessibility" class="footer__anchor">Accessibility</a></li>
			</ul>

			
			<div class="footer__social">
        <h5 class="sr-only">Connexion</h5>
        <ul class="footer__social-icons">
          <li class="footer__list-item"><a target="_blank" href="http://www.facebook.com/iferu" title="Retrouvez-nous sur Facebook" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-facebook-official" aria-hidden="true"></i><span class="sr-only">Facebook</span></a></li>
          <li class="footer__list-item"><a target="_blank" href="https://www.linkedin.com/company/iferu-com/" title="Rejoignez notre communauté LinkedIn" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-linkedin-square" aria-hidden="true"></i><span class="sr-only">LinkedIn</span></a></li>
          <li class="footer__list-item"><a target="_blank" href="https://www.instagram.com/iferusolution/" title="Retrouvez-nous sur Instagram" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-instagram" aria-hidden="true"></i><span class="sr-only">Instagram</span></a></li>
          <li class="footer__list-item"><a target="_blank" href="https://twitter.com/IferuSolution" title="Suivez-nous sur Twitter" class="footer__anchor" rel="noopener noreferrer"><i class="fa fa-twitter-square" aria-hidden="true"></i><span class="sr-only">Twitter</span></a></li>
          
          
        </ul>
      </div>

			
			<div class="footer__copyright">
				<a href="http://backend.iferu.com/en/pages/jsp/Copyright.jsp" class="footer__anchor">
					<p class="footer__paragraph">© 1999-2021, iferu</p>
				</a>
			</div>

		</div>

	</div>
</footer>

<div id="fb-root"></div>
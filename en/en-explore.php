<?php 
			 session_start();  ?>
			
<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html class="js no-touch" lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>iferu</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="fb:app_id" content="143891362343386">
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" defer>
<link rel="search" href="../opensearch.xml" type="application/opensearchdescription+xml" title="iferu" defer>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" defer>
<link rel="stylesheet" type="text/css" href="../include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css" defer>
<script type="text/javascript" src="../fr-explore_files/996526df3c"></script><script type="text/javascript" src="../fr-explore_files/pd.js"></script><script src="../fr-explore_files/nr-1184.min.js"></script><script src="../fr-explore_files/feedbackweb-new.js" type="text/javascript" id="hs-feedback-web-4918719" crossorigin="anonymous" data-hubspot-feedback-portal-id="4918719" data-hubspot-feedback-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../fr-explore_files/leadflows.js" type="text/javascript" id="LeadFlows-4918719" crossorigin="anonymous" data-leadin-portal-id="4918719" data-leadin-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../fr-explore_files/4918719.js" type="text/javascript" id="cookieBanner-4918719" data-cookieconsent="ignore" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script src="../fr-explore_files/4918719(1).js" type="text/javascript" id="hs-analytics"></script><script src="../fr-explore_files/fb.js" type="text/javascript" id="hs-ads-pixel-4918719" data-ads-portal-id="4918719" data-ads-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod"></script><script async="" src="../fr-explore_files/clearbit.min.js"></script><script async="" src="../fr-explore_files/uwt.js"></script><script src="../fr-explore_files/832786183443261" async=""></script><script async="" src="../fr-explore_files/fbevents.js"></script><script async="" src="../fr-explore_files/tfa.js" id="tb_tfa_script"></script><script async="" src="../fr-explore_files/obtp.js" type="text/javascript"></script><script type="text/javascript" async="" src="../fr-explore_files/bat.js"></script><script type="text/javascript" async="" src="../fr-explore_files/insight.min.js"></script><script type="text/javascript" async="" src="../fr-explore_files/analytics.js"></script><script type="text/javascript" async="" src="../fr-explore_files/dc.js"></script><script async="" src="../fr-explore_files/gtm.js"></script><script type="text/javascript" async="async" src="../fr-explore_files/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js"></script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"996526df3c",applicationID:"3375187"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,y=["click","keydown","mousedown","pointerdown","touchstart"];y.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?f(e,c,o):o()}function n(n,r,i,o){if(!p.aborted||o){e&&e(n,r,i);for(var a=t(i),c=v(n),f=c.length,u=0;u<f;u++)c[u].apply(a,r);var d=s[w[n]];return d&&d.push([b,n,r,a]),a}}function l(e,t){h[e]=v(e).concat(t)}function m(e,t){var n=h[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return h[e]||[]}function g(e){return d[e]=d[e]||i(n)}function y(e,t){u(e,function(e,n){t=t||"feature",w[n]=t,t in s||(s[t]=[])})}var h={},w={},b={on:l,addEventListener:l,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:y,abort:a,aborted:!1};return b}function o(){return new r}function a(){(s.api||s.feature)&&(p.aborted=!0,s=p.backlog={})}var c="nr@context",f=e("gos"),u=e(6),s={},d={},p=t.exports=i();p.backlog=s},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!E++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(h,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var y=""+location,h={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1184.min.js"},w=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:y,features:{},xhrWrappable:w,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var E=0},{}],"wrap-function":[function(e,t,n){function r(e){return!(e&&e instanceof Function&&e.apply&&!e[a])}var i=e("ee"),o=e(7),a="nr@original",c=Object.prototype.hasOwnProperty,f=!1;t.exports=function(e,t){function n(e,t,n,i){function nrWrapper(){var r,a,c,f;try{a=this,r=o(arguments),c="function"==typeof n?n(r,a):n||{}}catch(u){p([u,"",[r,a,i],c])}s(t+"start",[r,a,i],c);try{return f=e.apply(a,r)}catch(d){throw s(t+"err",[r,a,d],c),d}finally{s(t+"end",[r,a,f],c)}}return r(e)?e:(t||(t=""),nrWrapper[a]=e,d(e,nrWrapper),nrWrapper)}function u(e,t,i,o){i||(i="");var a,c,f,u="-"===i.charAt(0);for(f=0;f<t.length;f++)c=t[f],a=e[c],r(a)||(e[c]=n(a,u?c+i:i,o,c))}function s(n,r,i){if(!f||t){var o=f;f=!0;try{e.emit(n,r,i,t)}catch(a){p([a,n,r,i])}f=o}}function d(e,t){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(e);return n.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(r){p([r])}for(var i in e)c.call(e,i)&&(t[i]=e[i]);return t}function p(t){try{e.emit("internal-error",t)}catch(n){}}return e||(e=i),n.inPlace=u,n.flag=a,n}},{}]},{},["loader"]);</script>
	
	<link rel="stylesheet" type="text/css" href="../fr-explore_files/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css" defer>

<link rel="stylesheet" type="text/css" href="../fr-explore_files/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css" defer>

	<link rel="stylesheet" type="text/css" href="../fr-explore_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css" defer>
	
<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
	<script>
	window.VWO = window.VWO || [];
	window.VWO.push(['tag', 'Login', 'true', 'session']); 
	_vis_opt_check_segment = {'95': true };
	window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
	window.anonymous = 'true'; /* Used for VWO segmentation */
	</script>
	<script>
	var _vwo_code=(function(){
	var account_id=5083,
	settings_tolerance=2000,
	library_tolerance=2500,
	use_existing_jquery=false,
	// DO NOT EDIT BELOW THIS LINE
	f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script><script src="../fr-explore_files/j.php" type="text/javascript"></script>

<?php 
if(isset($_SESSION["username"])){
  echo "<input type='hidden' value='true' id='session'></input>";
}?>
<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->
<!--
<script src="../fr-explore_files/f.txt"></script>
<script src="../fr-explore_files/f(1).txt"></script>
<script src="../fr-explore_files/va-ffd39e015e5d25ce3182fc10ac34feab.js" crossorigin="anonymous" type="text/javascript"></script>
<script src="../fr-explore_files/track-ffd39e015e5d25ce3182fc10ac34feab.js" crossorigin="anonymous" type="text/javascript"></script><script src="../fr-explore_files/opa-e3db69dc6d0af05a6f9f8b749ec76384.js" crossorigin="anonymous" type="text/javascript"></script>
<style type="text/css">
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }

  }
</style><script type="text/javascript" src="../fr-explore_files/analytics"></script></head>-->
<body class=""><!--<div id="hs-feedback-fetcher">-->
 <!-- <iframe frameborder="0" src="../fr-explore_files/feedback-web-fetcher.html"></iframe></div>
-->
		<script>
			dataLayer = [{
				"PageView": "\x2Ffr\x2Fexplore"
				
			}];
		</script>
		
	<div class="sr-only"><a href="../fr/explore#main-content" tabindex="-1">Skip navigation</a></div>
	<?php 
 include ("header_eng.php")
?>
<style>
.navigation__search {height: 53px;}
</style>
	<div class="" id="main-content" role="main">

<!--- START MAIN_BODY -->
	
	<div class="container">
		<div class="page-header">
			<h1 style="margin-bottom: 0;">Explore</h1>
		</div>

      <div class="row">
        
      <div class="portlet-title">Trending Channel</div>
	   
	  <div class="slick-container"  data-slick="{
  &quot;arrows&quot;: true,
  &quot;dots&quot;: true,
  &quot;slidesToShow&quot;: 4,
  &quot;slidesToScroll&quot;: 4,
  &quot;adaptiveHeight&quot;: true,
  &quot;infinite&quot;: false,
  &quot;responsive&quot;: [ 
    {
    &quot;breakpoint&quot;: 1200,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 3,
      &quot;slidesToScroll&quot;: 3
      }
    },{
    &quot;breakpoint&quot;: 720,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 2,
      &quot;slidesToScroll&quot;: 2
      }
    },{
    &quot;breakpoint&quot;: 615,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 2,
      &quot;slidesToScroll&quot;: 2
      }
    },{
    &quot;breakpoint&quot;: 470,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 2,
      &quot;slidesToScroll&quot;: 2
      }
    }
  ]}"> 
  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                      
                                <div class="ga-channel-box">
	
	        <a href="en-model_contenu_de_chaine?idChaine=1002" tabindex="0">
			<img src="http://backend.iferu.com/channel-img/1002.jpg" data-src="/channel-img/1002-leadership-1535459141000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title">
	<a href="en-model_contenu_de_chaine?idChaine=1002" tabindex="0">Leadership</a>
	</div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                         <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=94151" tabindex="0">
	<img src="http://backend.iferu.com/channel-img/94151.jpg" data-src="/channel-img/94151-personal-development-1561992647000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=94151" tabindex="0">Personal Development</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
          






<div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1050" tabindex="0">
	<img src="http://backend.iferu.com/channel-img/1050.jpg" data-src="/channel-img/1050-career-1535362881000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1050" tabindex="0">Career</a></div>
	
	
</div>
				</div>

				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
          
                            <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1003" tabindex="0">
	<img src="http://backend.iferu.com/channel-img/1003.jpg" data-src="/channel-img/1003-management-1550826300000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1003" tabindex="0">Management</a></div>
	
	
</div>
				</div>
                        
                                  
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
          
                                  <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=90873" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/90873.jpg" data-src="/channel-img/90873-workplace-skills-1552405529000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=90873" tabindex="-1">Workplace Skills</a></div>
	
	
</div>
				</div>

				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                              <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1898" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1898.jpg" data-src="/channel-img/1898-technology-1537536783000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1898" tabindex="-1">Technology</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                              <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1005" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1005.jpg" data-src="/channel-img/1005-innovation-1535455546000.jpg?s=L" alt="" class="channel-cover lazyloaded"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1005" tabindex="-1">Innovation</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                              <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1063" tabindex="-1">
	<img src="http://backend.iferu.com//channel-img/1063.jpg" data-src="/channel-img/1063-life-advice-1552384496000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1063" tabindex="-1">Life Advice</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                      
                            <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1179" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1179.jpg"  alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1179" tabindex="-1">Women in Business</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                      






                        <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1073" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1073.jpg" data-src="/channel-img/1073-economics-1535369494000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1073" tabindex="-1">Economics</a></div>
	
	
</div>
				</div> <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                      
        <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1099" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1099.jpg" data-src="/channel-img/1099-science-1537536593000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1099" tabindex="-1">Science</a></div>
	
	
</div>
				</div>




				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
<div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1026" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1026.jpg" data-src="/channel-img/1026-marketing-1535454433000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1026" tabindex="-1">Marketing</a></div>
	
	
</div>
				</div> <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
        <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1859" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1859.jpg" data-src="/channel-img/1859-corporate-communication-1535457733000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1859" tabindex="-1">Corporate Communication</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

        <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1098" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1098.jpg" data-src="/channel-img/1098-society-1535379918000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1098" tabindex="-1">Society</a></div>
	
	
</div>
				</div>
				<div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
                                     <div class="ga-channel-box">
	
	<a href="en-model_contenu_de_chaine?idChaine=1100" tabindex="-1">
	<img src="http://backend.iferu.com/channel-img/1100.jpg" data-src="/channel-img/1100-history-1535376646000.jpg?s=L" alt="" class="channel-cover lazyload"></a>
	<div class="ga-channel-box-title"><a href="en-model_contenu_de_chaine?idChaine=1100" tabindex="-1">History</a></div>
	
	
</div>
				
          </div> 
  </div>
  </div>
  <div class="container">
  <div class="row">
                  <div class="portlet-header"> 
                                    <div class="portlet-options">     
                                                            
                                    &nbsp;<a href="http://backend.iferu.com/portlets/489/view-all">Show all</a> 
                                
                                   </div>
                                   <div class="portlet-title">
                                  
                                   <a class="portlet-title-link" href="http://backend.iferu.com/portlets/489/view-all">Popular Summaries&nbsp;</a>
                                    </div>
                  </div>
				
				  
				  <div class="slick-container" data-slick="{
  &quot;arrows&quot;: true,
  &quot;dots&quot;: true,
  &quot;slidesToShow&quot;: 7,
  &quot;slidesToScroll&quot;: 7,
  &quot;adaptiveHeight&quot;: true,
  &quot;infinite&quot;: false,
  &quot;responsive&quot;: [ 
    {
    &quot;breakpoint&quot;: 1200,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 5,
      &quot;slidesToScroll&quot;: 5
      }
    },{
    &quot;breakpoint&quot;: 720,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 4,
      &quot;slidesToScroll&quot;: 4
      }
    },{
    &quot;breakpoint&quot;: 615,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 3,
      &quot;slidesToScroll&quot;: 3
      }
    },{
    &quot;breakpoint&quot;: 470,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 2,
      &quot;slidesToScroll&quot;: 2
      }
    }
  ]}">   
  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide01">
						 <div class="ga-summary-box ">
        <div class="gsb-cover">
          
		<a href="en-book.php?id=3515" tabindex="0">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
	  <img src="http://backend.iferu.com/summary-img/3515.jpg" data-src="" alt="The 7 Habits Of Highly Effective People" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">The 7 Habits Of Highly Effective People</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=34182" tabindex="0">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/34182.jpg" data-src="" alt="Data Driven" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Data Driven</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=38849" tabindex="0">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/38849.jpg" data-src="" alt="No Filter" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">No Filter</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=32943" tabindex="0">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/32943.jpg" data-src="" alt="The Age Of Agile" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">The Age Of Agile</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=23518" tabindex="0">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/23518.jpg" data-src="" alt="Your Brain At Work" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Your Brain At Work</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=38582" tabindex="0">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/38582.jpg" data-src="" alt="The Advice Trap" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">The Advice Trap</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=1906" tabindex="0">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/1906.jpg" data-src="" alt="Emotional Intelligence" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Emotional Intelligence</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
  </div>  <div  class="slick-item slick-slide slick-active" data-slick-index="11" aria-hidden="false" style="width: 163px;height:200px;" tabindex="-1" role="option" aria-describedby="slick-slide01">

		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=26998" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/26998.jpg" data-src="" alt="Storytelling With Data" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Storytelling With Data</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="8" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide18">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=25953" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/25953.jpg" data-src="" alt="15 Secrets Successful People Know About Management" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">15 Secrets Successful People Know About Management</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="9" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide19">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=29695" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/29695.jpg" data-src="" alt="The Nature Fix" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">The Nature Fix</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="10" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide110">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=26522" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/26522.jpg" data-src="" alt="Invisible Influence" class="scover scover--book scover--s lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Invisible Influence</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="11" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide111">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=40001" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/40001.jpg" data-src="" alt="Disaster By Choice" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Disaster By Choice</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="12" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide112">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=34435" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/34435.jpg" data-src="" alt="Bad Advice" class="scover scover--book scover--s ls-is-cached lazyloaded">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Bad Advice</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">7</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="13" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide113">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=40004" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/40004.jpg" data-src="" alt="Too Smart" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Too Smart</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><!-- <div class="slick-item slick-slide" data-slick-index="14" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide114">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=41153" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/41153.jpg" data-src="" alt="Own Your Career Own Your Life" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Own Your Career Own Your Life</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">7</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div> --><div class="slick-item slick-slide" data-slick-index="15" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide115">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=15856" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
		  <img src="http://backend.iferu.com/summary-img/15856.jpg" data-src="" alt="Thinking, Fast and Slow" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Thinking, Fast and Slow</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="26" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide126">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=34797" tabindex="-1">
	  
	  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/34797.jpg" data-src="" alt="Decision Maker's Playbook" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Decision Maker's Playbook</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><!-- <div class="slick-item slick-slide" data-slick-index="17" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide117">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=40319" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/40319.jpg" data-src="" alt="You – According to Them" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">You – According to Them</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div> --><div class="slick-item slick-slide" data-slick-index="18" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide118">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=36101" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/36101.jpg" data-src="" alt="Decisive Intuition" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">Decisive Intuition</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="19" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide119">
		
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  <div class="ga-summary-box ">
	<div class="gsb-cover">
	  
		  <a href="en-book.php?id=29317" tabindex="-1">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
	  
	
  
  
	  
	  <img src="http://backend.iferu.com/summary-img/29317.jpg" data-src="" alt="7 Things Resilient People Do Differently" class="scover scover--book scover--s lazyload">
	</a>
		
	</div>
	<div class="ga-summary-box-title">7 Things Resilient People Do Differently</div>
	<div class="ga-summary-box-icons">
	  <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
	  <span data-icon=""></span>
	  
	</div>
  </div>
	  </div><div class="slick-item slick-slide" data-slick-index="20" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide120">
		
  
  
  
  
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=40967" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/40967.jpg"  alt="9 Things a Leader Must Do" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">9 Things a Leader Must Do</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="21" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide121">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=37371" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/37371.jpg"  alt="Everyone Deserves a Great Manager" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">Everyone Deserves a Great Manager</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
          
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="22" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide122">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=38359" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/38359.jpg"  alt="Get Out of Your Own Way" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">Get Out of Your Own Way</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">7</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="23" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide123">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=39525" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/39525.jpg"  alt="Love Your Imposter" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">Love Your Imposter</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">7</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="24" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide124">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=40173" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/40173.jpg"  alt="The Space Barons" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">The Space Barons</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="25" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide125">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=15856" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/15856.jpg"  alt="Thinking, Fast and Slow" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">Thinking, Fast and Slow</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="26" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide126">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=39659" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/39659.jpg"  alt="From the Sea to the C-Suite" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">From the Sea to the C-Suite</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="27" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide127">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=37764" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/37764.jpg"  alt="Game Changers" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">Game Changers</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
          
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="28" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide128">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=36826" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/36826.jpg" alt="The Creative Thinking Handbook" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">The Creative Thinking Handbook</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">9</span></span>
          
          
        </div>
      </div>
          </div><div class="slick-item slick-slide" data-slick-index="29" aria-hidden="true" style="width: 163px;" tabindex="-1" role="option" aria-describedby="slick-slide129">
            
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div class="ga-summary-box ">
        <div class="gsb-cover">
          
              <a href="en-book.php?id=40280" tabindex="-1">
      
      
      
      
      
      
      
      
      
      
      
      
      
      
          
        
      
      
          
          <img src="http://backend.iferu.com/summary-img/40280.jpg"  alt="Why Men Win at Work" class="scover scover--book scover--s lazyload">
        </a>
            
        </div>
        <div class="ga-summary-box-title">Why Men Win at Work</div>
        <div class="ga-summary-box-icons">
          <span class="gsb-rating" title="Rating"><i class="ico-star-empty"></i> <span class="gsb-rating-number">8</span></span>
          <span data-icon=""></span>
          
        </div>
      </div>
          </div></div></div>
                </div</div></div>
        
              
          </div>
        </div>


	
		<!-- Channel list -->
    <div class="container channel-overview" id="all-channels">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Channel Overview
				</h2>
				<!-- Channel search -->
				<!-- <div class="form-group chov__search-wrapper">
					
					<input class="chov__search-input form-control" type="search" placeholder="Search for channels">
					<span class="chov__search-btn-clear" hidden=""><icon class="ico-cancel"></icon></span>
				</div> -->
				<div class="chov__item-group chov__item-group--level-0">
					









<div class="chov__item chov__item--level-0 " data-channel-id="1002" data-title="Leadership" data-synonyms="leadership development">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1002#channel-1002--1002" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Leadership</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1002" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1002--1002" data-parent-channel-id="1002">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1120" data-title="Being CEO" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1120#channel-1002-1002-1120" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Being CEO</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1120" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1001" data-title="Business Leaders" data-synonyms="biographies; biography;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1001#channel-1002-1002-1001" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Business Leaders</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1001" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1002-1002-1001" data-parent-channel-id="1001">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1564" data-title="Steve Jobs" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1564#channel-1002-1001-1564" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Steve Jobs</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1564" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1338" data-title="Coaching" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1338#channel-1002-1002-1338" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Coaching</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1338" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1002-1002-1338" data-parent-channel-id="1338">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1187" data-title="Executive Coaching" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1187#channel-1002-1338-1187" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Executive Coaching</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1187" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1217" data-title="Decision Making" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1217#channel-1002-1002-1217" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Decision Making</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1217" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1002-1002-1217" data-parent-channel-id="1217">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1719" data-title="Game Theory" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1719#channel-1002-1217-1719" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Game Theory</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1719" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1288" data-title="Leadership Mistakes" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1288#channel-1002-1002-1288" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Leadership Mistakes</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1288" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="118955" data-title="Lessons from Sports" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=118955#channel-1002-1002-118955" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Lessons from Sports</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=118955" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1433" data-title="Middle Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1433#channel-1002-1002-1433" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Middle Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1433" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1185" data-title="Motivating People" data-synonyms="Motivating employees; inspiration; employee engagement;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1185#channel-1002-1002-1185" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Motivating People</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1185" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1328" data-title="Power" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1328#channel-1002-1002-1328" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Power</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1328" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1007" data-title="Leading Teams" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1007#channel-1002-1002-1007" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Leading Teams</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1007" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1002-1002-1007" data-parent-channel-id="1007">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1207" data-title="Remote Teams" data-synonyms="remote work; telework; remote; home office; distance working; virtual workplace; telecommuting; distributed work; virtual teams;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1207#channel-1002-1007-1207" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Remote Teams</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1207" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1277" data-title="Holding Meetings" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1277#channel-1002-1007-1277" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Holding Meetings</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1277" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1148" data-title="Performance Reviews" data-synonyms="performance appraisals">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1148#channel-1002-1002-1148" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Performance Reviews</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1148" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1600" data-title="Delegating" data-synonyms="micromanagement">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1600#channel-1002-1002-1600" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Delegating</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1600" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1003" data-title="Management" data-synonyms="Corporate Management;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1003#channel-1003--1003" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1003" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1003--1003" data-parent-channel-id="1003">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1120" data-title="Being CEO" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1120#channel-1003-1003-1120" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Being CEO</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1120" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1034" data-title="Corporate Governance" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1034#channel-1003-1003-1034" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Governance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1034" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1034" data-parent-channel-id="1034">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1031" data-title="Compliance" data-synonyms="regulation; rules ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1031#channel-1003-1034-1031" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Compliance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1031" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1003-1034-1031" data-parent-channel-id="1031">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1344" data-title="Fraud" data-synonyms="scam; graft; corporate corruption">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1344#channel-1003-1031-1344" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Fraud</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1344" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1521" data-title="Tax Havens" data-synonyms="offshore financial centers; tax jurisdictions">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1521#channel-1003-1031-1521" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Tax Havens</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1521" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1645" data-title="Economic Espionage" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1645#channel-1003-1034-1645" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic Espionage</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1645" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1012" data-title="Corporate Vision &amp; Mission" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1012#channel-1003-1003-1012" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Vision &amp; Mission</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1012" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1010" data-title="Crisis Management" data-synonyms="Natural Disasters">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1010#channel-1003-1003-1010" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Crisis Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1010" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1044" data-title="Digital Transformation" data-synonyms="DT; digital technology; digital solution; digital usage; going paperless; digital business;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1044#channel-1003-1003-1044" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Digital Transformation</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1044" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1096" data-title="Doing Business Abroad" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1096#channel-1003-1003-1096" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business Abroad</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1096" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1096" data-parent-channel-id="1096">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1633" data-title="Expatriates" data-synonyms="abroad;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1633#channel-1003-1096-1633" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Expatriates</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1633" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1208" data-title="Outsourcing and Offshoring" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1208#channel-1003-1096-1208" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Outsourcing and Offshoring</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1208" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1094" data-title="Doing Business in Africa" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1094#channel-1003-1003-1094" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in Africa</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1094" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1091" data-title="Doing Business in Asia" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1091#channel-1003-1003-1091" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in Asia</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1091" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1091" data-parent-channel-id="1091">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1342" data-title="Doing Business in China" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1342#channel-1003-1091-1342" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in China</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1342" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1003-1091-1342" data-parent-channel-id="1342">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1465" data-title="Advertising in China" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1465#channel-1003-1342-1465" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Advertising in China</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1465" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1174" data-title="Chinese Business Etiquette" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1174#channel-1003-1342-1174" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Chinese Business Etiquette</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1174" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1323" data-title="Doing Business in India" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1323#channel-1003-1091-1323" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in India</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1323" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1089" data-title="Doing Business in Europe" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1089#channel-1003-1003-1089" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in Europe</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1089" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1093" data-title="Doing Business in Latin America" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1093#channel-1003-1003-1093" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in Latin America</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1093" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1088" data-title="Doing Business in North America" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1088#channel-1003-1003-1088" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in North America</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1088" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1095" data-title="Doing Business in Oceania" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1095#channel-1003-1003-1095" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in Oceania</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1095" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1090" data-title="Doing Business in Russia" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1090#channel-1003-1003-1090" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in Russia</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1090" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1092" data-title="Doing Business in the Middle East" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1092#channel-1003-1003-1092" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Doing Business in the Middle East</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1092" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1008" data-title="Management Concepts" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1008#channel-1003-1003-1008" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Management Concepts</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1008" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1008" data-parent-channel-id="1008">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1651" data-title="Agility" data-synonyms="agile; co-evolution; edge of chaos; self-organization; organizational intelligence">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1651#channel-1003-1008-1651" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Agility</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1651" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1003-1008-1651" data-parent-channel-id="1651">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1716" data-title="Scrum" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1716#channel-1003-1651-1716" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Scrum</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1716" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1154" data-title="Corporate Entrepreneurship" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1154#channel-1003-1008-1154" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Entrepreneurship</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1154" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1337" data-title="Enterprise Resource Planning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1337#channel-1003-1008-1337" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Enterprise Resource Planning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1337" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1257" data-title="Lean Management" data-synonyms="lean; lean thinking">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1257#channel-1003-1008-1257" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Lean Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1257" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1600" data-title="Delegating" data-synonyms="micromanagement">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1600#channel-1003-1008-1600" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Delegating</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1600" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="89396" data-title="Organizational Self-Management" data-synonyms="teal; holacracy; autogestion; labor management">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89396#channel-1003-1008-89396" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Organizational Self-Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=89396" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1149" data-title="Measuring Business Performance" data-synonyms="Performance figures; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1149#channel-1003-1003-1149" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Measuring Business Performance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1149" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1433" data-title="Middle Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1433#channel-1003-1003-1433" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Middle Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1433" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1006" data-title="Project Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1006#channel-1003-1003-1006" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Project Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1006" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1006" data-parent-channel-id="1006">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1281" data-title="Event Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1281#channel-1003-1006-1281" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Event Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1281" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1295" data-title="Restructuring" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1295#channel-1003-1003-1295" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Restructuring</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1295" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1030" data-title="Risk Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1030#channel-1003-1003-1030" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Risk Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1030" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1030" data-parent-channel-id="1030">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1645" data-title="Economic Espionage" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1645#channel-1003-1030-1645" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic Espionage</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1645" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1441" data-title="Intellectual Property" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1441#channel-1003-1030-1441" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Intellectual Property</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1441" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1803" data-title="IT Security" data-synonyms="hack; cyber; attacks; information security; data protection; cyber risk; cyber crime; cybercrime; cybersecurity; computer security; information technology security; IT security; exploit; vulnerability; CVE; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1803#channel-1003-1030-1803" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">IT Security</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1803" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1067" data-title="Small Business" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1067#channel-1003-1003-1067" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Small Business</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1067" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1067" data-parent-channel-id="1067">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1065" data-title="Family Business" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1065#channel-1003-1067-1065" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Family Business</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1065" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1698" data-title="Social Entrepreneurship" data-synonyms="social start-up; greater good;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1698#channel-1003-1003-1698" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Social Entrepreneurship</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1698" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1417" data-title="Stakeholder Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1417#channel-1003-1003-1417" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Stakeholder Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1417" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1066" data-title="Starting a Business" data-synonyms="start-up; entrepreneurship;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1066#channel-1003-1003-1066" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Starting a Business</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1066" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1066" data-parent-channel-id="1066">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1420" data-title="The Business Plan" data-synonyms="business proposal; business planning;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1420#channel-1003-1066-1420" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The Business Plan</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1420" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1450" data-title="Venture Capital" data-synonyms="angel investing; equity investing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1450#channel-1003-1066-1450" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Venture Capital</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1450" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1004" data-title="Change Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1004#channel-1003-1003-1004" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Change Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1004" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1017" data-title="Strategy" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1017#channel-1003-1003-1017" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Strategy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1017" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1017" data-parent-channel-id="1017">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1245" data-title="Competitor Analysis" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1245#channel-1003-1017-1245" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Competitor Analysis</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1245" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1014" data-title="Corporate Alliances" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1014#channel-1003-1017-1014" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Alliances</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1014" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1032" data-title="Mergers &amp; Acquisitions" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1032#channel-1003-1017-1032" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mergers &amp; Acquisitions</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1032" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1003-1017-1032" data-parent-channel-id="1032">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1522" data-title="Selling Your Business" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1522#channel-1003-1032-1522" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Selling Your Business</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1522" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1577" data-title="Due Diligence" data-synonyms="audit; review">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1577#channel-1003-1032-1577" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Due Diligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1577" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1016" data-title="Organizational Development" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1016#channel-1003-1017-1016" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Organizational Development</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1016" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1208" data-title="Outsourcing and Offshoring" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1208#channel-1003-1017-1208" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Outsourcing and Offshoring</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1208" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1013" data-title="Strategic Planning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1013#channel-1003-1017-1013" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Strategic Planning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1013" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1304" data-title="Balanced Scorecard" data-synonyms="Kaplan and Norton; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1304#channel-1003-1017-1304" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Balanced Scorecard</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1304" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1128" data-title="Positioning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1128#channel-1003-1017-1128" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Positioning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1128" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1123" data-title="Growth Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1123#channel-1003-1017-1123" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Growth Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1123" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1119" data-title="Lessons from the Military" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1119#channel-1003-1017-1119" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Lessons from the Military</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1119" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1420" data-title="The Business Plan" data-synonyms="business proposal; business planning;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1420#channel-1003-1017-1420" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The Business Plan</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1420" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1657" data-title="VUCA" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1657#channel-1003-1017-1657" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">VUCA</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1657" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1009" data-title="Business Ethics" data-synonyms="value; honesty; probity; ethical; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1009#channel-1003-1003-1009" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Business Ethics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1009" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1003-1003-1009" data-parent-channel-id="1009">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1015" data-title="Corporate Social Responsibility" data-synonyms="CSR; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1015#channel-1003-1009-1015" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Social Responsibility</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1015" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1828" data-title="Disruption" data-synonyms="disruption; clayton christensen; jobs to be done theory; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1828#channel-1003-1003-1828" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Disruption</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1828" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1050" data-title="Career" data-synonyms="Professional Development">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1050#channel-1050--1050" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Career</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1050" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1050--1050" data-parent-channel-id="1050">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="89200" data-title="Career Change" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89200#channel-1050-1050-89200" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Career Change</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=89200" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1362" data-title="Personal Branding" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1362#channel-1050-1050-1362" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Personal Branding</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1362" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1532" data-title="The MBA" data-synonyms="master of business administration;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1532#channel-1050-1050-1532" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The MBA</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1532" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="88033" data-title="Career Strategies" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88033#channel-1050-1050-88033" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Career Strategies</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=88033" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1058" data-title="Job Search" data-synonyms="hunting; seeking; searching; employer; employment; unemployment; position; interview; hiring; hire; fire;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1058#channel-1050-1050-1058" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Job Search</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1058" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1429" data-title="Starting a New Job" data-synonyms="ninety days; 90 days; probabtion;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1429#channel-1050-1050-1429" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Starting a New Job</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1429" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="90745" data-title="Future of Work" data-synonyms="new work;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=90745#channel-1050-1050-90745" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Future of Work</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=90745" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="94151" data-title="Personal Development" data-synonyms="self-development; self; development; personal; growth; competency; soft; skill;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=94151#channel-94151--94151" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Personal Development</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=94151" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-94151--94151" data-parent-channel-id="94151">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1169" data-title="Assertiveness" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1169#channel-94151-94151-1169" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Assertiveness</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1169" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="89309" data-title="Authenticity" data-synonyms="raw">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89309#channel-94151-94151-89309" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Authenticity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=89309" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1391" data-title="Change Your Attitude" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1391#channel-94151-94151-1391" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Change Your Attitude</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1391" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1184" data-title="Charisma" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1184#channel-94151-94151-1184" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Charisma</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1184" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1191" data-title="Emotional Intelligence" data-synonyms="EI; emotional leadership; EL; emotional quotient; EQ; emotional intelligence quotient; EIQ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1191#channel-94151-94151-1191" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Emotional Intelligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1191" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1541" data-title="Habits" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1541#channel-94151-94151-1541" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Habits</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1541" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1696" data-title="Personal Accountability" data-synonyms="assuming responsibility; ownership; self-empowerment; taking action; motivation;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1696#channel-94151-94151-1696" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Personal Accountability</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1696" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1612" data-title="Resilience" data-synonyms="endurance; resistance; force; recovery; adjustment; robustness; flexibility; resiliency;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1612#channel-94151-94151-1612" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Resilience</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1612" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1664" data-title="Mindfulness" data-synonyms="awareness; attentiveness; advertency; consciousness; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1664#channel-94151-94151-1664" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mindfulness</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1664" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1055" data-title="Creativity" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1055#channel-94151-94151-1055" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Creativity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1055" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-94151-94151-1055" data-parent-channel-id="1055">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1458" data-title="Group Creativity" data-synonyms="brainstorming; idea;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1458#channel-94151-1055-1458" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Group Creativity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1458" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="90873" data-title="Workplace Skills" data-synonyms="professional development; professional; development; competency; soft; skill;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=90873#channel-90873--90873" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Workplace Skills</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=90873" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-90873--90873" data-parent-channel-id="90873">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1173" data-title="Getting Organized" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1173#channel-90873-90873-1173" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Getting Organized</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1173" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="92780" data-title="Goal Setting" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=92780#channel-90873-90873-92780" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Goal Setting</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=92780" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1541" data-title="Habits" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1541#channel-90873-90873-1541" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Habits</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1541" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1277" data-title="Holding Meetings" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1277#channel-90873-90873-1277" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Holding Meetings</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1277" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1142" data-title="Learning Techniques" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1142#channel-90873-90873-1142" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Learning Techniques</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1142" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1165" data-title="Office Politics" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1165#channel-90873-90873-1165" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Office Politics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1165" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1052" data-title="Speak, Write, Present" data-synonyms="presentations">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1052#channel-90873-90873-1052" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Speak, Write, Present</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1052" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-90873-90873-1052" data-parent-channel-id="1052">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1492" data-title="Business Writing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1492#channel-90873-1052-1492" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Business Writing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1492" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1175" data-title="Presenting" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1175#channel-90873-1052-1175" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Presenting</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1175" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1676" data-title="Public Speaking" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1676#channel-90873-1052-1676" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Public Speaking</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1676" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1475" data-title="Storytelling" data-synonyms="story; tale; narrative; fable;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1475#channel-90873-1052-1475" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Storytelling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1475" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1171" data-title="Become More Productive" data-synonyms="efficient; efficiency; work ethic; focus; engagement;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1171#channel-90873-90873-1171" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Become More Productive</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1171" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-90873-90873-1171" data-parent-channel-id="1171">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="89231" data-title="Focus" data-synonyms="concentrate">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89231#channel-90873-1171-89231" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Focus</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=89231" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1167" data-title="Procrastination" data-synonyms="motivation;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1167#channel-90873-1171-1167" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Procrastination</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1167" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1054" data-title="Time Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1054#channel-90873-1171-1054" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Time Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1054" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1054" data-title="Time Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1054#channel-90873-90873-1054" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Time Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1054" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1217" data-title="Decision Making" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1217#channel-90873-90873-1217" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Decision Making</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1217" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-90873-90873-1217" data-parent-channel-id="1217">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1719" data-title="Game Theory" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1719#channel-90873-1217-1719" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Game Theory</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1719" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1599" data-title="Problem-Solving Techniques" data-synonyms="How to Solve Problems">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1599#channel-90873-90873-1599" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Problem-Solving Techniques</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1599" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1609" data-title="Reading" data-synonyms="speedreading">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1609#channel-90873-90873-1609" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Reading</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1609" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="104343" data-title="Social Skills" data-synonyms="social; competency; soft; skill; relation; relationship; interaction; communication; socialization; interpersonal;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=104343#channel-104343--104343" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Social Skills</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=104343" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-104343--104343" data-parent-channel-id="104343">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1297" data-title="Asking Questions " data-synonyms="Questioning">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1297#channel-104343-104343-1297" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Asking Questions </span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1297" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1339" data-title="Conflict Resolution" data-synonyms="conflict management;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1339#channel-104343-104343-1339" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Conflict Resolution</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1339" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1166" data-title="Dealing with Difficult People at Work" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1166#channel-104343-104343-1166" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Dealing with Difficult People at Work</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1166" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-104343-104343-1166" data-parent-channel-id="1166">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1336" data-title="Dealing with Difficult Bosses" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1336#channel-104343-1166-1336" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Dealing with Difficult Bosses</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1336" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1410" data-title="Effective Communication" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1410#channel-104343-104343-1410" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Effective Communication</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1410" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-104343-104343-1410" data-parent-channel-id="1410">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1621" data-title="Feedback" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1621#channel-104343-1410-1621" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Feedback</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1621" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1621" data-title="Feedback" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1621#channel-104343-104343-1621" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Feedback</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1621" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1087" data-title="Intercultural Competence" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1087#channel-104343-104343-1087" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Intercultural Competence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1087" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1248" data-title="Listening" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1248#channel-104343-104343-1248" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Listening</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1248" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1053" data-title="Negotiation" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1053#channel-104343-104343-1053" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Negotiation</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1053" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1161" data-title="Networking" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1161#channel-104343-104343-1161" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Networking</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1161" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1619" data-title="Nonverbal Communication" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1619#channel-104343-104343-1619" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Nonverbal Communication</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1619" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1540" data-title="Parenting" data-synonyms="child rearing; bringing up children; bringing up kids;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1540#channel-104343-104343-1540" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Parenting</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1540" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1298" data-title="Persuasion" data-synonyms="influence;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1298#channel-104343-104343-1298" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Persuasion</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1298" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1359" data-title="Small Talk" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1359#channel-104343-104343-1359" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Small Talk</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1359" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1143" data-title="Teamwork" data-synonyms="collaboration; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1143#channel-104343-104343-1143" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Teamwork</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1143" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1376" data-title="Trust" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1376#channel-104343-104343-1376" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Trust</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1376" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1063" data-title="Life Advice" data-synonyms="self-help; self-development; self; development; personal; growth; competency; soft; skill; coach; crisis; meaning of life; meaning;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1063#channel-1063--1063" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Life Advice</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1063" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1063--1063" data-parent-channel-id="1063">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="96245" data-title="Advice for Graduates" data-synonyms="graduation; motivation; inspiration; commencement speeches">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=96245#channel-1063-1063-96245" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Advice for Graduates</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=96245" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1391" data-title="Change Your Attitude" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1391#channel-1063-1063-1391" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Change Your Attitude</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1391" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1697" data-title="Happiness" data-synonyms="happiness research; happy; fulfilling; worth living; positive psychology">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1697#channel-1063-1063-1697" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Happiness</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1697" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="118955" data-title="Lessons from Sports" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=118955#channel-1063-1063-118955" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Lessons from Sports</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=118955" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="92888" data-title="Love &amp; Partnership" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=92888#channel-1063-1063-92888" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Love &amp; Partnership</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=92888" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1540" data-title="Parenting" data-synonyms="child rearing; bringing up children; bringing up kids;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1540#channel-1063-1063-1540" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Parenting</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1540" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1035" data-title="Personal Finances" data-synonyms="personal investing; money">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1035#channel-1063-1063-1035" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Personal Finances</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1035" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1057" data-title="Psychology" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1057#channel-1063-1063-1057" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Psychology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1057" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1063-1063-1057" data-parent-channel-id="1057">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1191" data-title="Emotional Intelligence" data-synonyms="EI; emotional leadership; EL; emotional quotient; EQ; emotional intelligence quotient; EIQ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1191#channel-1063-1057-1191" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Emotional Intelligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1191" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1612" data-title="Resilience" data-synonyms="endurance; resistance; force; recovery; adjustment; robustness; flexibility; resiliency;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1612#channel-1063-1057-1612" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Resilience</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1612" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1164" data-title="Purpose" data-synonyms="motivation">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1164#channel-1063-1063-1164" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Purpose</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1164" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1612" data-title="Resilience" data-synonyms="endurance; resistance; force; recovery; adjustment; robustness; flexibility; resiliency;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1612#channel-1063-1063-1612" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Resilience</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1612" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1196" data-title="Retirement" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1196#channel-1063-1063-1196" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Retirement</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1196" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1051" data-title="Health &amp; Fitness" data-synonyms="well-being">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1051#channel-1063-1063-1051" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Health &amp; Fitness</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1051" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1063-1063-1051" data-parent-channel-id="1051">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1634" data-title="Aging Well" data-synonyms="age;health;staying young; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1634#channel-1063-1051-1634" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Aging Well</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine.php?idChaine=1634" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1881" data-title="Sleep" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1881#channel-1063-1051-1881" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sleep</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1881" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="89919" data-title="Work-Life-Balance" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89919#channel-1063-1063-89919" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Work-Life-Balance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89919" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1059" data-title="Stress" data-synonyms="anxiety; pressure; fight-or-flight response;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1059#channel-1063-1063-1059" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Stress</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1059" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1063-1063-1059" data-parent-channel-id="1059">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1364" data-title="Burnout" data-synonyms="stress">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1364#channel-1063-1059-1364" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Burnout</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1364" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1664" data-title="Mindfulness" data-synonyms="awareness; attentiveness; advertency; consciousness; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1664#channel-1063-1063-1664" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mindfulness</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1664" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1217" data-title="Decision Making" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1217#channel-1063-1063-1217" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Decision Making</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1217" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1063-1063-1217" data-parent-channel-id="1217">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1719" data-title="Game Theory" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1719#channel-1063-1217-1719" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Game Theory</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1719" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>











<div class="chov__item chov__item--level-0 chov__item--childless" data-channel-id="112978" data-title="Coronavirus Pandemic" data-synonyms="COVID-19; SARS-CoV-2; severe acute respiratory syndrome coronavirus 2;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=112978#channel-112978--112978" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Coronavirus Pandemic</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=112978" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->











<div class="chov__item chov__item--level-0 " data-channel-id="1099" data-title="Science" data-synonyms="advancement; research; scientist; scientific; laboratory; lab">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1099#channel-1099--1099" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Science</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1099" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1099--1099" data-parent-channel-id="1099">
		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1874" data-title="Biology" data-synonyms="biologist; life; living; organism; evolution; animal; plant; human; natural science">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1874#channel-1099-1099-1874" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Biology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1874" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1874" data-parent-channel-id="1874">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1875" data-title="Genetics" data-synonyms="gene; crispr; dna; inherit; chromosome; heredity;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1875#channel-1099-1874-1875" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Genetics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1875" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1099-1874-1875" data-parent-channel-id="1875">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1876" data-title="Genetic Engineering" data-synonyms="genetic modification; genetic manipulation; gene editing; genetics; crispr; gene;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1876#channel-1099-1875-1876" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Genetic Engineering</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1876" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1895" data-title="History of Science" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1895#channel-1099-1099-1895" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">History of Science</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1895" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1899" data-title="Mathematics" data-synonyms="math; algebra; geometry; calculation; calculate; numbers;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1899#channel-1099-1099-1899" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mathematics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1899" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1883" data-title="Medicine" data-synonyms="physician; health; doctor; hospital; disease; illness; surgery; intervention; blood; accident; vaccination; emergency: medical; diagnose; diagnosis; pharma; pharmaceuticals; therapy; therapies;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1883#channel-1099-1099-1883" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Medicine</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1883" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1883" data-parent-channel-id="1883">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1885" data-title="Neurodegeneration" data-synonyms="Neuroinflammation; AD; Alzheimer; Parkinson;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1885#channel-1099-1883-1885" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Neurodegeneration</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1885" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1888" data-title="Oncology " data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1888#channel-1099-1883-1888" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Oncology </span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1888" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1887" data-title="Stem Cells" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1887#channel-1099-1883-1887" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Stem Cells</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1887" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1886" data-title="Mental Health" data-synonyms="mental; mental illness; psychiatry; psycho; psyche; obsessive; compulsive; bipolar; phobia; anxiety; panic; post-traumatic stress disorder.; PTSD; mood; mania; schizophrenia; depression; eating disorder; anorexia; binge eating; purging; bulimia; delusion;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1886#channel-1099-1883-1886" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mental Health</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1886" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1099-1883-1886" data-parent-channel-id="1886">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="93774" data-title="Depression" data-synonyms="mood disorder; mental disorder; low mood; major depressive disorder; MDD); low; mood; self-esteem; loss of interest; antidepressant;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=93774#channel-1099-1886-93774" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Depression</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=93774" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1364" data-title="Burnout" data-synonyms="stress">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1364#channel-1099-1886-1364" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Burnout</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1364" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1925" data-title="Vaccination" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1925#channel-1099-1883-1925" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Vaccination</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1925" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="89937" data-title="Antibiotics" data-synonyms="antibacterial; microbes; penicillin; microorganism; antiseptic; resistance; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89937#channel-1099-1883-89937" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Antibiotics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89937" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="113551" data-title="Epidemics" data-synonyms="pandemic; infectious; population; disease; outbreak; virus; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=113551#channel-1099-1883-113551" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Epidemics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=113551" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1877" data-title="Neuroscience" data-synonyms="brain; neuroscientist; nervous; CNS; PNS; cognition; memory; learning; circuitry; sleep; behavior; behaviour; lobes; amygdala; hippocampus; hemisphere; limbic; sense; neural; neural network; nerve; natural science;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1877#channel-1099-1099-1877" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Neuroscience</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1877" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1877" data-parent-channel-id="1877">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="92564" data-title="Cognitive Neuroscience" data-synonyms="cognition; cognitive science; behavioral neuroscience; affective">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=92564#channel-1099-1877-92564" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cognitive Neuroscience</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=92564" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1882" data-title="Learning &amp; Memory" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1882#channel-1099-1877-1882" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Learning &amp; Memory</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1882" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1099-1877-1882" data-parent-channel-id="1882">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1541" data-title="Habits" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1541#channel-1099-1882-1541" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Habits</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1541" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1881" data-title="Sleep" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1881#channel-1099-1877-1881" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sleep</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1881" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1897" data-title="Paleontology" data-synonyms="palaeontology; paleoecology; evolution; Holocene; fossils;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1897#channel-1099-1099-1897" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Paleontology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1897" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1355" data-title="Physics" data-synonyms="natural science; universe; Newton; laws; thermo; motion; matter; space; time; gravitation; laser;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1355#channel-1099-1099-1355" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Physics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1355" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1355" data-parent-channel-id="1355">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1922" data-title="Quantum Physics" data-synonyms="quantum mechanics;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1922#channel-1099-1355-1922" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Quantum Physics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1922" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1099-1355-1922" data-parent-channel-id="1922">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1923" data-title="Quantum Computing" data-synonyms="superposition; entanglement; quantum bits; qubits; Turing; universal quantum computer; Paul Benioff; Yuri Manin; Richard Feynman; David Deutsch;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1923#channel-1099-1922-1923" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Quantum Computing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1923" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1893" data-title="Agriculture" data-synonyms="farming; cultivation; breeding; food; monoculture; agricultural science; crops; world population">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1893#channel-1099-1099-1893" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Agriculture</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1893" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1894" data-title="Pharmacology" data-synonyms="pharma; pharmaceutical; pharmaceutical industry; substance; compound; drug; drug action; pharmacon; bioactive; medicinal; clinical; clinical trial; clinical development; phase I; phase 1; phase II; phase 2; phase III; phase 3; r&amp;d; research &amp; development;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1894#channel-1099-1099-1894" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Pharmacology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1894" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1894" data-parent-channel-id="1894">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="89937" data-title="Antibiotics" data-synonyms="antibacterial; microbes; penicillin; microorganism; antiseptic; resistance; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89937#channel-1099-1894-89937" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Antibiotics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89937" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1747" data-title="Artificial Intelligence" data-synonyms="AI; machine intelligence">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1747#channel-1099-1099-1747" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Artificial Intelligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1747" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1747" data-parent-channel-id="1747">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="111172" data-title="AI &amp; Ethics" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=111172#channel-1099-1747-111172" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">AI &amp; Ethics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=111172" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1896" data-title="AI in the Workplace" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1896#channel-1099-1747-1896" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">AI in the Workplace</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1896" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1911" data-title="Machine Learning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1911#channel-1099-1747-1911" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Machine Learning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1911" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1913" data-title="AI Applications" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1913#channel-1099-1747-1913" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">AI Applications</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1913" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1099-1747-1913" data-parent-channel-id="1913">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1799" data-title="Autonomous Cars" data-synonyms="self-driving car; driverless car; autonomous vehicles">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1799#channel-1099-1913-1799" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Autonomous Cars</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1799" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1200" data-title="Bioethics" data-synonyms="ethical concerns; ethical questions; ethics">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1200#channel-1099-1099-1200" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Bioethics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1200" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1101" data-title="Environment" data-synonyms="sustainability; global; organism; population">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1101#channel-1099-1099-1101" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Environment</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1101" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1101" data-parent-channel-id="1101">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1903" data-title="Natural Disasters" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1903#channel-1099-1101-1903" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Natural Disasters</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1903" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1189" data-title="Sustainability" data-synonyms="environment; social; governance; ESG; nature; protect;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1189#channel-1099-1101-1189" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sustainability</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1189" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1428" data-title="Climate Change" data-synonyms="global warming; global temperature; climate research; climate emergency; climate crisis; climate breakdown; global heating">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1428#channel-1099-1101-1428" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Climate Change</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1428" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1099-1101-1428" data-parent-channel-id="1428">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1906" data-title="Carbon Dioxide" data-synonyms="CO2; gas; atmosphere; emission; greenhouse; ozone; climate change; climate; global warming;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1906#channel-1099-1428-1906" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Carbon Dioxide</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1906" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1907" data-title="Nature Protection" data-synonyms="nature conservation; nature reserve; environmental protection; environmental movement; environmental impact; biodiversity; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1907#channel-1099-1101-1907" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Nature Protection</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1907" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1889" data-title="Science &amp; Society" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1889#channel-1099-1099-1889" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Science &amp; Society</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1889" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1099-1099-1889" data-parent-channel-id="1889">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="89899" data-title="Science Denial" data-synonyms="war on science; faith;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89899#channel-1099-1889-89899" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Science Denial</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89899" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1890" data-title="Scientific Publishing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1890#channel-1099-1889-1890" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Scientific Publishing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1890" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1901" data-title="Astronomy" data-synonyms="space; universe; mars; moon; astro; physics;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1901#channel-1099-1099-1901" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Astronomy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1901" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1898" data-title="Technology" data-synonyms="techno; technologies; tech; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1898#channel-1898--1898" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Technology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1898" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1898--1898" data-parent-channel-id="1898">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1704" data-title="Blockchain" data-synonyms="block chain">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1704#channel-1898-1898-1704" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Blockchain</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1704" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1916" data-title="Man-Machine-Interface" data-synonyms="user interface; human-machine interface; HMI; MMI; human-computer interface;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1916#channel-1898-1898-1916" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Man-Machine-Interface</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1916" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1914" data-title="Robots" data-synonyms="robot; robotics">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1914#channel-1898-1898-1914" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Robots</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1914" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1637" data-title="Big Data" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1637#channel-1898-1898-1637" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Big Data</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1637" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1747" data-title="Artificial Intelligence" data-synonyms="AI; machine intelligence">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1747#channel-1898-1898-1747" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Artificial Intelligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1747" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1898-1898-1747" data-parent-channel-id="1747">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="111172" data-title="AI &amp; Ethics" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=111172#channel-1898-1747-111172" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">AI &amp; Ethics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=111172" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1896" data-title="AI in the Workplace" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1896#channel-1898-1747-1896" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">AI in the Workplace</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1896" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1911" data-title="Machine Learning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1911#channel-1898-1747-1911" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Machine Learning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1911" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1913" data-title="AI Applications" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1913#channel-1898-1747-1913" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">AI Applications</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1913" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1898-1747-1913" data-parent-channel-id="1913">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1799" data-title="Autonomous Cars" data-synonyms="self-driving car; driverless car; autonomous vehicles">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1799#channel-1898-1913-1799" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Autonomous Cars</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1799" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1803" data-title="IT Security" data-synonyms="hack; cyber; attacks; information security; data protection; cyber risk; cyber crime; cybercrime; cybersecurity; computer security; information technology security; IT security; exploit; vulnerability; CVE; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1803#channel-1898-1898-1803" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">IT Security</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1803" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1880" data-title="Digitalization" data-synonyms="digitalisation">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1880#channel-1898-1898-1880" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Digitalization</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1880" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="2021" data-title="X Reality" data-synonyms="Virtual Reality; VR; Augmented Reality; AR; Mixed Reality; MR; Cross Reality; XR; cyborg; wearable">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=2021#channel-1898-1898-2021" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">X Reality</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=2021" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1179" data-title="Women in Business" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1179#channel-1179--1179" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Women in Business</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1179" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1179--1179" data-parent-channel-id="1179">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="87404" data-title="Women’s Careers" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=87404#channel-1179-1179-87404" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Women’s Careers</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=87404" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="87411" data-title="Women in Leadership" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=87411#channel-1179-1179-87411" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Women in Leadership</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=87411" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1005" data-title="Innovation" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1005#channel-1005--1005" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Innovation</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1005" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1005--1005" data-parent-channel-id="1005">
		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1019" data-title="Product Development" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1019#channel-1005-1005-1019" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Product Development</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1019" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1005-1005-1019" data-parent-channel-id="1019">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1455" data-title="Product Design" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1455#channel-1005-1019-1455" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Product Design</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1455" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1691" data-title="Design Thinking" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1691#channel-1005-1019-1691" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Design Thinking</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1691" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1374" data-title="Innovation Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1374#channel-1005-1005-1374" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Innovation Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1374" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1055" data-title="Creativity" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1055#channel-1005-1005-1055" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Creativity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1055" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1005-1005-1055" data-parent-channel-id="1055">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1458" data-title="Group Creativity" data-synonyms="brainstorming; idea;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1458#channel-1005-1055-1458" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Group Creativity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1458" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1026" data-title="Marketing" data-synonyms="pricing; planning; strategy; advertising">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1026#channel-1026--1026" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Marketing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1026" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1026--1026" data-parent-channel-id="1026">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1021" data-title="Branding" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1021#channel-1026-1026-1021" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Branding</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1021" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1245" data-title="Competitor Analysis" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1245#channel-1026-1026-1245" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Competitor Analysis</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1245" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1379" data-title="Consumer Behavior" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1379#channel-1026-1026-1379" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Consumer Behavior</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1379" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1024" data-title="Customer Relations" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1024#channel-1026-1026-1024" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Customer Relations</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1024" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1026-1026-1024" data-parent-channel-id="1024">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1310" data-title="Customer Experience" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1310#channel-1026-1024-1310" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Customer Experience</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1310" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1020" data-title="Customer Relationship Management" data-synonyms="CRM; Kundenbeziehungsmanagement; Kundenbeziehung; Kundenmanagement">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1020#channel-1026-1024-1020" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Customer Relationship Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1020" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1152" data-title="Customer Retention" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1152#channel-1026-1024-1152" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Customer Retention</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1152" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1845" data-title="Customer Service" data-synonyms="client service">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1845#channel-1026-1024-1845" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Customer Service</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1845" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1026-1024-1845" data-parent-channel-id="1845">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1316" data-title="Complaint Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1316#channel-1026-1845-1316" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Complaint Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1316" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1020" data-title="Customer Relationship Management" data-synonyms="CRM; Kundenbeziehungsmanagement; Kundenbeziehung; Kundenmanagement">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1020#channel-1026-1026-1020" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Customer Relationship Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1020" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1186" data-title="Direct Marketing" data-synonyms="Dialogue Marketing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1186#channel-1026-1026-1186" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Direct Marketing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1186" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1026-1026-1186" data-parent-channel-id="1186">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1131" data-title="Cold Calling" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1131#channel-1026-1186-1131" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cold Calling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1131" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1512" data-title="Gender Marketing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1512#channel-1026-1026-1512" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Gender Marketing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1512" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1548" data-title="Market Research" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1548#channel-1026-1026-1548" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Market Research</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1548" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1018" data-title="Marketing Strategy" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1018#channel-1026-1026-1018" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Marketing Strategy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1018" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1128" data-title="Positioning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1128#channel-1026-1026-1128" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Positioning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1128" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1296" data-title="Pricing Strategies" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1296#channel-1026-1026-1296" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Pricing Strategies</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1296" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1022" data-title="Advertising" data-synonyms="marketing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1022#channel-1026-1026-1022" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Advertising</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1022" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1026-1026-1022" data-parent-channel-id="1022">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1465" data-title="Advertising in China" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1465#channel-1026-1022-1465" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Advertising in China</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1465" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1844" data-title="Social Media Marketing" data-synonyms="twitter; facebook; instagram">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1844#channel-1026-1026-1844" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Social Media Marketing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1844" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1023" data-title="Sales" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1023#channel-1023--1023" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sales</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1023" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1023--1023" data-parent-channel-id="1023">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1488" data-title="B2B Selling" data-synonyms="negotiation;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1488#channel-1023-1023-1488" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">B2B Selling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1488" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1580" data-title="Bidding and Contracting" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1580#channel-1023-1023-1580" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Bidding and Contracting</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1580" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1133" data-title="Closing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1133#channel-1023-1023-1133" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Closing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1133" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1134" data-title="Complex Sale" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1134#channel-1023-1023-1134" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Complex Sale</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1134" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1129" data-title="Hard Selling" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1129#channel-1023-1023-1129" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Hard Selling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1129" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1130" data-title="Soft Selling" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1130#channel-1023-1023-1130" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Soft Selling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1130" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1131" data-title="Cold Calling" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1131#channel-1023-1023-1131" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cold Calling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1131" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1135" data-title="Sales Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1135#channel-1023-1023-1135" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sales Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1135" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1043" data-title="Human Resources" data-synonyms="HR">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1043#channel-1043--1043" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Human Resources</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1043" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1043--1043" data-parent-channel-id="1043">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1038" data-title="Employee Retention" data-synonyms="Employee Engagement">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1038#channel-1043-1043-1038" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Employee Retention</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1038" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1850" data-title="Employer Branding" data-synonyms="internal marketing; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1850#channel-1043-1043-1850" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Employer Branding</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1850" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1851" data-title="Healthy Workplace" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1851#channel-1043-1043-1851" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Healthy Workplace</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1851" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1253" data-title="How to Dismiss Employees" data-synonyms="Layoffs; firing people; dismissal; firing; redundancy; termination; furlough;sack; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1253#channel-1043-1043-1253" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">How to Dismiss Employees</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1253" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1214" data-title="Intellectual Capital" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1214#channel-1043-1043-1214" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Intellectual Capital</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1214" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1040" data-title="Knowledge Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1040#channel-1043-1043-1040" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Knowledge Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1040" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1039" data-title="Learning &amp; Development" data-synonyms="Corporate Learning; Talent; L&amp;D; Training;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1039#channel-1043-1043-1039" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Learning &amp; Development</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1039" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1043-1043-1039" data-parent-channel-id="1039">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1338" data-title="Coaching" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1338#channel-1043-1039-1338" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Coaching</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1338" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1043-1039-1338" data-parent-channel-id="1338">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1187" data-title="Executive Coaching" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1187#channel-1043-1338-1187" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Executive Coaching</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1187" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1206" data-title="E-Learning" data-synonyms="online learning; MOOCs; online classroom;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1206#channel-1043-1039-1206" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">E-Learning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1206" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1553" data-title="Informal Learning" data-synonyms="Self-directed learning; learning from experience">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1553#channel-1043-1039-1553" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Informal Learning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1553" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1205" data-title="Leadership Development" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1205#channel-1043-1039-1205" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Leadership Development</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1205" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1043-1039-1205" data-parent-channel-id="1205">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1187" data-title="Executive Coaching" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1187#channel-1043-1205-1187" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Executive Coaching</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1187" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1293" data-title="Mentoring" data-synonyms="mentorship; mentors; mentees;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1293#channel-1043-1039-1293" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mentoring</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1293" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1141" data-title="Organizational Learning" data-synonyms="Learning Organization">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1141#channel-1043-1039-1141" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Organizational Learning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1141" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1037" data-title="Recruitment" data-synonyms="Talent Management; Hiring">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1037#channel-1043-1043-1037" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Recruitment</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1037" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1043-1043-1037" data-parent-channel-id="1037">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1348" data-title="Conducting Job Interviews" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1348#channel-1043-1037-1348" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Conducting Job Interviews</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1348" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1858" data-title="Remuneration" data-synonyms="compensation; pay; salary">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1858#channel-1043-1043-1858" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Remuneration</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1858" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1140" data-title="Rewarding Employees" data-synonyms="bonus systems; rewards; incentives;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1140#channel-1043-1043-1140" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Rewarding Employees</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1140" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1215" data-title="Succession Planning" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1215#channel-1043-1043-1215" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Succession Planning</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1215" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1446" data-title="Talent Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1446#channel-1043-1043-1446" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Talent Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1446" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1041" data-title="Working Time Arrangements" data-synonyms="flex time">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1041#channel-1043-1043-1041" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Working Time Arrangements</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1041" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1607" data-title="Millennials" data-synonyms="Generation Y ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1607#channel-1043-1043-1607" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Millennials</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1607" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1148" data-title="Performance Reviews" data-synonyms="performance appraisals">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1148#channel-1043-1043-1148" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Performance Reviews</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1148" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1447" data-title="Generation X" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1447#channel-1043-1043-1447" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Generation X</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1447" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="88085" data-title="HR Strategies" data-synonyms="RRHH estratégicos">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88085#channel-1043-1043-88085" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">HR Strategies</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88085" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="87966" data-title="Workplace Bullying" data-synonyms="Mobbying">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=87966#channel-1043-1043-87966" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Workplace Bullying</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=87966" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1642" data-title="Diversity &amp; Inclusion" data-synonyms="inclusion; equality; diversity;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1642#channel-1043-1043-1642" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Diversity &amp; Inclusion</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1642" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1042" data-title="Corporate Culture" data-synonyms="value">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1042#channel-1043-1043-1042" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Culture</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1042" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1043-1043-1042" data-parent-channel-id="1042">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1154" data-title="Corporate Entrepreneurship" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1154#channel-1043-1042-1154" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Entrepreneurship</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1154" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1376" data-title="Trust" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1376#channel-1043-1042-1376" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Trust</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1376" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="90745" data-title="Future of Work" data-synonyms="new work;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=90745#channel-1043-1043-90745" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Future of Work</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=90745" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1073" data-title="Economics" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1073#channel-1073--1073" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1073" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1073--1073" data-parent-channel-id="1073">
		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1072" data-title="Economic History" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1072#channel-1073-1073-1072" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic History</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1072" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1073-1073-1072" data-parent-channel-id="1072">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1103" data-title="Company Portraits" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1103#channel-1073-1072-1103" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Company Portraits</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1103" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1073-1072-1103" data-parent-channel-id="1103">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1556" data-title="General Electric" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1556#channel-1073-1103-1556" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">General Electric</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1556" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1620" data-title="Amazon" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1620#channel-1073-1103-1620" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Amazon</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1620" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1527" data-title="Financial History" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1527#channel-1073-1072-1527" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Financial History</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1527" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1073-1072-1527" data-parent-channel-id="1527">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1137" data-title="Stock Market Crashes" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1137#channel-1073-1527-1137" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Stock Market Crashes</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1137" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1849" data-title="Economic Policy" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1849#channel-1073-1073-1849" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic Policy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1849" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1073-1073-1849" data-parent-channel-id="1849">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1252" data-title="Economic Growth" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1252#channel-1073-1849-1252" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic Growth</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1252" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1521" data-title="Tax Havens" data-synonyms="offshore financial centers; tax jurisdictions">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1521#channel-1073-1849-1521" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Tax Havens</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1521" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1705" data-title="Inequality" data-synonyms="income inequality; wealth inequality; income distribution">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1705#channel-1073-1849-1705" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Inequality</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1705" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1070" data-title="Economic Theory" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1070#channel-1073-1073-1070" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic Theory</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1070" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1073-1073-1070" data-parent-channel-id="1070">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1682" data-title="Behavioral Economics" data-synonyms="heuristics; game theory">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1682#channel-1073-1070-1682" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Behavioral Economics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1682" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1252" data-title="Economic Growth" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1252#channel-1073-1070-1252" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic Growth</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1252" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1705" data-title="Inequality" data-synonyms="income inequality; wealth inequality; income distribution">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1705#channel-1073-1070-1705" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Inequality</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1705" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1155" data-title="Emerging Markets" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1155#channel-1073-1073-1155" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Emerging Markets</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1155" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1027" data-title="Financial Markets" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1027#channel-1073-1073-1027" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Financial Markets</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1027" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1073-1073-1027" data-parent-channel-id="1027">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1453" data-title="Private Equity" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1453#channel-1073-1027-1453" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Private Equity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1453" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1073-1027-1453" data-parent-channel-id="1453">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1450" data-title="Venture Capital" data-synonyms="angel investing; equity investing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1450#channel-1073-1453-1450" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Venture Capital</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1450" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1263" data-title="Hedge Funds" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1263#channel-1073-1027-1263" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Hedge Funds</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1263" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1285" data-title="Securitization" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1285#channel-1073-1027-1285" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Securitization</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1285" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1384" data-title="Predicting Markets" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1384#channel-1073-1027-1384" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Predicting Markets</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1384" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1399" data-title="Bonds" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1399#channel-1073-1027-1399" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Bonds</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1399" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1443" data-title="Microfinance" data-synonyms="unbanked; underbanked; microlending">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1443#channel-1073-1027-1443" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Microfinance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1443" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1527" data-title="Financial History" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1527#channel-1073-1027-1527" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Financial History</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1527" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1073-1027-1527" data-parent-channel-id="1527">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1137" data-title="Stock Market Crashes" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1137#channel-1073-1527-1137" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Stock Market Crashes</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1137" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1629" data-title="Complementary Currencies" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1629#channel-1073-1027-1629" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Complementary Currencies</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1629" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1073-1027-1629" data-parent-channel-id="1629">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="103946" data-title="Cryptocurrencies" data-synonyms="bitcoin; libra; crypto assets; cryptocurrency; ethereum; filecoin; Augur;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=103946#channel-1073-1629-103946" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cryptocurrencies</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=103946" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="89921" data-title="Labor Market" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89921#channel-1073-1073-89921" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Labor Market</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89921" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1073-1073-89921" data-parent-channel-id="89921">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="88538" data-title="Gig Economy" data-synonyms="freelance; temporary; employment; hiring; hire;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88538#channel-1073-89921-88538" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Gig Economy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88538" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1863" data-title="Markets in Asia" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1863#channel-1073-1073-1863" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Markets in Asia</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1863" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1073-1073-1863" data-parent-channel-id="1863">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="90484" data-title="Markets in China" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=90484#channel-1073-1863-90484" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Markets in China</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=90484" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1870" data-title="Global Trade" data-synonyms="world trade; imports; exports; mercantilism">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1870#channel-1073-1073-1870" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Global Trade</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1870" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1068" data-title="Globalization" data-synonyms="World; International; Global">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1068#channel-1073-1073-1068" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Globalization</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1068" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="91756" data-title="Sharing Economy" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=91756#channel-1073-1073-91756" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sharing Economy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=91756" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1086" data-title="Industries" data-synonyms="sectors; manufacturing; production; fabrication">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1086#channel-1086--1086" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Industries</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1086" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1086--1086" data-parent-channel-id="1086">
		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1074" data-title="Financial Industry" data-synonyms="banks; banking; hedge funds; funds">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1074#channel-1086-1086-1074" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Financial Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1074" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1074" data-parent-channel-id="1074">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1138" data-title="Investment Banking" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1138#channel-1086-1074-1138" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Investment Banking</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1138" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1263" data-title="Hedge Funds" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1263#channel-1086-1074-1263" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Hedge Funds</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1263" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1443" data-title="Microfinance" data-synonyms="unbanked; underbanked; microlending">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1443#channel-1086-1074-1443" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Microfinance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1443" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1453" data-title="Private Equity" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1453#channel-1086-1074-1453" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Private Equity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1453" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1086-1074-1453" data-parent-channel-id="1453">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1450" data-title="Venture Capital" data-synonyms="angel investing; equity investing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1450#channel-1086-1453-1450" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Venture Capital</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1450" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1408" data-title="Logistics Industry" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1408#channel-1086-1086-1408" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Logistics Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1408" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1872" data-title="Commodities Trading" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1872#channel-1086-1086-1872" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Commodities Trading</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1872" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1893" data-title="Agriculture" data-synonyms="farming; cultivation; breeding; food; monoculture; agricultural science; crops; world population">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1893#channel-1086-1086-1893" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Agriculture</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1893" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="94203" data-title="Insurance Industry" data-synonyms="reinsurance; risk">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=94203#channel-1086-1086-94203" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Insurance Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=94203" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="94434" data-title="Metals and Minerals" data-synonyms="Extraction; Mining">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=94434#channel-1086-1086-94434" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Metals and Minerals</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=94434" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="97464" data-title="Telecommunications" data-synonyms="mobile; phone; 2G; 3G; 4G; GSM; UMTS; LTE; LTE Advanced Pro; cellular; mobile; network;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=97464#channel-1086-1086-97464" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Telecommunications</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=97464" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-97464" data-parent-channel-id="97464">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="112979" data-title="5G" data-synonyms="5G NR; telecommunications; mobile; cell; phone;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=112979#channel-1086-97464-112979" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">5G</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=112979" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="125553" data-title="Aviation Industry" data-synonyms="airline; aerospace">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=125553#channel-1086-1086-125553" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Aviation Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=125553" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1075" data-title="Technology Industry" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1075#channel-1086-1086-1075" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Technology Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1075" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1075" data-parent-channel-id="1075">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1477" data-title="Google" data-synonyms="search engine; browser; Alphabet; android; YouTube; Chrome;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1477#channel-1086-1075-1477" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Google</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1477" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1556" data-title="General Electric" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1556#channel-1086-1075-1556" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">General Electric</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1556" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1076" data-title="Consumer Goods Industry" data-synonyms="FMCG">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1076#channel-1086-1086-1076" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Consumer Goods Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1076" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1076" data-parent-channel-id="1076">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1396" data-title="Retailing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1396#channel-1086-1076-1396" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Retailing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1396" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1086-1076-1396" data-parent-channel-id="1396">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1620" data-title="Amazon" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1620#channel-1086-1396-1620" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Amazon</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1620" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1620" data-title="Amazon" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1620#channel-1086-1076-1620" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Amazon</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1620" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1077" data-title="Consulting Industry" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1077#channel-1086-1086-1077" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Consulting Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1077" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1078" data-title="Automotive Industry" data-synonyms="Car Industry; Car Manufacture; Vehicle Manufacture">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1078#channel-1086-1086-1078" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Automotive Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1078" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1078" data-parent-channel-id="1078">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1799" data-title="Autonomous Cars" data-synonyms="self-driving car; driverless car; autonomous vehicles">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1799#channel-1086-1078-1799" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Autonomous Cars</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1799" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="97463" data-title="Electric Cars" data-synonyms="electric vehicle; EV; electric motor; electric drive; FCV; fuel cell electric vehicle; FCEV">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=97463#channel-1086-1078-97463" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Electric Cars</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=97463" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1086-1078-97463" data-parent-channel-id="97463">
		
			









<div class="chov__item chov__item--level-3 " data-channel-id="1084" data-title="Energy" data-synonyms="electricity; gas; oil; wind; solar; battery; thermodynamics; conservation; joule; kinetic; potential; elastic; chemical; radiant; thermal;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1084#channel-1086-97463-1084" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Energy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1084" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-4 collapse" id="channel-1086-97463-1084" data-parent-channel-id="1084">
		
			









<div class="chov__item chov__item--level-4 chov__item--childless" data-channel-id="88089" data-title="Oil &amp; Gas" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88089#channel-1086-1084-88089" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Oil &amp; Gas</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88089" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1079" data-title="Nonprofit Sector" data-synonyms="philanthropy; charity; charitable; non-profit">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1079#channel-1086-1086-1079" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Nonprofit Sector</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1079" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1080" data-title="Public Sector" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1080#channel-1086-1086-1080" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Public Sector</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1080" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1080" data-parent-channel-id="1080">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="92804" data-title="Urban Development " data-synonyms="Cities">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=92804#channel-1086-1080-92804" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Urban Development </span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=92804" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1081" data-title="Education" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1081#channel-1086-1086-1081" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Education</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1081" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1082" data-title="Health Industry" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1082#channel-1086-1086-1082" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Health Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1082" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1082" data-parent-channel-id="1082">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1894" data-title="Pharmacology" data-synonyms="pharma; pharmaceutical; pharmaceutical industry; substance; compound; drug; drug action; pharmacon; bioactive; medicinal; clinical; clinical trial; clinical development; phase I; phase 1; phase II; phase 2; phase III; phase 3; r&amp;d; research &amp; development;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1894#channel-1086-1082-1894" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Pharmacology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1894" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1086-1082-1894" data-parent-channel-id="1894">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="89937" data-title="Antibiotics" data-synonyms="antibacterial; microbes; penicillin; microorganism; antiseptic; resistance; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89937#channel-1086-1894-89937" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Antibiotics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89937" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1083" data-title="Media Industry" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1083#channel-1086-1086-1083" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Media Industry</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1083" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1084" data-title="Energy" data-synonyms="electricity; gas; oil; wind; solar; battery; thermodynamics; conservation; joule; kinetic; potential; elastic; chemical; radiant; thermal;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1084#channel-1086-1086-1084" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Energy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1084" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1086-1086-1084" data-parent-channel-id="1084">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="88089" data-title="Oil &amp; Gas" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88089#channel-1086-1084-88089" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Oil &amp; Gas</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88089" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1047" data-title="Manufacturing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1047#channel-1047--1047" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Manufacturing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1047" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1047--1047" data-parent-channel-id="1047">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1826" data-title="Quality Management" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1826#channel-1047-1047-1826" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Quality Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1826" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1046" data-title="Supply Chain Management" data-synonyms="logistics; shipping">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1046#channel-1047-1047-1046" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Supply Chain Management</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1046" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1859" data-title="Corporate Communication" data-synonyms="management communications; marketing communications; organisational communications; investor relations">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1859#channel-1859--1859" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Communication</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1859" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1859--1859" data-parent-channel-id="1859">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1025" data-title="Public Relations" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1025#channel-1859-1859-1025" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Public Relations</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1025" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1033" data-title="Investor Relations" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1033#channel-1859-1859-1033" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Investor Relations</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1033" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1315" data-title="Business Reputation" data-synonyms="good name; bad name; character; notoriety; image; persona; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1315#channel-1859-1859-1315" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Business Reputation</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1315" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1412" data-title="Corporate Blogging" data-synonyms="blogging; corporate communication; writing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1412#channel-1859-1859-1412" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Blogging</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1412" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1686" data-title="Lobbying" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1686#channel-1859-1859-1686" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Lobbying</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1686" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1860" data-title="Internal Communication" data-synonyms="employee communications; employee engagement; employee relations; internal marketing; company communications; staff communication">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1860#channel-1859-1859-1860" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Internal Communication</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1860" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1048" data-title="Corporate IT" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1048#channel-1048--1048" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate IT</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1048" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1048--1048" data-parent-channel-id="1048">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1803" data-title="IT Security" data-synonyms="hack; cyber; attacks; information security; data protection; cyber risk; cyber crime; cybercrime; cybersecurity; computer security; information technology security; IT security; exploit; vulnerability; CVE; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1803#channel-1048-1048-1803" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">IT Security</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1803" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="124387" data-title="Cloud Computing" data-synonyms="SaaS; PaaS; IaaS">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=124387#channel-1048-1048-124387" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cloud Computing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=124387" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1036" data-title="Corporate Finance" data-synonyms="funding; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1036#channel-1036--1036" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corporate Finance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1036" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1036--1036" data-parent-channel-id="1036">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1847" data-title="Budgeting" data-synonyms="cost allocations; capital allocation; working capital; cash flow">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1847#channel-1036-1036-1847" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Budgeting</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1847" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1271" data-title="Business Valuation" data-synonyms="appraisal">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1271#channel-1036-1036-1271" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Business Valuation</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1271" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1031" data-title="Compliance" data-synonyms="regulation; rules ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1031#channel-1036-1036-1031" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Compliance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1031" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1036-1036-1031" data-parent-channel-id="1031">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1344" data-title="Fraud" data-synonyms="scam; graft; corporate corruption">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1344#channel-1036-1031-1344" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Fraud</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1344" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1521" data-title="Tax Havens" data-synonyms="offshore financial centers; tax jurisdictions">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1521#channel-1036-1031-1521" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Tax Havens</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1521" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1150" data-title="Cutting Costs" data-synonyms="expense management; downsizing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1150#channel-1036-1036-1150" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cutting Costs</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1150" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1028" data-title="Financing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1028#channel-1036-1036-1028" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Financing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1028" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1036-1036-1028" data-parent-channel-id="1028">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1453" data-title="Private Equity" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1453#channel-1036-1028-1453" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Private Equity</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1453" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1036-1028-1453" data-parent-channel-id="1453">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1450" data-title="Venture Capital" data-synonyms="angel investing; equity investing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1450#channel-1036-1453-1450" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Venture Capital</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1450" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1218" data-title="Initial Public Offering" data-synonyms="IPO; direct listing; SPAC">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1218#channel-1036-1028-1218" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Initial Public Offering</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1218" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1036-1028-1218" data-parent-channel-id="1218">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1577" data-title="Due Diligence" data-synonyms="audit; review">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1577#channel-1036-1218-1577" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Due Diligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1577" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1450" data-title="Venture Capital" data-synonyms="angel investing; equity investing">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1450#channel-1036-1028-1450" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Venture Capital</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1450" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1032" data-title="Mergers &amp; Acquisitions" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1032#channel-1036-1036-1032" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Mergers &amp; Acquisitions</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1032" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1036-1036-1032" data-parent-channel-id="1032">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1522" data-title="Selling Your Business" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1522#channel-1036-1032-1522" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Selling Your Business</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1522" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1577" data-title="Due Diligence" data-synonyms="audit; review">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1577#channel-1036-1032-1577" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Due Diligence</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1577" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1033" data-title="Investor Relations" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1033#channel-1036-1036-1033" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Investor Relations</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1033" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1029" data-title="Controlling" data-synonyms="treasury; financial control; comptrolling; audit">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1029#channel-1036-1036-1029" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Controlling</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1029" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1036-1036-1029" data-parent-channel-id="1029">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1344" data-title="Fraud" data-synonyms="scam; graft; corporate corruption">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1344#channel-1036-1029-1344" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Fraud</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1344" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1848" data-title="Taxes" data-synonyms="levy; fiscal policy">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1848#channel-1036-1036-1848" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Taxes</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1848" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1036-1036-1848" data-parent-channel-id="1848">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1521" data-title="Tax Havens" data-synonyms="offshore financial centers; tax jurisdictions">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1521#channel-1036-1848-1521" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Tax Havens</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1521" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1098" data-title="Society" data-synonyms="Humanity; Mankind">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1098#channel-1098--1098" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Society</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1098" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1098--1098" data-parent-channel-id="1098">
		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1889" data-title="Science &amp; Society" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1889#channel-1098-1098-1889" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Science &amp; Society</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1889" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1098-1098-1889" data-parent-channel-id="1889">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="89899" data-title="Science Denial" data-synonyms="war on science; faith;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=89899#channel-1098-1889-89899" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Science Denial</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=89899" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1890" data-title="Scientific Publishing" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1890#channel-1098-1889-1890" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Scientific Publishing</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1890" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="85774" data-title="Media Manipulation" data-synonyms="Fake News; Trolling; Propanda; Desinformation; Wahlbeeinflussung">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=85774#channel-1098-1098-85774" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Media Manipulation</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=85774" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="87406" data-title="Gender Equality" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=87406#channel-1098-1098-87406" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Gender Equality</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=87406" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1098-1098-87406" data-parent-channel-id="87406">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="87969" data-title="Gender Pay Gap" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=87969#channel-1098-87406-87969" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Gender Pay Gap</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=87969" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="87420" data-title="Famous Women" data-synonyms="biographies; biography;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=87420#channel-1098-1098-87420" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Famous Women</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=87420" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="88020" data-title="Women’s Economic Empowerment" data-synonyms="women&#39;s rights; feminism">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88020#channel-1098-1098-88020" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Women’s Economic Empowerment</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88020" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="88538" data-title="Gig Economy" data-synonyms="freelance; temporary; employment; hiring; hire;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88538#channel-1098-1098-88538" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Gig Economy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88538" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="90745" data-title="Future of Work" data-synonyms="new work;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=90745#channel-1098-1098-90745" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Future of Work</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=90745" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1880" data-title="Digitalization" data-synonyms="digitalisation">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1880#channel-1098-1098-1880" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Digitalization</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1880" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1787" data-title="Universal Basic Income" data-synonyms="UBI; Basic living stipend; BLS; universal demogrant; basic income guarantee; Unconditional Basic Income">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1787#channel-1098-1098-1787" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Universal Basic Income</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1787" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1640" data-title="Surveillance" data-synonyms="Privacy">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1640#channel-1098-1098-1640" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Surveillance</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1640" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1200" data-title="Bioethics" data-synonyms="ethical concerns; ethical questions; ethics">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1200#channel-1098-1098-1200" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Bioethics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1200" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1227" data-title="Privacy" data-synonyms="data; protection; information;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1227#channel-1098-1098-1227" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Privacy</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1227" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1438" data-title="Sexual Harassment" data-synonyms="#metoo; victim; abuse; mistreatment;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1438#channel-1098-1098-1438" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sexual Harassment</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1438" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1447" data-title="Generation X" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1447#channel-1098-1098-1447" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Generation X</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1447" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1456" data-title="Social Media" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1456#channel-1098-1098-1456" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Social Media</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1456" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1544" data-title="Internet" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1544#channel-1098-1098-1544" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Internet</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1544" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1607" data-title="Millennials" data-synonyms="Generation Y ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1607#channel-1098-1098-1607" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Millennials</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1607" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="117247" data-title="Discrimination" data-synonyms="sexism; racism; ageism; shaming; prejudice; bias; biases; ableism; minorities; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=117247#channel-1098-1098-117247" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Discrimination</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=117247" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1071" data-title="Politics" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1071#channel-1071--1071" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1071" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1071--1071" data-parent-channel-id="1071">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1068" data-title="Globalization" data-synonyms="World; International; Global">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1068#channel-1071-1071-1068" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Globalization</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1068" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1775" data-title="Domestic Politics of China" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1775#channel-1071-1071-1775" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Domestic Politics of China</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1775" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1775" data-parent-channel-id="1775">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1781" data-title="The South China Sea Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1781#channel-1071-1775-1781" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The South China Sea Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1781" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1778" data-title="Democratic Recession" data-synonyms="Illiberalism; Authoritarianism; Populism; Polarization">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1778#channel-1071-1071-1778" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Democratic Recession</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1778" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1790" data-title="Global Security" data-synonyms="War; Terrorism; Defense">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1790#channel-1071-1071-1790" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Global Security</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1790" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1790" data-parent-channel-id="1790">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1783" data-title="Cyberwarfare" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1783#channel-1071-1790-1783" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Cyberwarfare</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1783" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1791" data-title="Sharp Power" data-synonyms="Propaganda; Media Manipulation; Information Warfare; Gerasimov Doctrine; Psychological Warfare; Fake News">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1791#channel-1071-1790-1791" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Sharp Power</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1791" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1792" data-title="Political Figures" data-synonyms="biographies; biography;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1792#channel-1071-1071-1792" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Political Figures</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1792" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1793" data-title="Politics of Pakistan" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1793#channel-1071-1071-1793" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Pakistan</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1793" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1811" data-title="Global Risks" data-synonyms="Threats; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1811#channel-1071-1071-1811" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Global Risks</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1811" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1811" data-parent-channel-id="1811">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1428" data-title="Climate Change" data-synonyms="global warming; global temperature; climate research; climate emergency; climate crisis; climate breakdown; global heating">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1428#channel-1071-1811-1428" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Climate Change</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1428" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1071-1811-1428" data-parent-channel-id="1428">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1906" data-title="Carbon Dioxide" data-synonyms="CO2; gas; atmosphere; emission; greenhouse; ozone; climate change; climate; global warming;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1906#channel-1071-1428-1906" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Carbon Dioxide</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1906" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1069" data-title="Terrorism" data-synonyms="Jihad">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1069#channel-1071-1811-1069" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Terrorism</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1069" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1573" data-title="Corruption" data-synonyms="Nepotism; Embezzlement; State Capture; Bad Governance">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1573#channel-1071-1811-1573" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Corruption</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1573" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1776" data-title="The North Korean Nuclear Threat" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1776#channel-1071-1811-1776" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The North Korean Nuclear Threat</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1776" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1778" data-title="Democratic Recession" data-synonyms="Illiberalism; Authoritarianism; Populism; Polarization">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1778#channel-1071-1811-1778" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Democratic Recession</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1778" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1781" data-title="The South China Sea Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1781#channel-1071-1811-1781" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The South China Sea Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1781" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1782" data-title="Migration" data-synonyms="Emigration; Immigration; Refugees; Asylum; Flight">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1782#channel-1071-1811-1782" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Migration</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1782" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1785" data-title="Global Health Threats" data-synonyms="Epidemics; Epidemic; Pandemic; Pandemics; Superbug">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1785#channel-1071-1811-1785" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Global Health Threats</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1785" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1071-1811-1785" data-parent-channel-id="1785">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="113551" data-title="Epidemics" data-synonyms="pandemic; infectious; population; disease; outbreak; virus; ">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=113551#channel-1071-1785-113551" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Epidemics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=113551" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="86468" data-title="Foreign Politics of China" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=86468#channel-1071-1071-86468" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Foreign Politics of China</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=86468" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-86468" data-parent-channel-id="86468">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1781" data-title="The South China Sea Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1781#channel-1071-86468-1781" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The South China Sea Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1781" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1774" data-title="Foreign Politics of the USA" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1774#channel-1071-1071-1774" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Foreign Politics of the USA</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1774" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1774" data-parent-channel-id="1774">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1781" data-title="The South China Sea Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1781#channel-1071-1774-1781" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The South China Sea Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1781" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1773" data-title="Domestic Politics of the USA" data-synonyms="United States; America">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1773#channel-1071-1071-1773" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Domestic Politics of the USA</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1773" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1773" data-parent-channel-id="1773">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1777" data-title="Gun Control in the USA" data-synonyms="Second Amendment; Gun Violence; Mass shootings">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1777#channel-1071-1773-1777" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Gun Control in the USA</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1777" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1771" data-title="Politics of Europe" data-synonyms="EU; European Union">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1771#channel-1071-1071-1771" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Europe</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1771" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1771" data-parent-channel-id="1771">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1769" data-title="The Ukrainian Crisis" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1769#channel-1071-1771-1769" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The Ukrainian Crisis</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1769" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1772" data-title="Brexit" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1772#channel-1071-1771-1772" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Brexit</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1772" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1759" data-title="Political Theory" data-synonyms="Politology; Political Science; Political Philosophy">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1759#channel-1071-1071-1759" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Political Theory</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1759" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1760" data-title="Political Psychology" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1760#channel-1071-1071-1760" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Political Psychology</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1760" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1761" data-title="Politics of Africa" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1761#channel-1071-1071-1761" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Africa</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1761" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1762" data-title="Global Politics" data-synonyms="Geopolitics">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1762#channel-1071-1071-1762" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Global Politics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1762" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1762" data-parent-channel-id="1762">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1781" data-title="The South China Sea Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1781#channel-1071-1762-1781" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The South China Sea Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1781" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1763" data-title="Politics of Latin America" data-synonyms="South America">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1763#channel-1071-1071-1763" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Latin America</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1763" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1764" data-title="Politics of the Middle East" data-synonyms="Near East; Arab Countries; Levante">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1764#channel-1071-1071-1764" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of the Middle East</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1764" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1764" data-parent-channel-id="1764">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1765" data-title="The Syria Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1765#channel-1071-1764-1765" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The Syria Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1765" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1766" data-title="Politics of Iran" data-synonyms="Persia">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1766#channel-1071-1764-1766" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Iran</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1766" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1780" data-title="Politics of Turkey" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1780#channel-1071-1764-1780" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Turkey</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1780" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1784" data-title="The Yemen Conflict" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1784#channel-1071-1764-1784" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The Yemen Conflict</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1784" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1767" data-title="Politics of Russia" data-synonyms="CIS">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1767#channel-1071-1071-1767" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of Russia</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1767" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1071-1071-1767" data-parent-channel-id="1767">
		
			









<div class="chov__item chov__item--level-2 chov__item--childless" data-channel-id="1769" data-title="The Ukrainian Crisis" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1769#channel-1071-1767-1769" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">The Ukrainian Crisis</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1769" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1770" data-title="Politics of India" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1770#channel-1071-1071-1770" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Politics of India</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1770" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1100" data-title="History" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1100#channel-1100--1100" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">History</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1100" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1100--1100" data-parent-channel-id="1100">
		
			









<div class="chov__item chov__item--level-1 " data-channel-id="1072" data-title="Economic History" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1072#channel-1100-1100-1072" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Economic History</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1072" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-2 collapse" id="channel-1100-1100-1072" data-parent-channel-id="1072">
		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1103" data-title="Company Portraits" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1103#channel-1100-1072-1103" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Company Portraits</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1103" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1100-1072-1103" data-parent-channel-id="1103">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1556" data-title="General Electric" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1556#channel-1100-1103-1556" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">General Electric</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1556" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1620" data-title="Amazon" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1620#channel-1100-1103-1620" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Amazon</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1620" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
			









<div class="chov__item chov__item--level-2 " data-channel-id="1527" data-title="Financial History" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1527#channel-1100-1072-1527" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Financial History</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1527" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-3 collapse" id="channel-1100-1072-1527" data-parent-channel-id="1527">
		
			









<div class="chov__item chov__item--level-3 chov__item--childless" data-channel-id="1137" data-title="Stock Market Crashes" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1137#channel-1100-1527-1137" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Stock Market Crashes</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1137" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


		
	</div>


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1105" data-title="Historical Figures" data-synonyms="biographies; biography;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1105#channel-1100-1100-1105" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Historical Figures</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1105" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="1895" data-title="History of Science" data-synonyms="">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1895#channel-1100-1100-1895" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">History of Science</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1895" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>











<div class="chov__item chov__item--level-0 " data-channel-id="1117" data-title="Classics" data-synonyms="fiction; novels; drama; prose; liberal arts; classics; story; antiquity; greek; roman; philosophy; religion;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=1117#channel-1117--1117" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Classics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=1117" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->

	
	<div class="chov__item-group chov__item-group--level-1 collapse" id="channel-1117--1117" data-parent-channel-id="1117">
		
			









<div class="chov__item chov__item--level-1 chov__item--childless" data-channel-id="88711" data-title="Literary Classics" data-synonyms="fiction; novels; drama; prose; liberal arts; classics; story; literature;">
	<!-- Title -->
	<a href="en-model_contenu_de_chaine.php?idChaine=88711#channel-1117-1117-88711" class="list-group-item-arrow collapsed" data-toggle="collapse">
		<div class="list-group-item-arrow-ico">
			<i class="ico-arrow-down"></i>
		</div>
		<span class="chov__item-title">Literary Classics</span>
	</a>
	<div class="chov__item-actions">
		<!-- Channel Link -->
		<a href="en-model_contenu_de_chaine?idChaine=88711" class="btn btn-sm btn-primary btn-outline">
			View
		</a>
		<!-- Follow -->
		
	</div>
</div>
<!-- Children -->


		
	</div>


				</div>
				<div class="chov__search-noresult hidden">
					   
				</div>
			</div>
		</div>
	</div>
</div><!--- END MAIN BODY -->
<!--- END WRAPPER -->
	

	










<?php include("./en-footer.php");?>
<!--
<script src="../fr-explore_files/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js"></script>
<script src="../fr-explore_files/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js"></script>-->
<script src="../fr-explore_files/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js" defer></script>
<script src="../fr-explore_files/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js" defer></script>
<script src="../fr-explore_files/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js" defer></script>

<script src="../fr-explore_files/plugins-cb-263spjwuo2ql4ws8p9pvlrp95thejfn.js" defer></script>
<script src="../fr-explore_files/main-cb-jelr67bnj55qdbl7da6fg4blydtxduv.js" defer></script>
<script src="../fr-explore_files/channel-overview-cb-q3d8sbe9owr731vgseah33wmdif4wga.js" defer></script>
		
<script>
	var hy = document.createElement('script'); hy.type = 'text/javascript'; hy.setAttribute("async", "async");
	hy.src = "/www/js/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js";
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hy, s);
</script>

<script>
 	
 	$(function(){
 		$.gaNotificationInit("/en/notification/check?jsp=%2fWEB-INF%2fviews%2fabstracts%2fexplore.jsp", "/en/notification/dismiss/{notificationId}");
 	});
</script>

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-S2VR" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-S2VR');
	</script>
	<!-- End Google Tag Manager -->
	
	
	<script>
		var google_conversion_id = 1043811595;
		var google_conversion_label = "dIVwCMG31wEQi5rd8QM";
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
	</script>
	<script type="text/javascript" src="../menu_explore_files/f(4).txt"></script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1043811595/?value=0&amp;label=dIVwCMG31wEQi5rd8QM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
	
	<script>
		var google_conversion_id = 1044773340;
		var google_conversion_label = "0ls-CJzzyAEQ3POX8gM";
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
	</script>
	<script type="text/javascript" src="../menu_explore_files/f(4).txt"></script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1044773340/?value=0&amp;label=0ls-CJzzyAEQ3POX8gM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
	
	
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam-cell.nr-data.net","licenseKey":"996526df3c","agent":"","beacon":"bam-cell.nr-data.net","applicationTime":7188,"applicationID":"3375187","transactionName":"ZlwEbUVTCENRAhVQWV8WNUlFWwhXcw4PTUReVQpcRR0DSEANDktTHhlOfnJmTw==","queueTime":0}</script>
<div id="stats" data-ga-analytics="" data-ga-analytics-vt-attributes="{}" data-ga-analytics-meta="">
	<noscript>
		
		<img src="/gastat.gif?pv=1&url=https%3a%2f%2fbackend.iferu.com%2fen%2fexplore&iframe=&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
	</noscript>
</div><div class="js-ga-dialog dialog" id="prospect-cta-dialog">  <div class="dialog__dialog" style="max-width: 600px; height: auto;">    <div class="dialog__titlebar">      <div class="dialog__title"></div>      <a href="javascript:;" class="btn btn-link js-dialog-close dialog__close">        <i class="ico-close" aria-hidden="true"></i>        <span class="sr-only">Close</span>      </a>    </div>    <div class="dialog__body"><div class="js-prospect-dialog">
		<div class="prospect-dialog">
			<h2>How would you like to use iferu?</h2>
			<div class="prospect-dialog__selectors">
				<span class="prospect-dialog__selector">
					<input type="radio" id="customerTypeB2C" name="ctaProspectType" checked="" data-ga-toggler-name="ctaProspectType" data-ga-toggler-value="b2c" class="active">
		    		<label for="customerTypeB2C">For myself</label> 
				</span>
				<span class="prospect-dialog__selector">
					<input type="radio" id="customerTypeB2B" name="ctaProspectType" data-ga-toggler-name="ctaProspectType" data-ga-toggler-value="b2b">
		    		<label for="customerTypeB2B">For my company</label> 
				</span>
			</div>
			<hr>
			<div data-ga-toggler-target="ctaProspectType b2c">
				<p><strong>Enjoy 3 days of full online access to 20,000+ summaries</strong>
</p><p>
Risk-free: no credit card is required. After three days, your trial will expire automatically.</p><p></p>
				




<form action="http://backend.iferu.com/en/trial" class="trial-cta trial-cta--wrap " data-ga-trial-form="">
	<input type="hidden" name="ajax" value="true">
	<div class="trial-cta__winnie">
   		
		<label for="name">Your Email Address*</label>
		<input type="text" name="name" id="name">
	</div>
    
    <div class="trial-cta__container">
    	<div class="trial-cta__email">
			<input name="email" class="trial-cta__email-input form-control" placeholder="Your email" data-ga-validator-required="true" value="">
		</div>
		<div>
			<button type="submit" class="btn btn-warning">
				Try it for free
			</button>
		</div>
	</div>
</form>


    

			</div>
			<div data-ga-toggler-target="ctaProspectType b2b" style="display: none;">
				<p>iferu's corporate solution enables companies to foster learning cultures by providing tools that allow users to learn as they work. We offer a range of possibilities to help your organization meet its specific learning objectives.</p>
				
				<a href="http://backend.iferu.com/en/enterprise/request-demo" class="btn btn-warning propect-dialog__corpsol-btn">Request a Demo</a>
			</div>		
		</div>
	</div></div></div></div><script type="text/javascript" id="" src="../menu_explore_files/100323.js"></script><script type="text/javascript" id="">(function(){function t(){++g;50==g&&(k=!0);l()}function u(){m=!0;l()}function l(){k&&m&&!n&&(n=!0,dataLayer.push({event:"nobounce",scrollCount:g}))}var d=(new Date).getTime(),b=0,p=0,c=!0,h=!1,m=!1,k=!1,n=!1,g=0;setTimeout(u,3E4);var q=function(){p=(new Date).getTime();b+=p-d;c=!0},e=function(b){c&&(c=!1,d=(new Date).getTime(),h=!1);window.clearTimeout(r);r=window.setTimeout(q,5E3)},a=function(b,a){window.addEventListener?window.addEventListener(b,a):window.attachEvent&&window.attachEvent("on"+b,
a)},f=function(a){c||(b+=(new Date).getTime()-d);!h&&0<b&&36E5>b&&window.dataLayer.push({event:"nonIdle",nonIdleTimeElapsed:b});c&&(h=!0);a&&"beforeunload"===a.type&&window.removeEventListener("beforeunload",f);b=0;d=(new Date).getTime();window.setTimeout(f,15E3)};a("mousedown",e);a("keydown",e);a("mousemove",e);a("beforeunload",f);a("scroll",e);a("scroll",t);var r=window.setTimeout(q,5E3);window.setTimeout(f,15E3)})();</script><script type="text/javascript" id="">!function(d,e){var b="0059a9de7316e9b74d58489d3ccdef8d87";if(d.obApi){var c=function(a){return"[object Array]"===Object.prototype.toString.call(a)?a:[a]};d.obApi.marketerId=c(d.obApi.marketerId).concat(c(b))}else{var a=d.obApi=function(){a.dispatch?a.dispatch.apply(a,arguments):a.queue.push(arguments)};a.version="1.1";a.loaded=!0;a.marketerId=b;a.queue=[];b=e.createElement("script");b.async=!0;b.src="//amplify.outbrain.com/cp/obtp.js";b.type="text/javascript";c=e.getElementsByTagName("script")[0];
c.parentNode.insertBefore(b,c)}}(window,document);obApi("track","PAGE_VIEW");</script><script type="text/javascript" id="">window._tfa=window._tfa||[];window._tfa.push({notify:"event",name:"page_view"});!function(a,b,d,c){document.getElementById(c)||(a.async=1,a.src=d,a.id=c,b.parentNode.insertBefore(a,b))}(document.createElement("script"),document.getElementsByTagName("script")[0],"//cdn.taboola.com/libtrc/unip/1154722/tfa.js","tb_tfa_script");</script>
<noscript>
  <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","832786183443261");fbq("set","agent","tmgoogletagmanager","832786183443261");fbq("track","Page View");</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="hs-script-loader" src="../menu_explore_files/4918719(2).js"></script><script type="text/javascript" id="">!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version="1.1",a.queue=[],b=e.createElement(f),b.async=!0,b.src="//static.ads-twitter.com/uwt.js",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,"script");twq("init","o0jtb");twq("track","PageView");</script><script type="text/javascript" id="">!function(b){var a=b.clearbit=b.clearbit||[];if(!a.initialize)if(a.invoked)b.console&&console.error&&console.error("Clearbit snippet included twice.");else{a.invoked=!0;a.methods="trackSubmit trackClick trackLink trackForm pageview identify reset group track ready alias page once off on".split(" ");a.factory=function(c){return function(){var d=Array.prototype.slice.call(arguments);d.unshift(c);a.push(d);return a}};for(b=0;b<a.methods.length;b++){var e=a.methods[b];a[e]=a.factory(e)}a.load=function(c){var d=
document.createElement("script");d.async=!0;d.src=("https:"===document.location.protocol?"https://":"http://")+"x.clearbitjs.com/v1/"+c+"/clearbit.min.js";c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(d,c)};a.SNIPPET_VERSION="3.1.0";a.load("pk_381e766f277206e0822dcd305318bae2");a.page()}}(window);</script><script type="text/javascript" id="">piAId="903971";piCId="6461";piHostname="pi.pardot.com";(function(){function b(){var a=document.createElement("script");a.type="text/javascript";a.src=("https:"==document.location.protocol?"https://pi":"http://cdn")+".pardot.com/pd.js";var c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(a,c)}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load",b,!1)})();</script><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon203629241498"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon244102909305" width="0" height="0" alt="" src="../menu_explore_files/0"></div>
<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
</body></html>
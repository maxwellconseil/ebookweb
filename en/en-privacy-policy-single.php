<?php   

 session_start();  
 ?> 

<!DOCTYPE html>
<!-- saved from url=(0043)https://www.Iferu.com/fr/terms-of-use -->
<html class="js no-touch" lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Privacy Policy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="fb:app_id" content="143891362343386">
<link rel="shortcut icon" href="https://www.Iferu.com/favicon.ico" type="image/x-icon">
<link rel="search" href="https://www.Iferu.com/opensearch.xml" type="application/opensearchdescription+xml" title="Iferu">

<script type="text/javascript" src="../include/996526df3c"></script><script type="text/javascript" src="../include/pd.js"></script><script src="../include/nr-1198.min.js"></script><script src="../include/leadflows.js" type="text/javascript" id="LeadFlows-4918719" crossorigin="anonymous" data-leadin-portal-id="4918719" data-leadin-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod" data-hsjs-hublet="na1"></script><script src="../include/4918719.js" type="text/javascript" id="hs-analytics"></script><script src="../include/feedbackweb-new.js" type="text/javascript" id="hs-feedback-web-4918719" crossorigin="anonymous" data-hubspot-feedback-portal-id="4918719" data-hubspot-feedback-env="prod" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod" data-hsjs-hublet="na1"></script><script src="../include/4918719(1).js" type="text/javascript" id="cookieBanner-4918719" data-cookieconsent="ignore" data-loader="hs-scriptloader" data-hsjs-portal="4918719" data-hsjs-env="prod" data-hsjs-hublet="na1"></script><script async="" src="../include/clearbit.min.js"></script><script async="" src="../include/uwt.js"></script><script async="" src="../include/fbevents.js"></script><script async="" src="../include/tfa.js" id="tb_tfa_script"></script><script async="" src="../include/obtp.js" type="text/javascript"></script><script type="text/javascript" async="" src="../include/bat.js"></script><script type="text/javascript" async="" src="../include/insight.min.js"></script><script type="text/javascript" async="" src="../include/analytics.js"></script><script type="text/javascript" async="" src="../include/dc.js"></script><script async="" src="../include/gtm.js"></script><script type="text/javascript" async="async" src="../include/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js"></script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={licenseKey:"996526df3c",applicationID:"3375187"};window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var i=t[n]={exports:{}};e[n][0].call(i.exports,function(t){var i=e[n][1][t];return r(i||t)},i,i.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var i=0;i<n.length;i++)r(n[i]);return r}({1:[function(e,t,n){function r(){}function i(e,t,n){return function(){return o(e,[u.now()].concat(c(arguments)),t?null:this,n),t?void 0:this}}var o=e("handle"),a=e(6),c=e(7),f=e("ee").get("tracer"),u=e("loader"),s=NREUM;"undefined"==typeof window.newrelic&&(newrelic=s);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(e,t){s[t]=i(p+t,!0,"api")}),s.addPageAction=i(p+"addPageAction",!0),s.setCurrentRouteName=i(p+"routeName",!0),t.exports=newrelic,s.interaction=function(){return(new r).get()};var m=r.prototype={createTracer:function(e,t){var n={},r=this,i="function"==typeof t;return o(l+"tracer",[u.now(),e,n],r),function(){if(f.emit((i?"":"no-")+"fn-start",[u.now(),r,i],n),i)try{return t.apply(this,arguments)}catch(e){throw f.emit("fn-err",[arguments,this,e],n),e}finally{f.emit("fn-end",[u.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(e,t){m[t]=i(l+t)}),newrelic.noticeError=function(e,t){"string"==typeof e&&(e=new Error(e)),o("err",[e,u.now(),!1,t])}},{}],2:[function(e,t,n){function r(){return c.exists&&performance.now?Math.round(performance.now()):(o=Math.max((new Date).getTime(),o))-a}function i(){return o}var o=(new Date).getTime(),a=o,c=e(8);t.exports=r,t.exports.offset=a,t.exports.getLastTimestamp=i},{}],3:[function(e,t,n){function r(e,t){var n=e.getEntries();n.forEach(function(e){"first-paint"===e.name?d("timing",["fp",Math.floor(e.startTime)]):"first-contentful-paint"===e.name&&d("timing",["fcp",Math.floor(e.startTime)])})}function i(e,t){var n=e.getEntries();n.length>0&&d("lcp",[n[n.length-1]])}function o(e){e.getEntries().forEach(function(e){e.hadRecentInput||d("cls",[e])})}function a(e){if(e instanceof m&&!g){var t=Math.round(e.timeStamp),n={type:e.type};t<=p.now()?n.fid=p.now()-t:t>p.offset&&t<=Date.now()?(t-=p.offset,n.fid=p.now()-t):t=p.now(),g=!0,d("timing",["fi",t,n])}}function c(e){d("pageHide",[p.now(),e])}if(!("init"in NREUM&&"page_view_timing"in NREUM.init&&"enabled"in NREUM.init.page_view_timing&&NREUM.init.page_view_timing.enabled===!1)){var f,u,s,d=e("handle"),p=e("loader"),l=e(5),m=NREUM.o.EV;if("PerformanceObserver"in window&&"function"==typeof window.PerformanceObserver){f=new PerformanceObserver(r);try{f.observe({entryTypes:["paint"]})}catch(v){}u=new PerformanceObserver(i);try{u.observe({entryTypes:["largest-contentful-paint"]})}catch(v){}s=new PerformanceObserver(o);try{s.observe({type:"layout-shift",buffered:!0})}catch(v){}}if("addEventListener"in document){var g=!1,w=["click","keydown","mousedown","pointerdown","touchstart"];w.forEach(function(e){document.addEventListener(e,a,!1)})}l(c)}},{}],4:[function(e,t,n){function r(e,t){if(!i)return!1;if(e!==i)return!1;if(!t)return!0;if(!o)return!1;for(var n=o.split("."),r=t.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var i=null,o=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var c=navigator.userAgent,f=c.match(a);f&&c.indexOf("Chrome")===-1&&c.indexOf("Chromium")===-1&&(i="Safari",o=f[1])}t.exports={agent:i,version:o,match:r}},{}],5:[function(e,t,n){function r(e){function t(){e(a&&document[a]?document[a]:document[i]?"hidden":"visible")}"addEventListener"in document&&o&&document.addEventListener(o,t,!1)}t.exports=r;var i,o,a;"undefined"!=typeof document.hidden?(i="hidden",o="visibilitychange",a="visibilityState"):"undefined"!=typeof document.msHidden?(i="msHidden",o="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(i="webkitHidden",o="webkitvisibilitychange",a="webkitVisibilityState")},{}],6:[function(e,t,n){function r(e,t){var n=[],r="",o=0;for(r in e)i.call(e,r)&&(n[o]=t(r,e[r]),o+=1);return n}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],7:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,i=n-t||0,o=Array(i<0?0:i);++r<i;)o[r]=e[t+r];return o}t.exports=r},{}],8:[function(e,t,n){t.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],ee:[function(e,t,n){function r(){}function i(e){function t(e){return e&&e instanceof r?e:e?u(e,f,a):a()}function n(n,r,i,o,a){if(a!==!1&&(a=!0),!l.aborted||o){e&&a&&e(n,r,i);for(var c=t(i),f=v(n),u=f.length,s=0;s<u;s++)f[s].apply(c,r);var p=d[h[n]];return p&&p.push([b,n,r,c]),c}}function o(e,t){y[e]=v(e).concat(t)}function m(e,t){var n=y[e];if(n)for(var r=0;r<n.length;r++)n[r]===t&&n.splice(r,1)}function v(e){return y[e]||[]}function g(e){return p[e]=p[e]||i(n)}function w(e,t){s(e,function(e,n){t=t||"feature",h[n]=t,t in d||(d[t]=[])})}var y={},h={},b={on:o,addEventListener:o,removeEventListener:m,emit:n,get:g,listeners:v,context:t,buffer:w,abort:c,aborted:!1};return b}function o(e){return u(e,f,a)}function a(){return new r}function c(){(d.api||d.feature)&&(l.aborted=!0,d=l.backlog={})}var f="nr@context",u=e("gos"),s=e(6),d={},p={},l=t.exports=i();t.exports.getOrSetContext=o,l.backlog=d},{}],gos:[function(e,t,n){function r(e,t,n){if(i.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(o){}return e[t]=r,r}var i=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){i.buffer([e],r),i.emit(e,t,n)}var i=e("ee").get("handle");t.exports=r,r.ee=i},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,o,function(){return i++})}var i=1,o="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!x++){var e=b.info=NREUM.info,t=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(e&&e.licenseKey&&e.applicationID&&t))return u.abort();f(y,function(t,n){e[t]||(e[t]=n)});var n=a();c("mark",["onload",n+b.offset],null,"api"),c("timing",["load",n]);var r=p.createElement("script");r.src="https://"+e.agent,t.parentNode.insertBefore(r,t)}}function i(){"complete"===p.readyState&&o()}function o(){c("mark",["domContent",a()+b.offset],null,"api")}var a=e(2),c=e("handle"),f=e(6),u=e("ee"),s=e(4),d=window,p=d.document,l="addEventListener",m="attachEvent",v=d.XMLHttpRequest,g=v&&v.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:v,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var w=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1198.min.js"},h=v&&g&&g[l]&&!/CriOS/.test(navigator.userAgent),b=t.exports={offset:a.getLastTimestamp(),now:a,origin:w,features:{},xhrWrappable:h,userAgent:s};e(1),e(3),p[l]?(p[l]("DOMContentLoaded",o,!1),d[l]("load",r,!1)):(p[m]("onreadystatechange",i),d[m]("onload",r)),c("mark",["firstbyte",a.getLastTimestamp()],null,"api");var x=0},{}],"wrap-function":[function(e,t,n){function r(e,t){function n(t,n,r,f,u){function nrWrapper(){var o,a,s,p;try{a=this,o=d(arguments),s="function"==typeof r?r(o,a):r||{}}catch(l){i([l,"",[o,a,f],s],e)}c(n+"start",[o,a,f],s,u);try{return p=t.apply(a,o)}catch(m){throw c(n+"err",[o,a,m],s,u),m}finally{c(n+"end",[o,a,p],s,u)}}return a(t)?t:(n||(n=""),nrWrapper[p]=t,o(t,nrWrapper,e),nrWrapper)}function r(e,t,r,i,o){r||(r="");var c,f,u,s="-"===r.charAt(0);for(u=0;u<t.length;u++)f=t[u],c=e[f],a(c)||(e[f]=n(c,s?f+r:r,i,f,o))}function c(n,r,o,a){if(!m||t){var c=m;m=!0;try{e.emit(n,r,o,t,a)}catch(f){i([f,n,r,o],e)}m=c}}return e||(e=s),n.inPlace=r,n.flag=p,n}function i(e,t){t||(t=s);try{t.emit("internal-error",e)}catch(n){}}function o(e,t,n){if(Object.defineProperty&&Object.keys)try{var r=Object.keys(e);return r.forEach(function(n){Object.defineProperty(t,n,{get:function(){return e[n]},set:function(t){return e[n]=t,t}})}),t}catch(o){i([o],n)}for(var a in e)l.call(e,a)&&(t[a]=e[a]);return t}function a(e){return!(e&&e instanceof Function&&e.apply&&!e[p])}function c(e,t){var n=t(e);return n[p]=e,o(e,n,s),n}function f(e,t,n){var r=e[t];e[t]=c(r,n)}function u(){for(var e=arguments.length,t=new Array(e),n=0;n<e;++n)t[n]=arguments[n];return t}var s=e("ee"),d=e(7),p="nr@original",l=Object.prototype.hasOwnProperty,m=!1;t.exports=r,t.exports.wrapFunction=c,t.exports.wrapInPlace=f,t.exports.argsToArray=u},{}]},{},["loader"]);</script>
  <link rel="stylesheet" type="text/css" href="../include/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css">

<link rel="stylesheet" type="text/css" href="../include/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css">

  <link rel="stylesheet" type="text/css" href="../include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
  <link rel="stylesheet" type="text/css" href="../fr-enterprise-solutions_files/styles-cb-32xz01rs480m3etnbbvy59fpsag2xav.css">

<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/AkkRg.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/AkkBd.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/AkkLi.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/line-icons.woff" type="font/woff">
<link rel="preload" as="font" crossorigin="anonymous" href="https://www.Iferu.com/www/font/line-icons.woff2" type="font/woff2">


	<link rel="stylesheet" type="text/css" href="../include/styles-cb-8hnwdoahd9ttvwdf8gzbp7geuw33slu.css">


<script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js'); if('ontouchstart' in document){H.className=H.className.replace('no-touch', '');}})(document.documentElement)</script>
	<script>
	window.VWO = window.VWO || [];
	window.VWO.push(['tag', 'Login', 'true', 'session']); 
	_vis_opt_check_segment = {'95': true };
	window.nopromo = 'true'; /* Used for VWO segmentation (price testing) */
	window.anonymous = 'true'; /* Used for VWO segmentation */
	</script>
	<script>
	var _vwo_code=(function(){
	var account_id=5083,
	settings_tolerance=2000,
	library_tolerance=2500,
	use_existing_jquery=false,
	// DO NOT EDIT BELOW THIS LINE
	f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
	</script><script src="../include/j.php" type="text/javascript"></script>


<!--[if IE ]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/document-register-element/1.4.1/document-register-element.js"></script>
<![endif]-->
<?php 
if(isset($_SESSION["username"])){
  echo "<input type='hidden' value='true' id='session'></input>";
}?>
<script src="../include/f.txt"></script><script src="../include/f(1).txt"></script><script src="../include/va-9d6ac57dbcbba3321dd904e6ee78b647.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include/track-9d6ac57dbcbba3321dd904e6ee78b647.js" crossorigin="anonymous" type="text/javascript"></script><script src="../include/opa-3bf1d20a05f5e943629318cc3d43e637.js" crossorigin="anonymous" type="text/javascript"></script><style type="text/css">
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }
  }
</style><script type="text/javascript" src="../include/analytics"></script><script type="text/javascript" src="../include/analytics(1)"></script></head>
<body class="" data-new-gr-c-s-check-loaded="14.997.0" data-gr-ext-installed=""><div id="hs-feedback-fetcher"><iframe frameborder="0" src="../fr-privacy-policy-single_files/feedback-web-fetcher.html"></iframe></div>

    <script>
      dataLayer = [{
        "PageView": "\x2Ffr\x2Fprivacy\x2Dpolicy\x2Fsingle"
        
      }];
    </script>
  
  <div class="sr-only"><a href="https://www.Iferu.com/en/privacy-policy/single#main-content" tabindex="-1"> Skip navigation</a></div>



  <div class="" id="main-content" role="main">
  <?php 
 include ("./header_eng.php")
?>
<!--- START MAIN_BODY -->






<div class="pageheader" style="background-image: url(/www/images/privacy-policy.jpg)">
  
  <div class="pageheader__container container">
    <div class="row">
    <div class="col-lg-offset-2 col-lg-8">
      
  <h1>Privacy Policy</h1>

    </div>
    </div>
  </div>
</div><div class="primary container">
  <div style="margin-top: 2rem;">
  <p>At iferu AG (<b>"we"</b> or <b>"us"</b>), we understand that your personal information is yours. All personal information collected about you (<b>"You"</b> or <b>"Your"</b>), is processed in compliance with the provisions under the Swiss Data Protection Act (<b>"DPA"</b>) and General Data Protection Regulation (<b>"GDPR"</b>), as amended from time to time (collectively "Regulation").</p>

<p>This Data Protection and Privacy Policy (<b>"Policy"</b>) explains how we use any personal information (also called “personal data” under the Regulation) we collect about You when You subscribe to and use our services as chosen in the Subscriber Agreement (<b>"Services"</b>) and when using this website (<b>"Website"</b>).</p>


<h2>How do we collect personal information about You?</h2>

<p>In the following context, we collect personal information about You:</p>

<p>When You first subscribe to the Services, You will provide various personal information (see below).</p>

<p>Thereupon, we set up and activate access to the iferu Library (<b>"Library"</b>) for You. The Library will be hosted and maintained on our servers in Switzerland. The Library consists of abstracts which shall remain available to You for the duration of the Subscriber Agreement. </p>

<p>Upon registration, You will be subscribed to the individual weekly abstract service. You will then receive via e-mail one weekly abstract according to your interest profile. The frequency of this service can be changed anytime in your personal account.</p>


<h2>What personal information do we collect about You?</h2>

<p>The information we collect about you relates to personal data which You choose to provide to us when subscribing to the Services. This entails: Your first- and last name, address, telephone number, e-mail address, your interests and language, invoice and/or credit card details.</p>

<p>When using the Services and the Website we will collect information on You and on Your use of the Services and the Website. This entails: IP-addresses (including country of your internet access), visits to the Library and the Website, reading and download history from the Library.</p>

<p>We also collect information when You voluntarily complete customer surveys, provide feedback and participate in competitions. Website usage information is collected using cookies (collectively together with the above information: the <b>"Information"</b>).</p>


<h2>Why do we process your Information and on what legal basis?</h2>

<p>We process your Information in order to perform our obligations under the Subscriber Agreement concluded with you, for user authentication, access control, enforcing regional copyright restrictions,  Website, app and notification personalization, delivery of push notifications or for the purpose of other legitimate interests, or in order to comply with a legal duty imposed on us in connection with the rendering of the Services.</p>


<h2>How will we use the Information about You?</h2>

<p>The Information is used to render the Services You subscribed to, to manage Your account, and, if You agree, to email You about other services we think may be of interest to You. We use the Information to personalize Your repeat visits to the Website and provide a better user experience and internal development purposes.</p>

<p>For the purposes of analytics, advertisement services, and payment processing, we may share some of Your Information with the following third-party providers:</p>

<ul>
<li>Datatrans which processes the online credit card payments</li>
<li>Bing Adcenter</li>
<li>DoubleClick Floodlight</li>
<li>Facebook Ads</li>
<li>Google Analytics</li>
<li>Leadforensics.com</li>
<li>LinkedIn Ads</li>
<li>Outbrain</li>
<li>VisualWebsiteOptimizer</li>
<li>Google Adwords</li>
<li>Visitortracklog.com</li>
<li>Taboola</li>
<li>Salesforce</li>
<li>Twitter</li>
<li>Hubspot</li>
<li>Clearbit</li>
<li>New Relic</li>
<li>Amazon QuickSight</li>
</ul>

<p>For the purpose of verifying student identity, we may share some of Your Information with the following third-party providers:</p>
<ul>
  <li>SheerId</li>
  <li>UNiDAYS</li>
</ul>

<p>For the purpose of displaying user avatars, we may share some of Your Information with Gravatar.</p>

<h2>Where is Your information stored?</h2>

<p>We are a Swiss company and all Your information is processed and stored on servers in Switzerland. Third-party providers may store and process Your Information in other countries. We have put in place data transfer agreements, such as the EU model clauses or the Swiss Transborder Data Flow Agreements with each of these providers.</p>


<h2>Transfer of Information to a country outside the EEA</h2>

<p>We may transfer Information to countries outside the EEA, such as Mexico, Philippines and the USA. The transfer of Information to such countries will only take place if data transfer agreements (such as the EU model clauses or the Swiss Transborder Data Flow Agreement) are put into place.</p>


<h2>Data Security</h2>

<p>We have implemented technical and organizational measures to protect the Information against intentional or accidental manipulation, unauthorized third-party access, loss, destruction or changes. We continually improve this protection as technology changes.</p>


<h2>Marketing</h2>

<p>We may send You information about services of ours which may be of interest to You. You have a right to stop us from contacting You for marketing purposes and you may opt-out at any time.</p>


<h2>Your Rights</h2>

<p>You have the right to request access to and/or a copy of the Information that we hold about You. If You would like a copy of some or all of Your Information, please email or write to us at the following address privacy@iferu.com.</p>

<p>We want to make sure that the Information is accurate and up to date. You may ask us to correct or remove Information You think is inaccurate.</p>

<p>You may also ask us to cease processing your Information.</p>

<p>You may also ask for the Information to be transferred in a structured, commonly used and machine-readable format, provided the requirements under the Regulation are met. You may exercise your rights with regard to profiling (where applicable).</p>


<h2>Data Breach Notifications</h2>

<p>All data breaches shall be reported to privacy@iferu.com. If a personal data breach occurs and that breach is likely to result in a risk to Your rights and freedom, then we shall ensure that the competent data protection authorities are informed without delay.</p>


<h2>Withdrawal of Consent</h2>

<p>In the event a consent was given, You have the right to withdraw such consent given at any time by sending a written notice or e-mail to privacy@iferu.com.</p>


<h2>Cookies</h2>

<p>This Website uses cookies and similar technologies in order to distinguish you from other users. By using cookies, we are able to provide you with a better experience and to improve our website by better understanding how you use it.</p>

<p>For further information please consult our <a href="/cookie-policy">Cookie Policy</a>.</p>


<h2>Other Websites</h2>

<p>The Website contains links to other websites. This Policy only applies to the Services and this Website so when You visit other websites linked on our Website, You should read their own privacy policies.</p>


<h2>Changes to our Policy</h2>

<p>We keep our Policy under regular review and we will place any updates on this Website. This Policy was last updated in November 2019.</p>


<h2>How to contact us</h2>

<p>Please contact us if You have any questions about our Policy or the Information we hold about You: privacy@iferu.com or iferu AG | Alpenquai 12 | 6005 Lucerne | Switzerland.</p>

  </div>

</div><!-- End .primary -->
</div><!--- END MAIN BODY -->
<!--- END WRAPPER -->
  












<?php 
 include ("./en-footer.php")
?>


<div id="fb-root"></div>

<script src="../include_book/jquery-3.5.1.min-cb-ngqyxctuyyypyq778izlo0vuklgdyde.js"></script>

<script src="../include/jquery-migrate-3.3.0.min-cb-s0j5pkzmbvf6ke354jyrczap4jdggih.js"></script>
<script src="../include/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js"></script>
<script src="../include/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js"></script>
<script src="../include/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js"></script>

<script src="../include/plugins-cb-rqhn3ki7bf6j5lwmkcckeu85uqiuj47.js"></script>
<script src="../include/main-cb-tqm751dbrn8x06irjukcpdj53han94l.js"></script>
<script>
 $(document).ready(function(){  
  $('.navigation__btn').click(function(){
	$('.navigation__dropdown').toggle("show");
  });
  $('#alertdata').click(function(){ 
   
    var listdata = [];

    $(".chov__item-group--level-1").each(function(){

   var parent=$(this).attr('data-parent-channel-id');
     
   
      Array.from($(this).children()).forEach(function(el) {

  listdata.push([parent,$(el).attr('data-channel-id'),$(el).attr('data-title')]);
});



   
    });
  
    
    console.log(JSON.stringify(listdata));

  
  $('.notifybar__dismiss').click(function(){ 
   
    $('.notifybar').hide();
  });  

  var state= $('#session').val();
  if(state=='true'){
    $('#logout').show();
    $('#connexion').hide();

  }
  $('#logout').click(function(){  
       
        var action = "logout"; 
        
        $.ajax({  
             url:"pdo.php",  
             method:"POST",  
             data:{action:action},  
             success:function()  
             {  
               
                  location.reload();  
                  
             }  
        });  
   });  

   
})
  });
  </script>
<script>

  var hy = document.createElement('script'); hy.type = 'text/javascript'; hy.setAttribute("async", "async");
  hy.src = "/www/js/hyphenator-cb-fmbee94oz09s2pewy8xizejmnv3pnx3.js";
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hy, s);
</script>

<script>
  
  $(function(){
    $.gaNotificationInit("/fr/notification/check?jsp=%2fWEB-INF%2fviews%2fb2c%2fwelcome.jsp", "/fr/notification/dismiss/{notificationId}");
  });
</script>



  
<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam.nr-data.net","licenseKey":"996526df3c","agent":"","beacon":"bam.nr-data.net","applicationTime":55,"applicationID":"3375187","transactionName":"ZlwEbUVTCENRAhVQWV8WNUlFWwhXcw4PTUReVQpcRR0RVVwCDlRTEREhfGMb","queueTime":0}</script>
<div id="stats" data-ga-analytics="" data-ga-analytics-vt-attributes="{}" data-ga-analytics-meta="">
  <noscript>
    
    <img src="/gastat.gif?pv=1&url=https%3a%2f%2fbackend.iferu.com%2ffr%2f&iframe=&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
  </noscript>
</div><script type="text/javascript" id="" src="../include/100323.js"></script>
<script type="text/javascript" id="">(function(){function t(){++g;50==g&&(k=!0);l()}function u(){m=!0;l()}function l(){k&&m&&!n&&(n=!0,dataLayer.push({event:"nobounce",scrollCount:g}))}var d=(new Date).getTime(),b=0,p=0,c=!0,h=!1,m=!1,k=!1,n=!1,g=0;setTimeout(u,3E4);var q=function(){p=(new Date).getTime();b+=p-d;c=!0},e=function(b){c&&(c=!1,d=(new Date).getTime(),h=!1);window.clearTimeout(r);r=window.setTimeout(q,5E3)},a=function(b,a){window.addEventListener?window.addEventListener(b,a):window.attachEvent&&window.attachEvent("on"+b,
a)},f=function(a){c||(b+=(new Date).getTime()-d);!h&&0<b&&36E5>b&&window.dataLayer.push({event:"nonIdle",nonIdleTimeElapsed:b});c&&(h=!0);a&&"beforeunload"===a.type&&window.removeEventListener("beforeunload",f);b=0;d=(new Date).getTime();window.setTimeout(f,15E3)};a("mousedown",e);a("keydown",e);a("mousemove",e);a("beforeunload",f);a("scroll",e);a("scroll",t);var r=window.setTimeout(q,5E3);window.setTimeout(f,15E3)})();</script><script type="text/javascript" id="">!function(d,e){var b="0059a9de7316e9b74d58489d3ccdef8d87";if(d.obApi){var c=function(a){return"[object Array]"===Object.prototype.toString.call(a)?a:[a]};d.obApi.marketerId=c(d.obApi.marketerId).concat(c(b))}else{var a=d.obApi=function(){a.dispatch?a.dispatch.apply(a,arguments):a.queue.push(arguments)};a.version="1.1";a.loaded=!0;a.marketerId=b;a.queue=[];b=e.createElement("script");b.async=!0;b.src="//amplify.outbrain.com/cp/obtp.js";b.type="text/javascript";c=e.getElementsByTagName("script")[0];
c.parentNode.insertBefore(b,c)}}(window,document);obApi("track","PAGE_VIEW");</script><script type="text/javascript" id="">window._tfa=window._tfa||[];window._tfa.push({notify:"event",name:"page_view"});!function(a,b,d,c){document.getElementById(c)||(a.async=1,a.src=d,a.id=c,b.parentNode.insertBefore(a,b))}(document.createElement("script"),document.getElementsByTagName("script")[0],"//cdn.taboola.com/libtrc/unip/1154722/tfa.js","tb_tfa_script");</script>
<noscript>
  <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","832786183443261");fbq("set","agent","tmgoogletagmanager","832786183443261");fbq("track","Page View");</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<script type="text/javascript" id="hs-script-loader" src="../include/4918719(2).js"></script><script type="text/javascript" id="">!function(d,e,f,a,b,c){d.twq||(a=d.twq=function(){a.exe?a.exe.apply(a,arguments):a.queue.push(arguments)},a.version="1.1",a.queue=[],b=e.createElement(f),b.async=!0,b.src="//static.ads-twitter.com/uwt.js",c=e.getElementsByTagName(f)[0],c.parentNode.insertBefore(b,c))}(window,document,"script");twq("init","o0jtb");twq("track","PageView");</script><script type="text/javascript" id="">!function(b){var a=b.clearbit=b.clearbit||[];if(!a.initialize)if(a.invoked)b.console&&console.error&&console.error("Clearbit snippet included twice.");else{a.invoked=!0;a.methods="trackSubmit trackClick trackLink trackForm pageview identify reset group track ready alias page once off on".split(" ");a.factory=function(c){return function(){var d=Array.prototype.slice.call(arguments);d.unshift(c);a.push(d);return a}};for(b=0;b<a.methods.length;b++){var e=a.methods[b];a[e]=a.factory(e)}a.load=function(c){var d=
document.createElement("script");d.async=!0;d.src=("https:"===document.location.protocol?"https://":"http://")+"x.clearbitjs.com/v1/"+c+"/clearbit.min.js";c=document.getElementsByTagName("script")[0];c.parentNode.insertBefore(d,c)};a.SNIPPET_VERSION="3.1.0";a.load("pk_381e766f277206e0822dcd305318bae2");a.page()}}(window);</script><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon846907519620"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon160317095711" width="0" height="0" alt="" src="../include/0"></div><script src="../include/cachedClickId"></script>
<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
<script>window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script><script>window.__vidChanged = function(state){if(state === 1) {document.body.dispatchEvent(new Event('vidChanged'))}}
if(document.querySelector('#movie_player')){document.querySelector('#movie_player').addEventListener('onStateChange', '__vidChanged');}</script>


  

<script>$(".footer__menu").hide();</script>

</body></html>

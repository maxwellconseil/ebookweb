<div class="primary container">
		<div style="margin-top: 2rem;">
			
			<p>These Website terms of use ("Terms") set out the terms of use under which you may use this Website, <a href="$/">backend.iferu.com</a> (“Website”). Please read these Terms carefully and ensure that you understand them. Your agreement to comply with and be bound by these Terms is deemed to occur upon your first use of the Website. If you do not agree to comply with and be bound by these Terms, you must stop using the website.</p>
			<h2>Definitions</h2>
<p>In these Terms, unless the context otherwise requires, the following expressions have the following meanings:</p>
<table>
 <tbody>
  <tr>
   <td>
    <p>
     <strong>“Content”</strong>
    </p>
   </td>
   <td>
    <p>means any and all text, images, audio, video, scripts, code, software, databases and any other form of information capable of being stored on a computer that appears on or forms part of the Website; and</p>
   </td>
  </tr>
  <tr>
   <td>
    <p>
     <strong>“iferu/we/us”</strong>
    </p>
   </td>
   <td>
    <p>means iferu AG, a company registered in Switzerland whose address is Alpenquai 12, 6005 Lucerne</p>
   </td>
  </tr>
 </tbody>
</table>
<h2><br>Information About Us</h2>
<p>The Website, <a href="/">backend.iferu.com</a>, is owned and operated by iferu.</p>
<h2>Access to the Website</h2>
<p>Access to the Website is free of charge. It is your responsibility to make any and all arrangements necessary in order to access the Website.</p>
<p>Access to the Website is provided "as is" and on an "as available" basis. We may alter, suspend or discontinue the Website (or any part of it) at any time and without notice. We will not be liable to you in any way if the Website (or any part of it) is unavailable at any time and for any period.</p>
<h2>Intellectual Property Rights</h2>
<p>All Content included on the Website and the copyright, trademarks and other intellectual property rights subsisting in that Content, unless specifically labelled otherwise, belongs to or has been licensed by us. All Content is protected by applicable Swiss and international intellectual property laws and treaties.</p>
<p>Unless stipulated otherwise by the applicable mandatory law, you may not reproduce, copy, distribute, sell, rent, sub-license, store, or in any other manner re-use Content from the Website, unless given express written permission to do so by us.</p>
<p>You may not use any Content saved or downloaded from the Website for commercial purposes without first obtaining a license from us (or our licensors, as appropriate) to do so.</p>
<h2>Links to Other Websites</h2>
<p>Links to other sites may be included on the Website. Unless expressly stated these sites are not under the control of iferu. We neither assume nor accept responsibility or liability for the content of third party sites. The inclusion of a link to another site on the Website is for information only and does not imply any endorsement of the sites themselves or of those in control of them.</p>
<h2>Disclaimers</h2>
<p>Nothing on the Website constitutes advice on which you should rely. It is provided for general information purposes only.</p>
<p>To the extent permitted by applicable law, we make no representation, warranty, or guarantee that the Website will meet your requirements, that it will not infringe the rights of third parties, that it will be compatible with all software and hardware, or that it will be secure.</p>
<p>We make reasonable efforts to ensure that the Content on the Website is complete, accurate, and up-to-date. We do not, however, make any representations, warranties or guarantees (whether express or implied) that the Content is complete, accurate, or up-to-date.</p>
<h2>Liability</h2>
<p>To the extent permitted by applicable law, we accept no liability to any user for any loss or damage, whether foreseeable or otherwise, in contract, tort (including negligence), for breach of statutory duty, or otherwise, arising out of or in connection with the use of (or inability to use) the Website or the use of or reliance upon any Content included on the Website.</p>
<p>To the extent permitted by law, we exclude all representations, warranties, and guarantees (whether express or implied) that may apply to the Website or any Content included on the Website.</p>
<p>We exercise all reasonable skill and care to ensure that the Website is free from viruses and other malware. We accept no liability for any loss or damage resulting from a virus or other malware, a distributed denial of service attack, or other harmful material or event that may adversely affect your hardware, software, data or other material that occurs as a result of your use of the Website (including the downloading of any Content from it) or any other site referred to on the Website.</p>
<p>We neither assume nor accept responsibility or liability arising out of any disruption or non-availability of the Website resulting from external causes including, but not limited to, ISP equipment failure, host equipment failure, communications network failure, natural events, acts of war, or legal restrictions and censorship.</p>
<h2>Cookies</h2>
<p>Please refer to our <a href="/en/cookie-policy">Cookie Policy</a>.</p>
<h2>Data Protection</h2>
<p>Please refer to our <a href="/en/privacy-policy/single">Privacy Policy</a>.</p>
<h2>Changes to these Terms</h2>
<p>We may alter these Terms at any time. Any such changes will become binding on you upon your first use of the Website after the changes have been implemented. You are therefore advised to check this Website from time to time.</p>
<p>In the event of any conflict between the current version of these Terms and any previous version(s), the provisions current and in effect shall prevail unless it is expressly stated otherwise.</p>
<h2>Law and Jurisdiction</h2>
<p>These Terms shall be governed by and construed in accordance with Swiss law (to the exclusion of Swiss Private International Law and international treaties, in particular the Vienna Convention on the International Sale of Goods dated April 11, 1980).</p>
<p>Any disputes concerning these Terms shall be subject to the jurisdiction of the competent courts at the domicile of iferu.</p>
<h2>Contacting Us</h2>
<p>To contact us, please email us at <a href="mailto:info@iferu.com">info@iferu.com</a> or using any of the methods provided on the <a href="/en/contact">contact page</a>.</p>
		</div>
	
</div>
<?php
if($_GET['action']=="delete"){

    $table = fopen('website-log.csv','r');
    $temp_table = fopen('table_temp.csv','w');

    // print_r($sentdata['select']);
    // print_r($sentdata);
    $selection = $sentdata['select'] ;// the name of the column you're looking for
    $ip=$sentdata['ip'];
    while (($data = fgetcsv($table, 1000)) !== FALSE){
        if($data[1] == $selection && $data[0] == $ip ){ // this is if you need the first column in a row
            continue;
        }
        fputcsv($temp_table,$data);
    }
    fclose($table);
    fclose($temp_table);
    rename('table_temp.csv','website-log.csv');
}

function similar(string $a, string $b) {
    $equivalency = 0;
    $minLength = (strlen($a) > strlen($b)) ? strlen($b) : strlen($a);    
    $maxLength = (strlen($a)< strlen($b)) ? strlen($b) : strlen($a);    
    for($i = 0; $i < $minLength; $i++) {
        if($a[i] == $b[i]) {
            $equivalency++;
        }
    }
    $weight = $equivalency / $maxLength;
    return ($weight * 100);
}

if($_GET['action']=="remove"){
    $selection = trim($_GET['select']);
    $selection = preg_replace("/\s+/", "", $selection);
    $table = fopen('website-log.csv','r');
    $temp_table = fopen('table_temp.csv','w');
    $sel = '';
    // print_r($sentdata['select']);
    // print_r($sentdata);
    // $selection = $sentdata['select'] ;// the name of the column you're looking for
    // $ip=$sentdata['ip'];
    while (($data = fgetcsv($table, 1000)) !== FALSE){
        
        $sel = strip_tags(trim(str_replace('&nbsp;', '', $data[1])));
        $sel = preg_replace("/\s+/", "", $sel);

        if($sel != $selection){
            fputcsv($temp_table,$data);
        }
    }

    fclose($table);
    fclose($temp_table);
    rename('table_temp.csv','website-log.csv');
}

else{
    $data = $_GET['jsonString'];
    $data[1] = str_replace('&nbsp;', '', $data[1]);
    $file = fopen("website-log.csv","a");
    fputcsv($file, $data);
    fclose($file);
}

?>
<?php  

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Content-Type: application/json');
//database connection  
$conn = mysqli_connect('localhost', 'max', 'toor','books');  
if (! $conn) {  
die("Connection failed" . mysqli_connect_error());  
}  
else {  
mysqli_select_db($conn, 'pagination');  
}  
if (!isset ($_GET['page'])  ) {  
    $page = 1;  
   
} else {  
    $page = $_GET['page'];
  
    
}  
if ( isset($_GET['idlang'])){
    $idlang=$_GET['idlang'];
}
else{
    $idlang="%";
}

//define total number of results per page  
$results_per_page = 10;  
$idChaine=  $_GET['idChaine'];
//find the total number of results stored in the database  
$query = "SELECT D.*,C.nom_fr  FROM Document D,Chaine C  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and C.id= ".$idChaine." and D.idLangue like '".$idlang."'";  
$result = mysqli_query($conn, $query);  
$number_of_result = mysqli_num_rows($result);  

//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  

//determine which page number visitor is currently on  


//determine the sql LIMIT starting number for the results on the displaying page  
$page_first_result = ($page-1) * $results_per_page;  

//retrieve the selected results from database   
// $query = "SELECT *FROM Document  LIMIT " . $page_first_result . ',' . $results_per_page;  
$query = "SELECT D.*,C.nom_fr,Auteur.* FROM  Doc_Aut,Auteur,Document D,Chaine C  WHERE D.id IN (SELECT idDoc FROM Doc_Chaine where idChaine = ".$idChaine.") and C.id= ".$idChaine." and D.idLangue like 1 and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id  order by date desc LIMIT " . $page_first_result . ',' . $results_per_page;
$result = mysqli_query($conn, $query);  
$table = array();

//display the retrieved result on the webpage  
while ($row = $result->fetch_assoc()) {  
      array_push($table,$row);
}  

 echo json_encode($table);	

 

?>  
<?php 
 include ("./poc/pdo.php");
 session_start();
if (!isset($_SESSION['username'])){

  echo '<script>window.location.href="./fr-explore";</script>';
}
 
?>
<!DOCTYPE html>
<!-- saved from url=(0040)http://backend.iferu.com/fr/mylibrary -->
<html class="js no-touch" lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style class="vjs-styles-defaults">
      .video-js {
        width: 300px;
        height: 150px;
      }

      .vjs-fluid {
        padding-top: 56.25%
      }
    </style>

<title>iferu</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="INDEX,FOLLOW">
<meta property="fb:app_id" content="143891362343386">
<link rel="shortcut icon" href="http://backend.iferu.com/favicon.ico" type="image/x-icon">
<link rel="search" href="http://backend.iferu.com/opensearch.xml" type="application/opensearchdescription+xml" title="iferu">

<link rel="stylesheet" type="text/css" href="./en-mylibrary/mylibrary_files/jquery-ui-1.12.1.min-cb-fyyzrx92fbilucbwhqke3jqbggxtiyc.css">

<link rel="stylesheet" type="text/css" href="./en-mylibrary/mylibrary_files/bootstrap-cb-1eib2am82vfhch339lo2u8dfgdjhrhj.css">

  <link rel="stylesheet" type="text/css" href="./en-mylibrary/mylibrary_files/styles-cb-kuwt5eli32t8nf8oeb0fqwd71jdygwp.css">
  <link rel="stylesheet" type="text/css" href="./include/styles-cb-jsfynwbbuw3w7v3sz8277ce5y5pqc5k.css">
  <?php
  if (isset($_SESSION['username'])){
  $username = $_SESSION['username'];
  echo "<input type='hidden' value='true' id='session'></input>";}?>

  <style type="text/css">
  #hs-feedback-ui {
    animation-duration: 0.4s;
    animation-timing-function: ease-out;
    display: none;
    height: 0;
    overflow: hidden;
    position: fixed;
    z-index: 2147483647;
    max-width: 100%;
  }

  .hubspot.space-sword #hs-feedback-ui {
    z-index: 1070;
  }

  #hs-feedback-ui.hs-feedback-shown {
    display: block;
  }

  #hs-feedback-fetcher {
    position: fixed;
    left: 1000%;
  }

  
  @keyframes feedback-slide-in-hs-feedback-left {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-left {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-left {
    animation-name: feedback-slide-in-hs-feedback-left;
  }

  #hs-feedback-ui.hs-feedback-left.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-left;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-right {
    from {transform: translate(0, 100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-right {
    from {transform: translate(0, 0);}
    to {transform: translate(0, 100%);}
  }

  #hs-feedback-ui.hs-feedback-right {
    animation-name: feedback-slide-in-hs-feedback-right;
  }

  #hs-feedback-ui.hs-feedback-right.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-right;
    animation-fill-mode: forwards;
  }

  
  @keyframes feedback-slide-in-hs-feedback-top {
    from {transform: translate(0, -100%);}
    to {transform: translate(0, 0);}
  }

  @keyframes feedback-slide-out-hs-feedback-top {
    from {transform: translate(0, 0);}
    to {transform: translate(0, -100%);}
  }

  #hs-feedback-ui.hs-feedback-top {
    animation-name: feedback-slide-in-hs-feedback-top;
  }

  #hs-feedback-ui.hs-feedback-top.hs-feedback-slid-out {
    animation-name: feedback-slide-out-hs-feedback-top;
    animation-fill-mode: forwards;
  }


  #hs-feedback-ui > iframe {
    width: 100%;
    height: 100%;
  }

  #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 0;
  }

  #hs-feedback-ui.hs-feedback-left {
    left: 0;
  }

  #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  .zorse #hs-feedback-ui:not(.hs-feedback-top) {
    bottom: 6px;
  }

  .zorse #hs-feedback-ui.hs-feedback-right {
    right: 0;
  }

  #hs-feedback-ui.hs-feedback-top {
    left: 0;
    top: 0;
    width: 100%;
  }

  #hs-feedback-ui.hs-feedback-nps:not(.hs-feedback-top) {
    width: 480px;
  }

  #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top) {
    width: 350px;
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px;
  }

  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-csat:not(.hs-feedback-top):not(.hs-feedback-expanded) {
      width: 450px;
    }
  }

  #hs-feedback-ui.hs-feedback-csat.hs-feedback-extended-width:not(.hs-feedback-top) {
    width: 550px !important;
  }

  #hs-feedback-ui.preview.hs-feedback-csat.hs-feedback-callout:not(.hs-feedback-expanded):not(.hs-feedback-top) {
    width: 450px !important;
  }

  @media only screen and (max-width: 768px) {
    #hs-feedback-ui:not(.preview):not(.hs-feedback-callout):not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded:not(.preview):not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  @media only screen and (max-width: 600px) {
    #hs-feedback-ui.preview:not(.hs-feedback-top),
    #hs-feedback-ui.hs-feedback-expanded.preview:not(.hs-feedback-top) {
      width: 100% !important;
    }
  }

  #hs-feedback-ui.hs-feedback-shown ~ #tally-widget-container,
  #hs-feedback-ui.hs-feedback-shown ~ #wootric-modal {
    display: none !important;
  }

  /* hide all popups in the same position as us */
  @media only screen and (min-width: 544px) {
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown   ~ .leadinModal-theme-top {
      display: none !important;
    }
  }

  /* hide leadflows when we're tablet-stretched across from them */
  @media only screen and (min-width: 544px) and (max-width: 970px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }
  }

  /* hide messages when we're tablet-stretched across from them */
  @media only screen and (max-width: 966px) {
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown ~ #hubspot-messages-iframe-container {
      display: none !important;
    }
  }

  @media only screen and (max-width: 544px) {

    /* repeat above rules for small screens when we're set to display on mobile */
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ #hubspot-messages-iframe-container,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-left-corner,
    #hs-feedback-ui.hs-feedback-top.hs-feedback-shown:not(.hs-feedback-no-mobile)   ~ .leadinModal-theme-top,
    #hs-feedback-ui.hs-feedback-left.hs-feedback-shown:not(.hs-feedback-no-mobile)  ~ .leadinModal-theme-bottom-right-corner,
    #hs-feedback-ui.hs-feedback-right.hs-feedback-shown:not(.hs-feedback-no-mobile) ~ .leadinModal-theme-bottom-left-corner {
      display: none !important;
    }

    /* don't display us on small screens if we're set to not display on mobile */
    #hs-feedback-ui.hs-feedback-no-mobile {
      display: none;
    }
  }
  </style>
  <body class="" data-new-gr-c-s-check-loaded="14.993.0" data-gr-ext-installed=""><div id="hs-feedback-fetcher"><iframe frameborder="0" src="./en-mylibrary/mylibrary_files/feedback-web-fetcher.html"></iframe></div>

    <div class="sr-only"><a href="http://backend.iferu.com/fr/mylibrary#main-content" tabindex="-1">Ignorer la navigation</a></div>
  

  
<!--include header.php-->

   <?php include("header.php");?>

   <style>.header{position:relative;}</style>
  <div class="" id="main-content" role="main">


    <!--- START MAIN_BODY -->
  

  <div class="container">
    <div class="head-container page-header">
      <h1 style="margin-bottom: 0;">Les livres aimés</h1>
    </div> 

    <div class="slick-container" data-slick="{
  &quot;arrows&quot;: true,
  &quot;dots&quot;: true,
  &quot;slidesToShow&quot;: 7,
  &quot;slidesToScroll&quot;: 7,
  &quot;adaptiveHeight&quot;: true,
  &quot;infinite&quot;: false,
  &quot;responsive&quot;: [ 
    {
    &quot;breakpoint&quot;: 1200,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 5,
      &quot;slidesToScroll&quot;: 5
      }
    },{
    &quot;breakpoint&quot;: 720,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 4,
      &quot;slidesToScroll&quot;: 4
      }
    },{
    &quot;breakpoint&quot;: 615,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 3,
      &quot;slidesToScroll&quot;: 3
      }
    },{
    &quot;breakpoint&quot;: 470,
    &quot;settings&quot;: {
      &quot;arrows&quot;: true,
      &quot;slidesToShow&quot;: 2,
      &quot;slidesToScroll&quot;: 2
      }
    }
  ]}">

<?php
$index=1;
$request_read_later="select * from user_like where user_name='".$username."';";
$res_rrl = $pdo->query($request_read_later);
while($result = $res_rrl->fetch(PDO::FETCH_ASSOC)){

   $arr = $result['idDoc'];

$user_read_later = $pdo->query("SELECT id,titre,image,idRating FROM Document WHERE id ='$arr'");
    $r = $user_read_later->fetch(PDO::FETCH_ASSOC);

    $book_title=$r["titre"];
    $book_id=$r["id"];
    $book_image=$r["image"];
    $book_rating=$r["idRating"];

    $res_book = explode(" ", strtolower($book_title));

    $res_book_book=$res_book[0];
    for($j=1; $j<=count($res_book)-1; $j++) {
      $res_book_book=$res_book_book."-".$res_book[$j];
      }
      $book_rating = $pdo->query("SELECT number FROM Rating WHERE id ='$book_rating'");
    $r2 = $book_rating->fetch(PDO::FETCH_ASSOC);
echo'<div class="slick-item slick-slide slick-active" data-slick-index="'.$index.'" aria-hidden="false" style="width: 163px;height:300px;" tabindex="-1" role="option" aria-describedby="slick-slide0'.$index.'">
      <div class="ga-summary-box ">
  <div class="gsb-cover">
   <a href="./fr/resume/'.$res_book_book.'/'.$book_id.'?st=LIST" tabindex="0">
<img src="https://'.$book_image.'" alt="'.$book_title.'" class="scover scover--book scover--s lazyloaded">
  </a>
</div>
  <div class="ga-summary-box-title">'.$book_title.'</div>
  <div class="ga-summary-box-icons">
    <span class="gsb-rating" title="Classement"><i class="ico-star-empty"></i> <span class="gsb-rating-number">'.$r2["number"].'</span></span>
    </div>
</div>
    </div>';
    $index++;
}
?>

      

  </div>  </div>

    
  
</div>
<?php include("footer.php")?>

<script src="./fr-explore_files/jquery-ui-1.12.1.min-cb-e1tewvf2mf31edp1mwyb8chlknvua6y.js"></script>
<script src="./fr-explore_files/slick.min-cb-rbyoskvgkr6j29jq6d01oiswyzxvm5z.js"></script>
<script src="./fr-explore_files/bootstrap-cb-qsbius9cxqxqgyzo703r3s033z2p9qx.js"></script>

<script src="./fr-explore_files/plugins-cb-263spjwuo2ql4ws8p9pvlrp95thejfn.js"></script>
<script src="./fr-explore_files/main-cb-jelr67bnj55qdbl7da6fg4blydtxduv.js"></script>

<noscript>
    <div style="display:inline;">
      <img height="1" width="1" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1044773340/?value=0&amp;label=0ls-CJzzyAEQ3POX8gM&amp;guid=ON&amp;script=0"/>
    </div>
  </noscript>
  <div id="stats" data-ga-analytics="" data-ga-analytics-vt-attributes="{}" data-ga-analytics-meta="">
  <noscript>
    
    <img src="/gastat.gif?pv=1&url=https%3a%2f%2fbackend.iferu.com%2ffr%2fmylibrary&iframe=&corp_uname=&drt=" alt="" style="display: none;" data-pagespeed-no-transform />
  </noscript>
</div>
<noscript>
  <img src="//trc.taboola.com/1154722/log/3/unip?en=page_view" width="0" height="0" style="display:none">
</noscript>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=832786183443261&amp;ev=PageView&amp;noscript=1"></noscript>

<noscript><img alt="" src="https://secure.leadforensics.com/100323.png" style="display:none;"></noscript>
</body>
</html>
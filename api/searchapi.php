<?php  

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Content-Type: application/json');
//database connection  
$conn = mysqli_connect('localhost', 'max', 'toor','books');  
if (! $conn) {  
die("Connection failed" . mysqli_connect_error());  
}  
else {  
mysqli_select_db($conn, 'pagination');  
}  

//define total number of results per page  
$results_per_page = 10;  
$titre=  $_GET['titre'];
//find the total number of results stored in the database  
$query = "select *from Document where titre LIKE '%".$titre."%'";  
$result = mysqli_query($conn, $query);  
$number_of_result = mysqli_num_rows($result);  

//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  

//determine which page number visitor is currently on  
if (!isset ($_GET['page'])  ) {  
    $page = 1;  
   
} else {  
    $page = $_GET['page'];
  
    
}  
if ( isset($_GET['idlang'])){
    $idlang=$_GET['idlang'];
}
else{
    $idlang=".";
}
if ( isset($_GET['hasaudio'])){
    $hasaudio=$_GET['hasaudio'];
}
else{
    $hasaudio="%";
}
if ( isset($_GET['bestseller'])){
    $bestseller=$_GET['bestseller'];
}
else{
    $bestseller="%";
}
if ( isset($_GET['type'])){


    $type=$_GET['type'];


    if(  $type =='book' OR $type == 'livre' OR $type =='Livro' OR   $type== 'Buch' OR   $type== 'Libro' OR $type=='Книга'){
        $type='Book|Livre|Livro|Buch|Libro|Книга';


    }
    if(  $type =='Artigo'||   $type == 'Artículo' || $type =='Artikel' ||   $type== 'article'){
        $type='Artigo|Artículo|Artikel|Article';

    }

}
else{
    $type=".";
}

//determine the sql LIMIT starting number for the results on the displaying page  
$page_first_result = ($page-1) * $results_per_page;  

//retrieve the selected results from database   
// $query = "SELECT *FROM Document  LIMIT " . $page_first_result . ',' . $results_per_page;  
$query = "SELECT D.*,Auteur.nom,Rating.number  FROM  Doc_Aut,Auteur,Document D,Rating,Doc_Chaine dc ,Chaine C,Doc_Aut S,Auteur A WHERE C.id=dc.idChaine and dc.idDoc=D.id and D.id=S.idDoc and A.id=S.idAut and titre !='Invalid' and Rating.id=D.idRating  and (titre LIKE '%".$titre."%' or C.nom_fr LIKE '%".$titre."%' or A.nom LIKE '%".$titre."%') and idLangue REGEXP '^(".$idlang.")' and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type REGEXP '^(".$type.")' and Doc_Aut.idDoc=D.id and Doc_Aut.idAut=Auteur.id group by D.id  LIMIT " . $page_first_result . ',' . $results_per_page;
$result = mysqli_query($conn, $query);  
$table = array();

//display the retrieved result on the webpage  
while ($row = $result->fetch_assoc()) {  
      array_push($table,$row);
}  

 echo json_encode($table);	

 

?>  
<?php  

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
header('Content-Type: application/json');
//database connection  
$conn = mysqli_connect('localhost', 'max', 'toor','books');  
if (! $conn) {  
die("Connection failed" . mysqli_connect_error());  
}  
else {  
mysqli_select_db($conn, 'pagination');  
}  

//define total number of results per page  
$results_per_page = 10;  
$titre=  $_GET['titre'];
//find the total number of results stored in the database  
$query = "select *from Document where titre LIKE '%".$titre."%'";  
$result = mysqli_query($conn, $query);  
$number_of_result = mysqli_num_rows($result);  

//determine the total number of pages available  
$number_of_page = ceil ($number_of_result / $results_per_page);  

//determine which page number visitor is currently on  
if (!isset ($_GET['page'])  ) {  
    $page = 1;  
   
} else {  
    $page = $_GET['page'];
  
    
}  
if ( isset($_GET['idlang'])){
    $idlang=$_GET['idlang'];
}
else{
    $idlang="%";
}
if ( isset($_GET['hasaudio'])){
    $hasaudio=$_GET['hasaudio'];
}
else{
    $hasaudio="%";
}
if ( isset($_GET['bestseller'])){
    $bestseller=$_GET['bestseller'];
}
else{
    $bestseller="%";
}
if ( isset($_GET['type'])){
    $type=$_GET['type'];
}
else{
    $type="%";
}


//determine the sql LIMIT starting number for the results on the displaying page  
$page_first_result = ($page-1) * $results_per_page;  

//retrieve the selected results from database   
// $query = "SELECT *FROM Document  LIMIT " . $page_first_result . ',' . $results_per_page;  
$query = "SELECT * FROM Document WHERE titre !='Invalid' and titre LIKE '%".$titre."%' and idLangue LIKE '".$idlang."' and hasaudio LIKE '".$hasaudio."' and bestseller LIKE '".$bestseller."' and type LIKE '".$type."'    LIMIT " . $page_first_result . ',' . $results_per_page;
$result = mysqli_query($conn, $query);  
$table = array();

//display the retrieved result on the webpage  
while ($row = $result->fetch_assoc()) {  
      array_push($table,$row);
}  

 echo json_encode($table);	

 

?>  